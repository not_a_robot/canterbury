/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"
#include "hard_keys.h"

#include "canterbury/messages-internal.h"

CanterburyHardKeys *hardkeys_obj = NULL;

void
init_hardkey (GDBusConnection *connection)
{
  DEBUG ("enter");

  back_handler_init();

  hardkeys_obj = canterbury_hard_keys_skeleton_new();

  g_signal_connect (hardkeys_obj,
                    "handle-back-press",
                    G_CALLBACK (handle_back_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-register-back-press",
                    G_CALLBACK (handle_register_back_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-back-press-consumed",
                    G_CALLBACK (handle_back_press_consumed_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-disable-back-press",
                    G_CALLBACK (handle_disable_back_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-home-press",
                    G_CALLBACK (handle_home_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-now-playing-press",
                    G_CALLBACK (handle_register_now_playing_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-category-press",
                    G_CALLBACK (handle_register_category_press_clb),
                    NULL);

  g_signal_connect (hardkeys_obj,
                    "handle-shutdown",
                    G_CALLBACK (handle_register_shutdown_clb),
                    NULL);
  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (hardkeys_obj),
                                         connection,
                                         "/org/apertis/Canterbury/HardKeys",
                                         NULL))
  {
    hard_keys_mgr_debug("export error \n");
  }
}

void
hard_keys_emit_back_press_response (gboolean result)
{
  canterbury_hard_keys_emit_back_press_response( hardkeys_obj ,result );
}

void
hard_keys_emit_back_press_register_response (const gchar *entry_point_id)
{
  canterbury_hard_keys_emit_back_press_register_response (hardkeys_obj,
                                                          entry_point_id);
}

void
hard_keys_emit_home_press_response (gboolean result)
{
  canterbury_hard_keys_emit_home_press_response( hardkeys_obj ,result );
}
