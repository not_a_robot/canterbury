/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ACTIVITY_MGR_IF__
#define __ACTIVITY_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/platform/entry-point.h"

G_BEGIN_DECLS

typedef enum
{
  ACTIVATE_FLAGS_NONE = 0,
  ACTIVATE_FLAGS_NEW = (1 << 0),
} ActivateFlags;

void activity_mgr_init (GDBusConnection *session_bus);

const gchar*
activity_mgr_get_app_on_top( void );

gboolean activity_mgr_has_entry_point (const gchar *entry_point_id);

void inform_app_new_state_to_audio_mgr (const gchar *client_name,
                                        CanterburyAppState NewState);

void activity_mgr_update_app_on_top (void);
gchar *activity_mgr_force_kill_something (void);
void activity_mgr_remove_entry_point (CbyEntryPoint *entry_point);

void activity_mgr_schedule_activation (CbyEntryPoint *entry_point,
                                       GPid pid,
                                       const gchar *const *uris,
                                       GVariant *platform_data,
                                       const gchar *const *argv,
                                       const gchar *launched_from,
                                       guint special_params,
                                       ActivateFlags flags);
void activity_mgr_schedule_force_kill (CbyEntryPoint *entry_point,
                                       GPid pid);
void activity_mgr_entry_point_activated (CbyEntryPoint *entry_point);

G_END_DECLS

#endif //__ACTIVITY_MGR_IF__
