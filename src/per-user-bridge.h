/*
 * Copyright © 2017 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _CBY_PER_USER_BRIDGE_H
#define _CBY_PER_USER_BRIDGE_H

#include <gio/gio.h>
#include <glib-object.h>

#include <canterbury/canterbury-platform.h>

#include "service-manager.h"

G_BEGIN_DECLS

#define CBY_TYPE_PER_USER_BRIDGE (cby_per_user_bridge_get_type ())

G_DECLARE_FINAL_TYPE (CbyPerUserBridge, cby_per_user_bridge, CBY, PER_USER_BRIDGE, CbyPerUserAppManager1Skeleton)

CbyPerUserBridge *cby_per_user_bridge_new (CbyServiceManager *service_manager);

void cby_per_user_bridge_register_async (CbyPerUserBridge *self,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data);
gboolean cby_per_user_bridge_register_finish (CbyPerUserBridge *self,
                                              GAsyncResult *result,
                                              GError **error);

G_END_DECLS

#endif /* _CBY_PER_USER_BRIDGE_H */
