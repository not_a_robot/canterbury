/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __LAUNCH_MGR_IF__
#define __LAUNCH_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

#include "app_mgr_if.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/platform/entry-point.h"
#include "canterbury/platform/entry-point-index.h"
#include "service-manager.h"

G_BEGIN_DECLS

typedef struct _LaunchNewAppInfo LaunchNewAppInfo;

struct _LaunchNewAppInfo
{
	CbyEntryPoint *entry_point;
	GPid      pid;
	/* Owned, does not include argv[0]. NULL if the entry point is
	 * D-Bus activatable. */
	gchar **legacy_arguments;
};

typedef enum
{
  LAUNCH_FLAGS_NONE = 0,
  LAUNCH_FLAGS_DAEMON = (1 << 0),
  LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_NEW = (1 << 2),
  LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_EXISTING = (1 << 3),
  LAUNCH_FLAGS_UPDATE_BACK_STACK =
    (LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_NEW |
     LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_EXISTING),
  LAUNCH_FLAGS_START_TIMER_IF_NEW = (1 << 4),
  LAUNCH_FLAGS_START_TIMER_IF_EXISTING = (1 << 5),
  LAUNCH_FLAGS_START_TIMER =
    (LAUNCH_FLAGS_START_TIMER_IF_NEW |
     LAUNCH_FLAGS_START_TIMER_IF_EXISTING),
  LAUNCH_FLAGS_CHECK_EXISTING_GUI = (1 << 6),
  LAUNCH_FLAGS_CHECK_EXISTING_DAEMON = (1 << 7),
  LAUNCH_FLAGS_NEW = (1 << 8),
  LAUNCH_FLAGS_EMIT_NEW_APP_STATE_IF_EXISTING = (1 << 9),
  LAUNCH_FLAGS_SET_ACTIVE_TITLE = (1 << 10),
  LAUNCH_FLAGS_ONLY_EXISTING = (1 << 11),
  LAUNCH_FLAGS_HIDE_GLOBAL_POPUPS = (1 << 12),
  LAUNCH_FLAGS_ARGV_REPLACE = (1 << 13),
  LAUNCH_FLAGS_ARGV_REPLACE_PLAY_MODE = (1 << 16),
} LaunchFlags;

void launch_mgr_init (CbyServiceManager *service_manager,
                      CbyEntryPointIndex *entry_point_index);

gboolean launch_mgr_stop_running_app (CbyEntryPoint *entry_point,
                                      GPid pid);
gboolean launch_mgr_kill_running_app (CbyEntryPoint *entry_point,
                                      GPid pid);
gboolean launch_mgr_signal_running_app (CbyEntryPoint *entry_point,
                                        GPid pid,
                                        gint signum);

void
v_kill_running_agent( const gchar* pAppName );

void v_launch_agent_or_service (CbyEntryPoint *entry_point);

void launch_mgr_launch_app_async (CbyEntryPoint *entry_point,
                                  const gchar *launched_from,
                                  guint special_params,
                                  const gchar * const *uris,
                                  GVariant *platform_data,
                                  const gchar * const *legacy_arguments,
                                  LaunchFlags flags,
                                  GCancellable *cancellation,
                                  GAsyncReadyCallback callback,
                                  gpointer user_data);
gboolean launch_mgr_launch_app_finish (GAsyncResult *result,
                                       GError **error);

gboolean is_app_launch_in_progress (void);

void v_launch_audio_daemon (CbyEntryPoint *entry_point);

LaunchNewAppInfo*
get_running_app_info_at_position(guint uinPosition);

guint get_running_app_info_stack_length (void);

LaunchNewAppInfo*
get_running_daemon_info_at_position(guint uinPosition);

guint get_running_daemon_info_stack_length (void);

CbyEntryPointIndex *launch_mgr_get_entry_point_index (void);

gboolean launch_mgr_kill_the_current_running_application (CbyEntryPoint *entry_point);

void display_launcher_app (const gchar *pCurrentAppNnTop,
    const gchar *pLaunchedClient);

void launch_mgr_launch_startup_applications (void);

void v_set_current_launcher_view_to_category (void);

void v_reset_current_launcher_view (void);

gboolean b_is_current_view_launcher (void);

LaunchNewAppInfo *launch_mgr_find_running_app (const gchar *app_name);

void launch_mgr_activate_launcher (const gchar *launched_by);

G_END_DECLS

#endif //__LAUNCH_MGR_IF__
