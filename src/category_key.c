/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"

#include "canterbury/platform/entry-point-internal.h"
#include "hard_keys.h"

#include <string.h>

gboolean handle_register_category_press_clb( CanterburyHardKeys *object,
                                             GDBusMethodInvocation   *invocation,
                                             gpointer                 pUserData )
{
  const gchar *pCurrentAppNnTop = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  const gchar *category;

  hard_keys_mgr_debug (" category_clb  \n");

  if(is_app_launch_in_progress())
  {
    canterbury_hard_keys_complete_category_press(object , invocation);
    return TRUE;
  }
  /* get the application running on top */
  pCurrentAppNnTop = activity_mgr_get_app_on_top();

  /* If there is no app on top, there's nothing to do. */
  if (pCurrentAppNnTop == NULL)
    {
      canterbury_hard_keys_complete_category_press (object, invocation);
      return TRUE;
    }

   if (launch_mgr_find_running_app (pCurrentAppNnTop) == NULL)
   {
     canterbury_hard_keys_complete_category_press(object , invocation);
     return TRUE;
   }

  /* check if the launcher already on top */
  if( !strcmp( pCurrentAppNnTop ,  LAUNCHER_APP_NAME ) )
  {
    hard_keys_mgr_debug("*** launcher on top \n");
    canterbury_hard_keys_complete_category_press(object , invocation);
    return TRUE;
  }

  hide_global_popups();
  canterbury_hard_keys_complete_category_press(object ,invocation );

  show_launcher_app();
  v_set_current_launcher_view_to_category();

  category = NULL;
  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_point = cby_entry_point_index_get_by_id (entry_point_index, pCurrentAppNnTop);

  if (entry_point != NULL)
    category = _cby_entry_point_get_category_label (entry_point);

  window_mgr_set_active (category, category);
  win_mgr_minimize_current_application_window( pCurrentAppNnTop , CANTERBURY_CATEGORY_KEY);

  return TRUE;
}

