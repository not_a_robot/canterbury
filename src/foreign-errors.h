/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

/*
 * CBY_SYSTEMD_ERROR:
 *
 * Error domain for (a subset of) errors raised by systemd.
 * Error codes in this domain are members of the #CbySystemdError enumeration.
 */
#define CBY_SYSTEMD_ERROR (_cby_systemd_error_quark ())

/*
 * CbySystemdError:
 * @CBY_SYSTEMD_ERROR_NO_SUCH_UNIT: The requested unit does not exist
 *
 * Error enumeration for errors in the %CBY_SYSTEMD_ERROR domain.
 * Please add new errors here when we find we need to handle them
 * specially, using systemd's
 * `src/libsystemd/sd-bus/bus-common-errors.h` for reference.
 */
typedef enum
{
  CBY_SYSTEMD_ERROR_NO_SUCH_UNIT,
} CbySystemdError;
#define CBY_N_SYSTEMD_ERRORS (CBY_SYSTEMD_ERROR_NO_SUCH_UNIT + 1)

GQuark _cby_systemd_error_quark (void);

G_END_DECLS
