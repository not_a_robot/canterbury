/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "service-manager.h"

#include <errno.h>

#include <glib/gstdio.h>

#include "canterbury/errors.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/dbus.h"
#include "canterbury/platform/entry-point-internal.h"
#include "canterbury/platform/enumtypes.h"

#include "foreign-errors.h"

#define SYSTEMD_BUS_NAME "org.freedesktop.systemd1"
#define SYSTEMD_PATH_MANAGER "/org/freedesktop/systemd1"
#define SYSTEMD_IFACE_MANAGER SYSTEMD_BUS_NAME ".Manager"
#define SYSTEMD_IFACE_SERVICE SYSTEMD_BUS_NAME ".Service"
#define SYSTEMD_IFACE_UNIT SYSTEMD_BUS_NAME ".Unit"
#define DBUS_NAME_DBUS "org.freedesktop.DBus"
#define DBUS_PATH_DBUS "/org/freedesktop/DBus"
#define DBUS_IFACE_DBUS DBUS_NAME_DBUS
#define DBUS_IFACE_PROPERTIES "org.freedesktop.DBus.Properties"

typedef struct _ServiceStoppedSignalData ServiceStoppedSignalData;
typedef struct _ServiceInfo ServiceInfo;

struct _ServiceStoppedSignalData
{
  CbyServiceManager *service_manager;
  CbyEntryPoint *entry_point;
};

struct _ServiceInfo
{
  gsize refcount;
  /* (element-type CbyEntryPoint) (ownership full) */
  GList *entry_points;
  gchar *service_name;
  gchar *slice;
  gchar *dbus_path;
  /* (element-type GTask) (ownership full)
   * Reverse chronological order (most recent first). The least recent (the
   * last in the list) is the one responsible for actually starting
   * the service. */
  GList *start_tasks;
  GPid pid;
};

struct _CbyServiceManager
{
  GObject parent;

  GDBusConnection *session_bus;
  CbyEntryPointIndex *entry_point_index;

  gchar *systemd_transient_path;
  gchar *dbus_transient_path;

  /* Map of entry point to systemd service:
   * { owned CbyEntryPoint: owned ServiceInfo } */
  GHashTable *entry_points;

  /* Map of systemd service name to ServiceInfo
   * { string borrowed from ServiceInfo: owned ServiceInfo } */
  GHashTable *systemd_services;

  /* Map of job name to GTask:
   * Map of { owned gchar*: owned GTask* } */
  GHashTable *pending_jobs;

  gulong entry_point_index_after_changes_id;
  gulong entry_point_index_added_id;
  gulong entry_point_index_removed_id;
  gulong entry_point_index_changed_id;
  guint properties_changed_id;
  guint job_removed_id;

  GError *init_error;
};

enum
{
  SIGNAL_SERVICE_CREATED,
  SIGNAL_SERVICE_UPDATED,
  SIGNAL_SERVICE_REMOVED,
  SIGNAL_SERVICE_STOPPED,
  LAST_SIGNAL
};

static guint cby_service_manager_signals[LAST_SIGNAL] = { 0 };

enum
{
  PROP_0,
  PROP_SESSION_BUS,
  PROP_ENTRY_POINT_INDEX,
  PROP_LAST
};

static void initable_iface_init (GInitableIface *initable_iface);
static void job_removed_cb (GDBusConnection *connection,
                            const gchar *sender_name,
                            const gchar *object_path,
                            const gchar *interface_name,
                            const gchar *signal_name,
                            GVariant *parameters,
                            gpointer user_data);
static void properties_changed_cb (GDBusConnection *connection,
                                   const gchar *sender_name,
                                   const gchar *object_path,
                                   const gchar *interface_name,
                                   const gchar *signal_name,
                                   GVariant *parameters,
                                   gpointer user_data);

G_DEFINE_TYPE_WITH_CODE (CbyServiceManager,
                         cby_service_manager,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                initable_iface_init))

/* We know/assume that Unix process IDs are ints */
G_STATIC_ASSERT (sizeof (GPid) == sizeof (int));

static inline void
object_list_free (GList *l)
{
  g_list_free_full (l, g_object_unref);
}

typedef GList ObjectList;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (ObjectList, object_list_free)

static ServiceStoppedSignalData *
service_stopped_signal_data_new (CbyServiceManager *service_manager,
                                 CbyEntryPoint *entry_point)
{
  ServiceStoppedSignalData *self;

  g_return_val_if_fail (service_manager != NULL, NULL);

  self = g_slice_new0 (ServiceStoppedSignalData);
  self->service_manager = g_object_ref (service_manager);
  self->entry_point = g_object_ref (entry_point);
  return self;
}

static void
service_stopped_signal_data_free (ServiceStoppedSignalData *self)
{
  g_object_unref (self->service_manager);
  g_object_unref (self->entry_point);
  g_slice_free (ServiceStoppedSignalData, self);
}

static ServiceInfo *
service_info_new (CbyEntryPoint *entry_point,
                  const gchar *service_name,
                  const gchar *slice)
{
  ServiceInfo *self;

  g_return_val_if_fail (service_name != NULL, NULL);

  self = g_slice_new0 (ServiceInfo);
  self->refcount = 1;
  self->entry_points = g_list_prepend (self->entry_points,
                                       g_object_ref (entry_point));
  self->service_name = g_strdup (service_name);
  self->slice = g_strdup (slice);
  return self;
}

static ServiceInfo *
service_info_ref (ServiceInfo *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);
  self->refcount++;
  return self;
}

static void
service_info_unref (ServiceInfo *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);

  if (--self->refcount > 0)
    return;

  g_list_free_full (self->entry_points, g_object_unref);
  g_free (self->service_name);
  g_free (self->slice);
  g_free (self->dbus_path);
  /* Because systemd_services keeps a reference, and we cancel all the tasks
   * before removing that reference, we shouldn't have any open start tasks
   * here. If we did, they wouldn't necessarily finish correctly. */
  g_warn_if_fail (self->start_tasks == NULL);
  g_list_free_full (self->start_tasks, g_object_unref);
  g_slice_free (ServiceInfo, self);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ServiceInfo, service_info_unref)

static ServiceInfo *
cby_service_manager_get_service_for_entry_point (CbyServiceManager *self,
                                                 CbyEntryPoint *entry_point)
{
  return g_hash_table_lookup (self->entry_points, entry_point);
}

static gboolean
find_service_for_dbus_path (CbyEntryPoint *entry_point,
                            ServiceInfo *service_info,
                            gpointer user_data)
{
  const gchar *dbus_path = user_data;

  return (g_strcmp0 (service_info->dbus_path, dbus_path) == 0);
}

static ServiceInfo *
cby_service_manager_get_service_for_object_path (CbyServiceManager *self,
                                                 const gchar *object_path)
{
  return g_hash_table_find (self->systemd_services,
                            (GHRFunc) find_service_for_dbus_path,
                            (gpointer) object_path);
}

static gboolean
ensure_systemd_service (CbyServiceManager *self,
                        CbyEntryPoint *entry_point,
                        gboolean *created,
                        GError **error)
{
  const gchar *bundle_id;
  const gchar *entry_point_id;
  g_autofree gchar *systemd_service_name = NULL;
  g_autofree gchar *slice = NULL;
  g_autofree gchar *parent = NULL;
  g_autofree gchar *dbus_contents = NULL;
  g_autofree gchar *dbus_path = NULL;
  g_autofree gchar *dbus_service_name = NULL;
  ServiceInfo *service_info = NULL;
  gboolean dbus_activatable;

  /* For now, we only create systemd service files for app-bundles (bundle id != NULL) */
  bundle_id = cby_process_info_get_bundle_id (_cby_entry_point_get_process_info (entry_point));
  if (!bundle_id)
    {
      if (created != NULL)
        *created = FALSE;

      return TRUE;
    }

  entry_point_id = cby_entry_point_get_id (entry_point);
  dbus_activatable = _cby_entry_point_get_dbus_activatable (entry_point);

  dbus_service_name = g_strdup_printf ("%s.service", entry_point_id);
  slice = g_strdup_printf ("%s.slice", bundle_id);

  if (dbus_activatable)
    {
      /* parent is non-NULL if this entry point (the *child*) is an alternate
       * o.fd.Application within some other activatable service, the
       * *parent*. */
      parent =
          cby_entry_point_get_string (entry_point,
                                      CBY_ENTRY_POINT_KEY_X_APERTIS_PARENT_ENTRY);

      if (parent != NULL)
        systemd_service_name = g_strdup_printf ("%s.service", parent);
      else
        systemd_service_name = g_strdup (dbus_service_name);

      dbus_contents =
        g_strdup_printf ("[D-BUS Service]\n"
                         "Name=%s\n"
                         /* We never actually execute this, so just use
                          * something that will always fail */
                         "Exec=/bin/false %s\n"
                         "SystemdService=%s\n",
                         entry_point_id,
                         entry_point_id,
                         systemd_service_name);

      dbus_path = g_build_filename (self->dbus_transient_path,
                                    dbus_service_name, NULL);
      DEBUG ("Configuring D-Bus service at \"%s\" for entry point \"%s\"",
             dbus_path, entry_point_id);

      if (!g_file_set_contents (dbus_path, dbus_contents, -1, error))
        {
          g_prefix_error (error,
                          "Unable to configure D-Bus service at \"%s\" for "
                          "entry point \"%s\": ",
                          dbus_path, entry_point_id);
          return FALSE;
        }
    }
  else
    {
      /* We ignore X-Apertis-ParentEntry (and leave parent set to NULL)
       * if the entry point is not D-Bus-activatable, since we need to
       * generate Type=simple systemd services for legacy child entry points
       * to get the correct argv passed to them. */
      systemd_service_name = g_strdup (dbus_service_name);
    }

  if (parent == NULL)
    {
      CanterburyExecutableType executable_type =
        _cby_entry_point_get_executable_type (entry_point);
      g_autoptr (GString) contents = NULL;
      g_autofree gchar *service_path = NULL;
      g_autoptr (GString) exec_start = NULL;
      const gchar *working_directory = NULL;
      int i;
      /* Only directly used if dbus_activatable, but must have this wider
       * scope so it isn't freed too soon */
      g_auto (GStrv) service_argv = NULL;
      /* Points to either service_argv or the CbyEntryPoint's argv */
      const gchar *const *argv = NULL;

      /* If it isn't D-Bus-activatable, we need to pass the Exec= argv
       * rather than X-Apertis-ServiceExec, so that it gets activated
       * implicitly. If it's D-Bus-activatable, we can leave the activation
       * until Activate(). */
      if (dbus_activatable)
        {
          /* The Exec line used when launching as a service without implicit
           * activation, typically something like
           * "/Applications/com.example.MyApp/bin/main --gapplication-service".
           */
          g_autofree gchar *service_exec =
            cby_entry_point_get_string (entry_point,
                                        CBY_ENTRY_POINT_KEY_X_APERTIS_SERVICE_EXEC);

          if (service_exec != NULL)
            {
              g_autoptr (GError) local_error = NULL;

              if (!g_shell_parse_argv (service_exec, NULL, &service_argv,
                                       &local_error))
                {
                  /* Use Exec instead of failing hard */
                  WARNING ("Invalid %s for %s: %s",
                           CBY_ENTRY_POINT_KEY_X_APERTIS_SERVICE_EXEC,
                           entry_point_id, local_error->message);
                }
              else
                {
                  argv = (const gchar * const *) service_argv;
                }
            }
          /* else leave argv set to NULL */
        }
      /* else leave argv set to NULL */

      if (argv == NULL)
        argv = _cby_entry_point_get_argv (entry_point);

      exec_start = g_string_new (BINDIR "/canterbury-exec --");

      for (i = 0; argv[i] != NULL; ++i)
        {
          int j;
          const gchar *arg = argv[i];

          g_string_append (exec_start, " ");

          for (j = 0; arg[j] != '\0'; ++j)
            {
              gchar c = arg[j];

              if (c == '$' || c == '%' || c == '\\')
                {
                  g_string_append_c (exec_start, c);
                  g_string_append_c (exec_start, c);
                }
              else if g_ascii_isgraph (c)
                {
                  g_string_append_c (exec_start, c);
                }
              else
                {
                  g_string_append_printf (exec_start, "\\x%02x", c);
                }
            }
        }

      working_directory = _cby_entry_point_get_working_directory (entry_point);
      /* The default for user services is the home directory of the user */
      if (working_directory == NULL)
        working_directory = "~";

      contents = g_string_new ("");
      g_string_append_printf (contents,
                              "[Service]\n"
                              "ExecStart=%s\n"
                              "WorkingDirectory=%s\n"
                              "Slice=%s\n"
                              "TimeoutStopSec=%ds\n",
                              exec_start->str,
                              working_directory,
                              slice,
                              CBY_SERVICE_STOP_TIMEOUT_SEC);

      if (dbus_activatable)
        g_string_append_printf (contents,
                                "Type=dbus\n"
                                "BusName=%s\n",
                                entry_point_id);
      else
        g_string_append (contents, "Type=simple\n");

      if (executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE ||
          executable_type == CANTERBURY_EXECUTABLE_TYPE_SERVICE)
        {
          /*
           * Restart agents (and other services if we found any) unless they
           * exit 0, are stopped by systemd, or are killed by signals that
           * normally indicate intentional termination: SIGHUP, SIGINT,
           * SIGTERM or SIGPIPE. Restart is controlled by several settings
           * (the defaults as of systemd 232 are shown here):
           *
           * [Service]
           * RestartSec=100ms
           * [Unit]
           * StartLimitBurst=5
           * StartLimitIntervalSec=10s
           *
           * If a service crashes, it will be restarted after RestartSec,
           * unless there have already been StartLimitBurst attempts to
           * restart it within StartLimitIntervalSec.
           *
           * For Apertis agents, we slow this down a bit to minimize the
           * effect of faulty agents on system performance: we don't try to
           * restart the agent for 5 seconds, and if it is still failing
           * after 5 attempts, we give up for 20 minutes. These are arbitrary
           * and can be adjusted as desired.
           */
          g_string_append (contents, "Restart=on-failure\n"
                                     "RestartSec=5s\n"
                                     "[Unit]\n"
                                     "StartLimitBurst=5\n"
                                     "StartLimitIntervalSec=20min\n");
        }

      service_path = g_build_filename (self->systemd_transient_path,
                                       systemd_service_name, NULL);
      DEBUG ("Configuring systemd service at \"%s\" for entry point \"%s\"",
             service_path, entry_point_id);

      if (!g_file_set_contents (service_path, contents->str, contents->len,
                                error))
        {
          g_prefix_error (error,
                          "Unable to configure systemd service at \"%s\" for "
                          "entry point \"%s\": ",
                          service_path, entry_point_id);
          return FALSE;
        }

      if (executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE)
        {
          g_autofree gchar *wants_path = NULL;
          g_autofree gchar *wants_path_temp = NULL;
          g_autofree gchar *wants_target = NULL;

          wants_path = g_build_filename (self->systemd_transient_path,
                                         "canterbury-agents.target.wants",
                                         systemd_service_name, NULL);
          wants_path_temp = g_strconcat (wants_path, ".tmp", NULL);
          wants_target = g_build_filename ("..", systemd_service_name, NULL);

          if (symlink (wants_target, wants_path_temp) != 0)
            {
              int saved_errno = errno;

              g_set_error (error, CBY_ERROR, CBY_ERROR_FAILED,
                           "Failed to create symbolic link \"%s\": %s",
                           wants_path_temp, g_strerror (saved_errno));
              return FALSE;
            }

          if (rename (wants_path_temp, wants_path) != 0)
            {
              int saved_errno = errno;

              g_set_error (error, CBY_ERROR, CBY_ERROR_FAILED,
                           "Failed to rename \"%s\" to \"%s\": %s",
                           wants_path_temp, wants_path,
                           g_strerror (saved_errno));

              if (unlink (wants_path_temp) != 0)
                {
                  saved_errno = errno;
                  WARNING ("Unable to recover from failure to rename \"%s\" "
                           "to \"%s\": unlink(\"%s\"): %s",
                           wants_path_temp, wants_path, wants_path_temp,
                           g_strerror (saved_errno));
                }

              return FALSE;
            }
        }
    }

  service_info = g_hash_table_lookup (self->systemd_services,
                                      systemd_service_name);

  if (service_info == NULL)
    {
      g_autoptr (ServiceInfo) new_info =
          service_info_new (entry_point, systemd_service_name, slice);

      service_info = new_info;
      g_hash_table_replace (self->systemd_services, service_info->service_name,
                            g_steal_pointer (&new_info));
    }
  else
    {
      service_info->entry_points =
          g_list_prepend (service_info->entry_points,
                          g_object_ref (entry_point));
    }

  g_hash_table_replace (self->entry_points, g_object_ref (entry_point),
                        service_info_ref (service_info));

  if (created != NULL)
    *created = TRUE;

  return TRUE;
}

static void stop_service_async (CbyServiceManager *self,
                                ServiceInfo *service_info,
                                GCancellable *cancellable,
                                GAsyncReadyCallback callback,
                                gpointer user_data);

static void
ensure_service_stopped_cb (GObject *source_object,
                           GAsyncResult *result,
                           gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source_object);
  g_autoptr (ServiceInfo) service_info = user_data;
  g_autoptr (GError) error = NULL;

  if (_cby_service_manager_stop_service_finish (service_manager, result,
                                                &error))
    DEBUG ("Service \"%s\" stopped", service_info->service_name);
  else if (g_error_matches (error, CBY_SYSTEMD_ERROR,
                            CBY_SYSTEMD_ERROR_NO_SUCH_UNIT))
    DEBUG ("Service \"%s\" wasn't loaded so there was nothing to stop",
           service_info->service_name);
  else
    WARNING ("Unable to stop service \"%s\": %s",
             service_info->service_name, error->message);
}

static void
ensure_service_stopped (CbyServiceManager *self,
                        ServiceInfo *service_info)
{
  DEBUG ("Ensuring service \"%s\" is stopped", service_info->service_name);
  stop_service_async (self, service_info, NULL, ensure_service_stopped_cb,
                      service_info_ref (service_info));
}

static void service_startup_failed (CbyServiceManager *self,
                                    GTask *task,
                                    ServiceInfo *service_info,
                                    gboolean stop_service,
                                    const GError *error);

static gboolean
remove_systemd_service (CbyServiceManager *self,
                        CbyEntryPoint *entry_point)
{
  const gchar *id = cby_entry_point_get_id (entry_point);
  ServiceInfo *service_info;
  g_autofree gchar *dbus_service_basename = NULL;
  g_autofree gchar *dbus_service_path = NULL;
  g_autofree gchar *service_path = NULL;
  int saved_errno;

  service_info = cby_service_manager_get_service_for_entry_point (self,
                                                                  entry_point);

  if (!service_info)
    return FALSE;

  dbus_service_basename = g_strdup_printf ("%s.service", id);
  dbus_service_path = g_build_filename (self->dbus_transient_path,
                                        dbus_service_basename, NULL);

  if (g_unlink (dbus_service_path) != 0)
    {
      saved_errno = errno;

      if (saved_errno != ENOENT)
        WARNING ("Unable to unlink D-Bus service at \"%s\" for entry point "
                 "\"%s\": %s",
                 dbus_service_path, id, g_strerror (saved_errno));
    }

  g_hash_table_remove (self->entry_points, entry_point);
  service_info->entry_points = g_list_remove_all (service_info->entry_points,
                                                  entry_point);

  /* Don't remove the ServiceInfo if another entry point still refers to it */
  if (service_info->entry_points != NULL)
    return TRUE;

  service_path = g_build_filename (self->systemd_transient_path,
                                   service_info->service_name, NULL);
  if (g_unlink (service_path) != 0)
    {
      saved_errno = errno;

      if (saved_errno != ENOENT)
        WARNING ("Unable to unlink systemd service at \"%s\" for entry point \"%s\": %s",
                 service_path, id, g_strerror (saved_errno));
    }

  if (service_info->start_tasks != NULL &&
      !g_task_had_error (service_info->start_tasks->data))
    {
      g_autoptr (GError) error = NULL;

      /* Service is still starting, fail the start tasks - the task result
       * will be checked during startup handling
       */
      DEBUG ("Service \"%s\" removed while still starting, failing startup",
             service_info->service_name);

      g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                   "Failed to start service for entry point \"%s\" - entry point got removed while starting",
                   id);
      /* We don't stop the service - we're about to do that (below) anyway */
      service_startup_failed (self, NULL, service_info, FALSE, error);
    }

  DEBUG ("Service removed, ensuring service \"%s\" is stopped",
         service_info->service_name);
  ensure_service_stopped (self, service_info);

  g_assert (service_info->start_tasks == NULL);
  g_hash_table_remove (self->systemd_services, service_info->service_name);
  return TRUE;
}

static void
reload_dbus_daemon_cb (GObject *source_object,
                       GAsyncResult *result,
                       gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      WARNING ("Failed to reload session dbus-daemon: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
    }
  else
    {
      DEBUG ("Session dbus-daemon reloaded");
    }

  if (!g_task_had_error (task))
    g_task_return_boolean (task, TRUE);
}

static void
reload_systemd_cb (GObject *source_object,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  CbyServiceManager *self = g_task_get_source_object (task);

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      WARNING ("Failed to reload systemd user daemon: %s", error->message);
      g_task_return_error (task, g_steal_pointer (&error));
      /* We continue to try to reload the dbus-daemon anyway */
    }
  else
    {
      DEBUG ("systemd user daemon reloaded");
    }

  DEBUG ("Reloading session dbus-daemon");

  g_dbus_connection_call (self->session_bus,
                          DBUS_NAME_DBUS,
                          DBUS_PATH_DBUS,
                          DBUS_IFACE_DBUS,
                          "ReloadConfig",
                          NULL,
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          NULL,
                          reload_dbus_daemon_cb,
                          g_object_ref (task));
}

/*
 * _cby_service_manager_reload_units_async:
 * @self: The service manager
 * @cancellable: (nullable): If not %NULL, can be used to cancel the callback
 * @callback: (nullable) (scope async): If not %NULL, called when all
 *  reloading has been done
 * @user_data: (nullable) (closure): Arbitrary data to be passed to @callback
 *
 * Instruct `systemd --user` and `dbus-daemon --session` to reload units.
 * In particular, this ensures that they are aware of the units generated
 * by this object.
 */
void
_cby_service_manager_reload_units_async (CbyServiceManager *self,
                                         GCancellable *cancellable,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _cby_service_manager_reload_units_async);

  DEBUG ("Reloading systemd user daemon\n");

  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "Reload",
                          NULL,
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          NULL,
                          reload_systemd_cb,
                          g_object_ref (task));
}

gboolean
_cby_service_manager_reload_units_finish (CbyServiceManager *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_reload_units_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
start_agents_after_changes_cb (GObject *source,
                               GAsyncResult *result,
                               gpointer user_data)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (source);
  g_autoptr (GError) error = NULL;

  if (!_cby_service_manager_start_agents_finish (self, result, &error))
    {
      WARNING ("Unable to start agents after changes to component index: %s",
               error->message);
    }
}

static void
reload_units_after_changes_cb (GObject *source,
                               GAsyncResult *result,
                               gpointer user_data)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (source);

  /* If the reload failed, we already issued a warning; blindly try starting
   * agents anyway */
  _cby_service_manager_start_agents_async (self, NULL,
                                           start_agents_after_changes_cb,
                                           NULL);
}

static void
entry_point_index_after_changes_cb (CbyEntryPointIndex *entry_point_index,
                                    gpointer user_data)
{
  CbyServiceManager *self = user_data;

  _cby_service_manager_reload_units_async (self, NULL,
                                           reload_units_after_changes_cb,
                                           NULL);
}

static void
entry_point_index_added_cb (CbyEntryPointIndex *entry_point_index,
                            CbyEntryPoint *entry_point,
                            gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  CbyServiceManager *self = user_data;
  gboolean created;

  if (!ensure_systemd_service (self, entry_point, &created, &error))
    {
      WARNING ("%s", error->message);
    }
  else if (created)
    {
      DEBUG ("Added systemd service for entry point \"%s\"",
             cby_entry_point_get_id (entry_point));
      g_signal_emit (self, cby_service_manager_signals[SIGNAL_SERVICE_CREATED], 0, entry_point);
    }
}

static void
entry_point_index_removed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  CbyServiceManager *self = user_data;

  if (remove_systemd_service (self, entry_point))
    {
      DEBUG ("Removed systemd service for entry point \"%s\"",
             cby_entry_point_get_id (entry_point));
      g_signal_emit (self, cby_service_manager_signals[SIGNAL_SERVICE_REMOVED], 0, entry_point);
    }
}

static void
entry_point_index_changed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  CbyServiceManager *self = user_data;
  gboolean removed;
  gboolean created;

  /* Ignore failures on remove, it may happen that the service file wasn't
   * created for the old entry point.
   */
  removed = remove_systemd_service (self, entry_point);

  if (!ensure_systemd_service (self, entry_point, &created, &error))
    {
      WARNING ("%s", error->message);
    }
  else if (created)
    {
      if (removed)
        {
          DEBUG ("Updated systemd service for entry point \"%s\"",
                 cby_entry_point_get_id (entry_point));
          g_signal_emit (self, cby_service_manager_signals[SIGNAL_SERVICE_UPDATED],
                         0, entry_point);
        }
      else
        {
          DEBUG ("Added systemd service for entry point \"%s\"",
                 cby_entry_point_get_id (entry_point));
          g_signal_emit (self, cby_service_manager_signals[SIGNAL_SERVICE_CREATED],
                         0, entry_point);
        }
    }
  else
    {
      if (removed)
        {
          DEBUG ("Removed systemd service for entry point \"%s\"",
                 cby_entry_point_get_id (entry_point));
          g_signal_emit (self, cby_service_manager_signals[SIGNAL_SERVICE_REMOVED],
                         0, entry_point);
        }
    }
}

static void
cby_service_manager_init (CbyServiceManager *self)
{
  self->entry_points =
      g_hash_table_new_full (g_direct_hash, g_direct_equal,
                             g_object_unref,
                             (GDestroyNotify) service_info_unref);
  self->systemd_services =
      g_hash_table_new_full (g_str_hash, g_str_equal,
                             NULL, (GDestroyNotify) service_info_unref);
  self->pending_jobs =
      g_hash_table_new_full (g_str_hash, g_str_equal,
                             g_free, (GDestroyNotify) g_object_unref);
}

static void
systemd_subscribe_cb (GObject *source_object,
                      GAsyncResult *result,
                      gpointer user_data)
{
  g_autoptr (CbyServiceManager) self = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    WARNING ("Failed to subscribe to systemd signals, service watch notification won't work: %s",
             error->message);
  else
    DEBUG ("Subscribed to systemd signals");
}

static void
systemd_subscribe (CbyServiceManager *self)
{
  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "Subscribe",
                          NULL,
                          NULL,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          NULL,
                          systemd_subscribe_cb,
                          g_object_ref (self));
}

static void
cby_service_manager_constructed (GObject *object)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (object);
  g_autoptr (GPtrArray) entry_points = NULL;
  g_autofree gchar *agents_target_wants = NULL;
  guint i;

  if (!G_IS_DBUS_CONNECTION (self->session_bus))
    {
      g_set_error (&self->init_error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                   "Required \"session-bus\" property not provided");
      return;
    }

  if (!CBY_IS_ENTRY_POINT_INDEX (self->entry_point_index))
    {
      g_set_error (&self->init_error, CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                   "Required \"entry-point-index\" property not provided");
      return;
    }

  self->systemd_transient_path = g_build_filename (g_get_user_runtime_dir (),
                                                   "systemd", "transient", NULL);
  agents_target_wants = g_build_filename (self->systemd_transient_path,
                                          "canterbury-agents.target.wants",
                                          NULL);

  /* Ensure directory to hold service files exists */
  if (g_mkdir_with_parents (self->systemd_transient_path, 0700) == -1)
    {
      int saved_errno = errno;

      g_set_error (&self->init_error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to create transient systemd service directory at \"%s\": %s",
                   self->systemd_transient_path, g_strerror (saved_errno));
      return;
    }

  /* We create this one separately so we can have a better error message if it
   * fails */
  if (g_mkdir_with_parents (agents_target_wants, 0700) == -1)
    {
      int saved_errno = errno;

      g_set_error (&self->init_error, G_IO_ERROR,
                   g_io_error_from_errno (saved_errno),
                   "Unable to create transient systemd service directory at "
                   "\"%s\": %s",
                   agents_target_wants, g_strerror (saved_errno));
      return;
    }

  self->dbus_transient_path = g_build_filename (g_get_user_runtime_dir (),
                                                "dbus-1", "services", NULL);
  if (g_mkdir_with_parents (self->dbus_transient_path, 0700) == -1)
    {
      int saved_errno = errno;

      g_set_error (&self->init_error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to create transient D-Bus service directory at \"%s\": %s",
                   self->dbus_transient_path, g_strerror (saved_errno));
      return;
    }

  systemd_subscribe (self);

  self->entry_point_index_after_changes_id = g_signal_connect (self->entry_point_index,
      "after-changes", G_CALLBACK (entry_point_index_after_changes_cb), self);
  self->entry_point_index_added_id = g_signal_connect (self->entry_point_index,
      "added", G_CALLBACK (entry_point_index_added_cb), self);
  self->entry_point_index_removed_id = g_signal_connect (self->entry_point_index,
      "removed", G_CALLBACK (entry_point_index_removed_cb), self);
  self->entry_point_index_changed_id = g_signal_connect (self->entry_point_index,
      "changed", G_CALLBACK (entry_point_index_changed_cb), self);

  entry_points = cby_entry_point_index_get_entry_points (self->entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      CbyEntryPoint *entry_point = g_ptr_array_index (entry_points, i);

      entry_point_index_added_cb (self->entry_point_index, entry_point, self);
    }
}

static gboolean
cby_service_manager_initable_init (GInitable *initable,
                                   GCancellable *cancellable,
                                   GError **error)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (initable);

  /* We did the checks at construct time; just pass on the result. */

  if (self->init_error == NULL)
    return TRUE;

  g_set_error_literal (error, self->init_error->domain, self->init_error->code,
                       self->init_error->message);
  return FALSE;
}

static void
cby_service_manager_get_property (GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (object);

  g_return_if_fail (self->init_error == NULL);

  switch (prop_id)
    {
    case PROP_SESSION_BUS:
      g_value_set_object (value, self->session_bus);
      break;

    case PROP_ENTRY_POINT_INDEX:
      g_value_set_object (value, self->entry_point_index);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_service_manager_set_property (GObject *object,
                                  guint prop_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (object);

  g_return_if_fail (self->init_error == NULL);

  switch (prop_id)
    {
    case PROP_SESSION_BUS:
      /* construct-only */
      g_assert (self->session_bus == NULL);
      self->session_bus = g_value_dup_object (value);
      break;

    case PROP_ENTRY_POINT_INDEX:
      /* construct-only */
      g_assert (self->entry_point_index == NULL);
      self->entry_point_index = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
cby_service_manager_dispose (GObject *object)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (object);

  if (self->entry_point_index_after_changes_id != 0)
    {
      g_signal_handler_disconnect (self->entry_point_index,
                                   self->entry_point_index_after_changes_id);
      self->entry_point_index_after_changes_id = 0;
    }
  if (self->entry_point_index_added_id != 0)
    {
      g_signal_handler_disconnect (self->entry_point_index,
                                   self->entry_point_index_added_id);
      self->entry_point_index_added_id = 0;
    }
  if (self->entry_point_index_removed_id != 0)
    {
      g_signal_handler_disconnect (self->entry_point_index,
                                   self->entry_point_index_removed_id);
      self->entry_point_index_removed_id = 0;
    }
  if (self->entry_point_index_changed_id != 0)
    {
      g_signal_handler_disconnect (self->entry_point_index,
                                   self->entry_point_index_changed_id);
      self->entry_point_index_changed_id = 0;
    }
  if (self->job_removed_id != 0)
    {
      g_dbus_connection_signal_unsubscribe (self->session_bus,
                                            self->job_removed_id);
      self->job_removed_id = 0;
    }
  if (self->properties_changed_id != 0)
    {
      g_dbus_connection_signal_unsubscribe (self->session_bus,
                                            self->properties_changed_id);
      self->properties_changed_id = 0;
    }

  g_clear_object (&self->session_bus);
  g_clear_object (&self->entry_point_index);
  g_clear_pointer (&self->systemd_transient_path, g_free);
  g_clear_pointer (&self->entry_points, g_hash_table_unref);
  g_clear_pointer (&self->systemd_services, g_hash_table_unref);
  g_clear_pointer (&self->pending_jobs, g_hash_table_unref);
}

static void
cby_service_manager_class_init (CbyServiceManagerClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  /* We use this error quark elsewhere. Call it for its side-effect of
   * being registered with GDBus. */
  (void) _cby_systemd_error_quark ();

  object_class->constructed = cby_service_manager_constructed;
  object_class->get_property = cby_service_manager_get_property;
  object_class->set_property = cby_service_manager_set_property;
  object_class->dispose = cby_service_manager_dispose;

  /**
   * CbyServiceManager::service-created:
   * @self: the service manager
   * @entry_point: the entry point for which the service file was created
   *
   * Emitted when the systemd service file for this entry point is created,
   * or when a new D-Bus service file for the entry point is associated with
   * a systemd service file that is shared with other entry points.
   *
   * Note that currently only entry points belonging to app-bundles have
   * corresponding systemd service files.
   */
  cby_service_manager_signals[SIGNAL_SERVICE_CREATED] =
      g_signal_new ("service-created", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);
  /**
   * CbyServiceManager::service-removed:
   * @self: the service manager
   * @entry_point: the entry point for which the service file was removed
   *
   * Emitted when the systemd service file for this entry point is removed,
   * or when a D-Bus service file for this D-Bus activatable entry point
   * is disassociated from its systemd service file.
   */
  cby_service_manager_signals[SIGNAL_SERVICE_REMOVED] =
      g_signal_new ("service-removed", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);
  /**
   * CbyServiceManager::service-updated:
   * @self: the service manager
   * @entry_point: the entry point for which the service file was updated
   *
   * Emitted when the systemd service file for this entry point is updated.
   */
  cby_service_manager_signals[SIGNAL_SERVICE_UPDATED] =
      g_signal_new ("service-updated", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);
  /**
   * CbyServiceManager::service-stopped:
   * @self: the service manager
   * @entry_point: the entry point for which the service has stopped
   *
   * Emitted when the systemd service started successfully with
   * _cby_service_manager_start_service_async stops.
   */
  cby_service_manager_signals[SIGNAL_SERVICE_STOPPED] =
      g_signal_new ("service-stopped", G_TYPE_FROM_CLASS (cls), G_SIGNAL_RUN_LAST,
                    0, NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, CBY_TYPE_ENTRY_POINT);

  /**
   * CbyServiceManager:session-bus:
   *
   * The #GDBusConnection for the user session bus.
   * This is never %NULL unless g_initable_init() failed.
   */
  g_object_class_install_property (object_class, PROP_SESSION_BUS,
      g_param_spec_object (
          "session-bus", "D-Bus session bus",
          "SessionBus", G_TYPE_DBUS_CONNECTION,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  /**
   * CbyServiceManager:entry-point-index:
   *
   * The #CbyEntryPointIndex associated with the service manager.
   * This is never %NULL unless g_initable_init() failed.
   */
  g_object_class_install_property (object_class, PROP_ENTRY_POINT_INDEX,
      g_param_spec_object (
          "entry-point-index", "EntryPointIndex",
          "EntryPointIndex", CBY_TYPE_ENTRY_POINT_INDEX,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
initable_iface_init (GInitableIface *initable_iface)
{
  initable_iface->init = cby_service_manager_initable_init;
}

CbyServiceManager *
_cby_service_manager_new (GDBusConnection *session_bus,
                          CbyEntryPointIndex *entry_point_index,
                          GError **error)
{
  g_return_val_if_fail (CBY_IS_ENTRY_POINT_INDEX (entry_point_index), NULL);

  return g_initable_new (CBY_TYPE_SERVICE_MANAGER, NULL, error,
                         "session-bus", session_bus,
                         "entry-point-index", entry_point_index,
                         NULL);
}

gboolean
_cby_service_manager_has_service (CbyServiceManager *self,
                                  CbyEntryPoint *entry_point)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (CBY_IS_ENTRY_POINT (entry_point), FALSE);

  return cby_service_manager_get_service_for_entry_point (self,
                                                          entry_point) != NULL;
}

const gchar *
_cby_service_manager_get_service_name (CbyServiceManager *self,
                                       CbyEntryPoint *entry_point)
{
  ServiceInfo *service_info;

  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), NULL);
  g_return_val_if_fail (CBY_IS_ENTRY_POINT (entry_point), NULL);

  service_info =
    cby_service_manager_get_service_for_entry_point (self, entry_point);
  g_return_val_if_fail (service_info != NULL, NULL);

  return service_info->service_name;
}

static void
service_info_started (ServiceInfo *service_info,
                      GPid pid)
{
  g_autoptr (ObjectList) start_tasks =
    g_steal_pointer (&service_info->start_tasks);
  GList *iter;

  DEBUG ("Service \"%s\" started with pid %u", service_info->service_name, pid);

  service_info->pid = pid;
  start_tasks = g_list_reverse (start_tasks);

  for (iter = start_tasks; iter != NULL; iter = iter->next)
    g_task_return_int (iter->data, pid);
}

/*
 * @self: The service manager
 * @entry_point: The service that failed
 * @task: If not %NULL, an applicable start task. If this has not failed
 *  and is not in service_info->start_tasks, something has gone wrong.
 * @service_info: The data structure corresponding to @entry_point
 * @stop_service: If %TRUE, forcibly terminate @entry_point
 * @error: The error that caused launching @entry_point to fail
 */
static void
service_startup_failed (CbyServiceManager *self,
                        GTask *task,
                        ServiceInfo *service_info,
                        gboolean stop_service,
                        const GError *error)
{
  GList *iter;
  g_autoptr (ObjectList) start_tasks = NULL;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));
  g_return_if_fail (task == NULL || G_IS_TASK (task));
  g_return_if_fail (error != NULL || (task != NULL && g_task_had_error (task)));
  g_return_if_fail (service_info != NULL);

  start_tasks = g_steal_pointer (&service_info->start_tasks);

  start_tasks = g_list_reverse (start_tasks);

  for (iter = start_tasks; iter != NULL; iter = iter->next)
    {
      if (task == (GTask *) iter->data)
        task = NULL;

      g_warn_if_fail (!g_task_had_error (iter->data));
      g_task_return_error (iter->data, g_error_copy (error));
    }

  service_info->pid = 0;

  if (task != NULL && !g_task_had_error (task))
    {
      WARNING ("Start task %p was not listed in service \"%s\" start tasks; "
               "failing it anyway",
               task, service_info->service_name);
      g_task_return_error (task, g_error_copy (error));
    }

  if (stop_service)
    ensure_service_stopped (self, service_info);
}

static void
get_service_pid_cb (GObject *source_object,
                    GAsyncResult *result,
                    gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  CbyServiceManager *self = g_task_get_source_object (task);
  ServiceInfo *service_info = g_task_get_task_data (task);

  g_return_if_fail (service_info != NULL);

  if (g_task_had_error (task))
    {
      /* This can happen if the entry point get removed before we get here or
       * if we got PropertiesChanged indicating the service already stopped.
       * In this case all the start tasks will have already failed, so this
       * GError shouldn't get used for anything. */
      g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                   "Internal error: all tasks for \"%s\" should already "
                   "have failed", service_info->service_name);
      service_startup_failed (self, NULL, service_info, TRUE, error);
      return;
    }

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      WARNING ("Failed to start service \"%s\": %s",
               service_info->service_name, error->message);
      service_startup_failed (self, task, service_info, TRUE, error);
    }
  else
    {
      g_autoptr (GVariant) inner;
      guint32 pid;

      g_variant_get (tuple, "(v)", &inner);
      if (!g_variant_is_of_type (inner, G_VARIANT_TYPE_UINT32))
        {
          WARNING ("Invalid signature \"%s\" (expected: \"u\") received when retrieving the MainPID property of service \"%s\"",
                   g_variant_get_type_string (inner),
                   service_info->service_name);
          g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                       "Failed to start service \"%s\": invalid PID received", service_info->service_name);
          service_startup_failed (self, task, service_info, TRUE, error);
          return;
        }

      pid = g_variant_get_uint32 (inner);

      if (pid > G_MAXINT)
        {
          WARNING ("MainPID property of service \"%s\" returned as %u but that makes no sense",
                   service_info->service_name, pid);
          g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                       "Failed to start service \"%s\": invalid PID received", service_info->service_name);
          service_startup_failed (self, task, service_info, TRUE, error);
          return;
        }

      /* pid is known to be in the range 0 to G_MAXINT inclusive now,
       * so this cast is non-truncating. */
      service_info_started (service_info, (int) pid);
      /* We should have no more start tasks, we completed them all */
      g_assert (service_info->start_tasks == NULL);
    }
}

static void
get_service_pid (CbyServiceManager *self,
                 ServiceInfo *service_info,
                 GTask *task)
{
  GVariant *args; /* floating */

  /* Floating reference, consumed by g_dbus_connection_call below */
  args = g_variant_new ("(ss)", SYSTEMD_IFACE_SERVICE, "MainPID");

  /* Get the MainPID property for this service */
  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          service_info->dbus_path,
                          DBUS_IFACE_PROPERTIES,
                          "Get",
                          args,
                          G_VARIANT_TYPE ("(v)"),
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          get_service_pid_cb,
                          g_object_ref (task));
}

static void
get_service_dbus_path_and_pid_cb (GObject *source_object,
                                  GAsyncResult *result,
                                  gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  CbyServiceManager *self = g_task_get_source_object (task);
  ServiceInfo *service_info = g_task_get_task_data (task);

  g_return_if_fail (service_info != NULL);

  if (g_task_had_error (task))
    {
      /* This can happen if the entry point get removed before we get here or
       * if we got PropertiesChanged indicating the service already stopped.
       * In this case all the start tasks will have already failed, so this
       * GError shouldn't get used for anything. */
      g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                   "Internal error: all tasks for \"%s\" should already "
                   "have failed", service_info->service_name);
      service_startup_failed (self, NULL, service_info, TRUE, error);
      return;
    }

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      WARNING ("Failed to start service \"%s\": %s",
               service_info->service_name, error->message);
      service_startup_failed (self, task, service_info, TRUE, error);
    }
  else
    {
      g_autofree gchar *service_dbus_path = NULL;

      g_variant_get (tuple, "(o)", &service_dbus_path);

      g_clear_pointer (&service_info->dbus_path, g_free);
      service_info->dbus_path = g_steal_pointer (&service_dbus_path);

      /* Now that we have the service dbus path we can do another round to
       * retrieve the pid
       */
      get_service_pid (self, service_info,
                       g_steal_pointer (&task));
    }
}

static void
get_service_dbus_path_and_pid (CbyServiceManager *self,
                               const gchar *service_name,
                               GTask *task)
{
  ServiceInfo *service_info = g_task_get_task_data (task);
  g_autoptr (GError) error = NULL;

  g_return_if_fail (service_info != NULL);

  if (g_task_had_error (task))
    {
      /* This can happen if the entry point get removed before we get here.
       * In this case all the start tasks will have already failed, so this
       * GError shouldn't get used for anything. */
      g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                   "Internal error: all tasks for \"%s\" should already "
                   "have failed", service_info->service_name);
      service_startup_failed (self, NULL, service_info, TRUE, error);
      return;
    }

  if (service_info->dbus_path)
    {
      /* We already got the dbus path for this service, do another
       * round to retrieve the pid
       */
      get_service_pid (self, service_info, task);
    }
  else
    {
      GVariant *args; /* floating */

      /* Ok, we don't have the service dbus path cached, lets request it first */

      /* Floating reference, consumed by g_dbus_connection_call below */
      args = g_variant_new ("(s)", service_name);

      /* Get the dbus path for this service */
      g_dbus_connection_call (self->session_bus,
                              SYSTEMD_BUS_NAME,
                              SYSTEMD_PATH_MANAGER,
                              SYSTEMD_IFACE_MANAGER,
                              "GetUnit",
                              args,
                              G_VARIANT_TYPE ("(o)"),
                              G_DBUS_CALL_FLAGS_NONE,
                              -1,
                              g_task_get_cancellable (task),
                              get_service_dbus_path_and_pid_cb,
                              g_object_ref (task));
    }
}

static gboolean
emit_service_stopped (gpointer user_data)
{
  ServiceStoppedSignalData *stopped_data = user_data;

  g_signal_emit (stopped_data->service_manager,
                 cby_service_manager_signals[SIGNAL_SERVICE_STOPPED],
                 0, stopped_data->entry_point);
  service_stopped_signal_data_free (stopped_data);

  /* Remove source */
  return G_SOURCE_REMOVE;
}

static void
properties_changed_cb (GDBusConnection *connection,
                       const gchar *sender_name,
                       const gchar *object_path,
                       const gchar *interface_name,
                       const gchar *signal_name,
                       GVariant *parameters,
                       gpointer user_data)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (user_data);
  const gchar *actual_interface = NULL;
  g_autoptr (GVariant) changed_properties = NULL;
  ServiceInfo *service_info;
  GVariantIter properties_iter;
  gchar *key;
  GVariant *value;

  changed_properties = NULL;

  if (!g_variant_is_of_type (parameters, G_VARIANT_TYPE ("(sa{sv}as)")))
    {
      WARNING ("Invalid parameters signature \"%s\" (expected: \"(sa{sv}as)\") received - ignoring PropertiesChanged signal",
               g_variant_get_type_string (parameters));
      return;
    }
  g_variant_get (parameters, "(&s@a{sv}^a&s)",
                 &actual_interface, &changed_properties, NULL);
  if (g_strcmp0 (actual_interface, SYSTEMD_IFACE_UNIT) != 0)
    return;

  service_info =
    cby_service_manager_get_service_for_object_path (self, object_path);

  if (!service_info)
    {
      /* Ignore ActiveState property change notifications until we know
       * the service dbus path
       */
      return;
    }

  g_variant_iter_init (&properties_iter, changed_properties);
  while (g_variant_iter_next (&properties_iter, "{sv}", &key, &value))
    {
      if (g_strcmp0 (key, "ActiveState") == 0)
        {
          const gchar *active_state;

          if (!g_variant_is_of_type (value, G_VARIANT_TYPE_STRING))
            {
              WARNING ("Invalid ActiveState property signature \"%s\" (expected: \"s\") received - ignoring PropertiesChanged signal",
                       g_variant_get_type_string (value));
              break;
            }
          active_state = g_variant_get_string (value, NULL);
          DEBUG ("ActiveState changed for service \"%s\": %s",
                 service_info->service_name, active_state);

          if (g_strcmp0 (active_state, "inactive") == 0 ||
              g_strcmp0 (active_state, "failed") == 0)
            {
              DEBUG ("Service \"%s\" stopped", service_info->service_name);

              /* If the service has a valid pid it means the startup finished */
              if (service_info->pid)
                {
                  GList *iter;

                  /* Service already started, so let's emit service-stopped on idle to give a chance
                   * for the caller to get it in case the start task just finished
                   */
                  for (iter = service_info->entry_points;
                       iter != NULL;
                       iter = iter->next)
                    {
                      ServiceStoppedSignalData *stopped_data;

                      stopped_data =
                          service_stopped_signal_data_new (self, iter->data);
                      g_idle_add (emit_service_stopped, stopped_data);
                    }

                  /* Unset pid in case the service had already started */
                  service_info->pid = 0;
                }
              /* Otherwise, ensure start task failed if still starting and already marked as failed */
              else if (service_info->start_tasks != NULL && g_strcmp0 (active_state, "failed") == 0)
                {
                  g_autoptr (GError) error = NULL;

                  /* If we had had an error, we would already have cleared out
                   * start_tasks */
                  g_warn_if_fail (!g_task_had_error (service_info->start_tasks->data));

                  /* Reset internals, no need to stop the service as we just
                   * got the info that it stopped :)
                   */
                  g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                               "Failed to start service \"%s\"",
                               service_info->service_name);
                  service_startup_failed (self, NULL, service_info, FALSE,
                                          error);
                }
            }
          g_free (key);
          g_variant_unref (value);
          break;
        }

      g_free (key);
      g_variant_unref (value);
    }
}

static void
job_removed_cb (GDBusConnection *connection,
                const gchar *sender_name,
                const gchar *object_path,
                const gchar *interface_name,
                const gchar *signal_name,
                GVariant *parameters,
                gpointer user_data)
{
  CbyServiceManager *self = CBY_SERVICE_MANAGER (user_data);
  GTask *task = NULL;
  g_autofree gchar *job_path = NULL;
  g_autofree gchar *service_name = NULL;
  g_autofree gchar *job_result = NULL;
  gconstpointer tag;

  if (!g_variant_is_of_type (parameters, G_VARIANT_TYPE ("(uoss)")))
    {
      WARNING ("Invalid parameters signature \"%s\" (expected: \"(uoss)\") received - ignoring JobRemoved signal",
               g_variant_get_type_string (parameters));
      return;
    }
  g_variant_get (parameters, "(uoss)",
                 NULL /* numeric ID */,
                 &job_path,
                 &service_name, /* primary unit name for this job */
                 &job_result);

  task = g_hash_table_lookup (self->pending_jobs, job_path);
  if (!task)
    {
      /* This job wasn't started by us, ignoring */
      return;
    }

  tag = g_task_get_source_tag (task);

  if (g_strcmp0 (job_result, "done") == 0)
    {
      if (tag == _cby_service_manager_start_service_async)
        {
          /* Job started, do another round to get the service PID */
          get_service_dbus_path_and_pid (self, service_name, task);
        }
      else
        {

          g_assert (tag == _cby_service_manager_stop_service_async ||
                    tag == _cby_service_manager_stop_bundle_async);
          g_task_return_boolean (task, TRUE);
        }
    }
  else
    {
      if (tag == _cby_service_manager_start_service_async)
        {
          g_autoptr (GError) error = NULL;
          ServiceInfo *service_info = g_task_get_task_data (task);

          g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                       "Failed to start service \"%s\": %s", service_name,
                       job_result);
          service_startup_failed (self, task, service_info, TRUE, error);
        }
      else
        {
          g_autoptr (GError) error = NULL;

          g_assert (tag == _cby_service_manager_stop_service_async ||
                    tag == _cby_service_manager_stop_bundle_async);
          g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                       "Failed to stop unit \"%s\": %s",
                       service_name, job_result);
          g_task_return_error (task, g_steal_pointer (&error));
        }
    }
  /* Removing from hash unrefs the task */
  g_hash_table_remove (self->pending_jobs, job_path);
}

static void
ensure_change_notification_subscription (CbyServiceManager *self)
{
  if (!self->properties_changed_id)
    {
      /* Listen globally and filter in properties_changed_cb */
      self->properties_changed_id = g_dbus_connection_signal_subscribe (self->session_bus,
                                                                        SYSTEMD_BUS_NAME,
                                                                        DBUS_IFACE_PROPERTIES,
                                                                        "PropertiesChanged",
                                                                        NULL,
                                                                        SYSTEMD_IFACE_UNIT,
                                                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                                                        properties_changed_cb,
                                                                        self,
                                                                        NULL);
    }

  if (!self->job_removed_id)
    {
      /* Listen globally and filter in job_removed_cb */
      self->job_removed_id = g_dbus_connection_signal_subscribe (self->session_bus,
                                                                 SYSTEMD_BUS_NAME,
                                                                 SYSTEMD_IFACE_MANAGER,
                                                                 "JobRemoved",
                                                                 SYSTEMD_PATH_MANAGER,
                                                                 NULL,
                                                                 G_DBUS_SIGNAL_FLAGS_NONE,
                                                                 job_removed_cb,
                                                                 self,
                                                                 NULL);
    }
}

static void
start_service_cb (GObject *source_object,
                  GAsyncResult *result,
                  gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  CbyServiceManager *self = g_task_get_source_object (task);
  ServiceInfo *service_info = g_task_get_task_data (task);

  g_return_if_fail (service_info != NULL);

  if (g_task_had_error (task))
    {
      /* This can happen if the entry point get removed before we get here.
       * In this case all the start tasks will have already failed, so this
       * GError shouldn't get used for anything. */
      g_set_error (&error, CBY_ERROR, CBY_ERROR_FAILED,
                   "Internal error: all tasks for \"%s\" should already "
                   "have failed", service_info->service_name);
      service_startup_failed (self, NULL, service_info, TRUE, error);
      return;
    }

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      /* No need to stop service here because if we get here during startup,
       * it means StartUnit failed. */
      service_startup_failed (self, task, service_info, FALSE, error);
    }
  else
    {
      gchar *job_path = NULL;

      g_variant_get (tuple, "(o)", &job_path);

      /* Add to map of pending jobs - to be processed on job_removed_cb.
       *
       * This code assumes that the result of Start/StopUnit is always returned
       * before JobRemoved is signalled for the corresponding job.
       */
      g_hash_table_insert (self->pending_jobs, job_path,
                           g_steal_pointer (&task));
    }
}

/*
 * The following diagram describes the state machine for the service startup:
 *
 * initial idle state, pid == 0, start_tasks == NULL, no job in pending_jobs
 *           |
 *           | call StartUnit(), set start_tasks
 *           v
 * waiting for StartUnit() result, start_tasks != NULL
 *           |\
 *   success | \--> failure: go to initial state
 *           v
 * waiting for JobRemoved, start_tasks != NULL, job in pending_jobs
 *           |\
 *  success: | \--> failure: remove job, go to initial state
 *  remove   |
 *    job    |
 *          / \
 *  already/   \ call GetUnit
 *   know /     \
 *   path |   waiting for GetUnit reply, start_tasks != NULL
 *        \     /\
 *         \   /  \--> failure: go to initial state
 *          \ /
 *           | call Get() for PID
 *           |
 *    waiting for PID, start_tasks != NULL
 *           |\
 *  success, | \--> failure: go to initial state
 *  set pid  |
 *  != 0     |
 *           v
 * service is running, pid != 0, start_tasks == NULL,
 *
 * If we see that the service exited unsuccessfully from any state above, we
 * go back to initial state (reset start_tasks, clear the pid).
 *
 * Apart from that, we also early return before calling StartUnit if we already
 * know the pid beforehand (the service was already started by us and we didn't receive
 * a notification that it stopped).
 *
 * If a service that has reached the "service is running" state stops,
 * we emit ::service-stopped.
 */
static void
start_service (CbyServiceManager *self,
               ServiceInfo *service_info,
               GCancellable *cancellable,
               GAsyncReadyCallback callback,
               gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GVariant *args; /* floating */

  ensure_change_notification_subscription (self);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _cby_service_manager_start_service_async);
  g_task_set_task_data (task, service_info_ref (service_info),
                        (GDestroyNotify) service_info_unref);

  if (service_info->pid)
    {
      /* Service already started by us */
      g_task_return_int (task, service_info->pid);
      return;
    }

  if (service_info->start_tasks == NULL)
    {
      /* @task is the first to want to start this service, so it has to
       * actually do the work, and not just freeload on a previous element
       * of start_tasks. */

      /* Floating reference, consumed by g_dbus_connection_call below */
      args = g_variant_new ("(ss)", service_info->service_name, "replace");

      g_dbus_connection_call (self->session_bus,
                              SYSTEMD_BUS_NAME,
                              SYSTEMD_PATH_MANAGER,
                              SYSTEMD_IFACE_MANAGER,
                              "StartUnit",
                              args,
                              G_VARIANT_TYPE ("(o)"),
                              G_DBUS_CALL_FLAGS_NONE,
                              -1,
                              g_task_get_cancellable (task),
                              start_service_cb,
                              g_object_ref (task));
    }

  service_info->start_tasks = g_list_prepend (service_info->start_tasks,
                                              g_steal_pointer (&task));
}

/*
 * Async callback for any StopUnit() call with a GTask as user_data
 */
static void
stop_unit_cb (GObject *source_object,
              GAsyncResult *result,
              gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;
  CbyServiceManager *self = g_task_get_source_object (task);

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
    }
  else
    {
      gchar *job_path = NULL;

      g_variant_get (tuple, "(o)", &job_path);

      /* Add to map of pending jobs - to be processed on job_removed_cb.
       *
       * This code assumes that the result of StopUnit is always returned
       * before JobRemoved is signalled for the corresponding job.
       */
      g_hash_table_insert (self->pending_jobs, job_path,
                           g_steal_pointer (&task));
    }
}

static void
stop_service_async (CbyServiceManager *self,
                    ServiceInfo *service_info,
                    GCancellable *cancellable,
                    GAsyncReadyCallback callback,
                    gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GVariant *args; /* floating */

  ensure_change_notification_subscription (self);

  /* Floating reference, consumed by g_dbus_connection_call below */
  args = g_variant_new ("(ss)", service_info->service_name, "replace");

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _cby_service_manager_stop_service_async);
  g_task_set_task_data (task, service_info_ref (service_info),
                        (GDestroyNotify) service_info_unref);

  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "StopUnit",
                          args,
                          G_VARIANT_TYPE ("(o)"),
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          stop_unit_cb,
                          g_object_ref (task));
}

static void
kill_service_cb (GObject *source_object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
kill_service (CbyServiceManager *self,
              const gchar *service_name,
              gint signum,
              gpointer source_tag,
              GCancellable *cancellable,
              GAsyncReadyCallback callback,
              gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GVariant *args; /* floating */

  /* Floating reference, consumed by g_dbus_connection_call below */
  args = g_variant_new ("(ssi)", service_name, "all", signum);

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, source_tag);

  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "KillUnit",
                          args,
                          G_VARIANT_TYPE_UNIT,
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          kill_service_cb,
                          g_object_ref (task));
}

void
_cby_service_manager_start_service_async (CbyServiceManager *self,
                                          CbyEntryPoint *entry_point,
                                          GCancellable *cancellable,
                                          GAsyncReadyCallback callback,
                                          gpointer user_data)
{
  ServiceInfo *service_info;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));
  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));

  service_info =
    cby_service_manager_get_service_for_entry_point (self, entry_point);
  g_return_if_fail (service_info != NULL);

  start_service (self, service_info,
                 cancellable, callback, user_data);
}

GPid
_cby_service_manager_start_service_finish (CbyServiceManager *self,
                                           GAsyncResult *result,
                                           GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_start_service_async),
                        FALSE);

  return g_task_propagate_int (G_TASK (result), error);
}

void
_cby_service_manager_stop_service_async (CbyServiceManager *self,
                                         CbyEntryPoint *entry_point,
                                         GCancellable *cancellable,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  ServiceInfo *service_info;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));
  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));

  service_info =
    cby_service_manager_get_service_for_entry_point (self, entry_point);
  g_return_if_fail (service_info != NULL);

  stop_service_async (self, service_info, cancellable, callback, user_data);
}

gboolean
_cby_service_manager_stop_service_finish (CbyServiceManager *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_stop_service_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
_cby_service_manager_stop_bundle_async (CbyServiceManager *self,
                                        const gchar *bundle_id,
                                        GCancellable *cancellable,
                                        GAsyncReadyCallback callback,
                                        gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autofree gchar *unit_name = NULL;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _cby_service_manager_stop_bundle_async);

  /* All services for bundle com.example.Foo are placed in
   * com.example.Foo.slice. We don't need to worry about whether the slice
   * is empty, or even whether it exists at all, because StopUnit() on a
   * nonexistent slice just returns a job that is immediately removed
   * as "done". */
  unit_name = g_strdup_printf ("%s.slice", bundle_id);
  ensure_change_notification_subscription (self);
  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "StopUnit",
                          g_variant_new ("(ss)", unit_name, "replace"),
                          G_VARIANT_TYPE ("(o)"),
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          stop_unit_cb,
                          g_object_ref (task));
}

gboolean
_cby_service_manager_stop_bundle_finish (CbyServiceManager *self,
                                         GAsyncResult *result,
                                         GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_stop_bundle_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
_cby_service_manager_signal_service_async (CbyServiceManager *self,
                                           CbyEntryPoint *entry_point,
                                           gint signum,
                                           GCancellable *cancellable,
                                           GAsyncReadyCallback callback,
                                           gpointer user_data)
{
  ServiceInfo *service_info;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));
  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));

  service_info =
    cby_service_manager_get_service_for_entry_point (self, entry_point);
  g_return_if_fail (service_info != NULL);

  kill_service (self, service_info->service_name, signum,
                _cby_service_manager_signal_service_async,
                cancellable, callback, user_data);
}

gboolean
_cby_service_manager_signal_service_finish (CbyServiceManager *self,
                                            GAsyncResult *result,
                                            GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_signal_service_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
start_agents_cb (GObject *source_object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  g_autoptr (GError) error = NULL;
  g_autoptr (GVariant) tuple = NULL;

  tuple = g_dbus_connection_call_finish (G_DBUS_CONNECTION (source_object),
                                         result, &error);

  if (tuple == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  /* We don't record the job object, because it doesn't track the status of
   * individual services, and if anything failed, systemd is good at putting
   * the relevant information in the Journal. */
  g_task_return_boolean (task, TRUE);
}

void
_cby_service_manager_start_agents_async (CbyServiceManager *self,
                                         GCancellable *cancellable,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (CBY_IS_SERVICE_MANAGER (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _cby_service_manager_start_agents_async);

  g_dbus_connection_call (self->session_bus,
                          SYSTEMD_BUS_NAME,
                          SYSTEMD_PATH_MANAGER,
                          SYSTEMD_IFACE_MANAGER,
                          "StartUnit",
                          g_variant_new ("(ss)", "canterbury-agents.target",
                                         "replace"),
                          G_VARIANT_TYPE ("(o)"),
                          G_DBUS_CALL_FLAGS_NONE,
                          -1,
                          g_task_get_cancellable (task),
                          start_agents_cb,
                          g_object_ref (task));
}

gboolean
_cby_service_manager_start_agents_finish (CbyServiceManager *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  _cby_service_manager_start_agents_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

CbyEntryPointIndex *
_cby_service_manager_get_entry_point_index (CbyServiceManager *self)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), NULL);

  return self->entry_point_index;
}

GDBusConnection *
_cby_service_manager_get_session_bus (CbyServiceManager *self)
{
  g_return_val_if_fail (CBY_IS_SERVICE_MANAGER (self), NULL);

  return self->session_bus;
}
