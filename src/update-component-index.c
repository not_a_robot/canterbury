/*
 * canterbury-update-component-index: update app-bundle metadata cache
 *
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <appstream-glib.h>
#include <gio/gio.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>

#include "canterbury/messages-internal.h"
#include "canterbury/platform/component-internal.h"
#include "canterbury/platform/component-index-internal.h"

#include "util.h"

static gboolean opt_platform = FALSE;
static const gchar *opt_root = NULL;

static GOptionEntry entries[] = {
  { "platform", 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &opt_platform,
    "Act on platform components and built-in app-bundles (default: act "
    "on store app-bundles)",
    NULL },
  /* Mainly for use by the regression tests. We could conceivably want
   * to use it to index the contents of a root filesystem DESTDIR or
   * a chroot/container in future, in which case it should be made
   * non-hidden. */
  { "root", 0, G_OPTION_FLAG_HIDDEN, G_OPTION_ARG_FILENAME, &opt_root,
    "Path prefix containing /usr and so on", NULL },
  { NULL }
};

/*
 * Clear *errorp if it represents "file exists".
 */
static gboolean
ignore_eexist (GError **errorp)
{
  if (errorp == NULL || *errorp == NULL)
    return FALSE;

  if (g_error_matches (*errorp, G_FILE_ERROR, G_FILE_ERROR_EXIST) ||
      g_error_matches (*errorp, G_IO_ERROR, G_IO_ERROR_EXISTS))
    {
      g_clear_error (errorp);
      return TRUE;
    }

  return FALSE;
}

static GHashTable *
map_id_to_app (GPtrArray *apps)
{
  g_autoptr (GHashTable) ret = NULL;
  guint i;

  ret = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);

  for (i = 0; i < apps->len; i++)
    {
      AsApp *app = g_ptr_array_index (apps, i);
      const gchar *id = as_app_get_id (app);

      if (G_UNLIKELY (id == NULL))
        {
          WARNING ("Found an AsApp with no ID");
          continue;
        }

      g_hash_table_replace (ret, g_strdup (id), g_object_ref (app));
    }

  return g_steal_pointer (&ret);
}

int
main (int argc, char **argv)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) root_file = NULL;
  g_autoptr (GFile) dest_file = NULL;
  g_autoptr (GFile) parent_file = NULL;
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (AsStore) store = NULL;
  g_autoptr (GDir) dir = NULL;
  g_autoptr (GHashTable) apps = NULL;
  g_autofree gchar *dest_path = NULL;
  g_autofree gchar *prefix = NULL;
  g_autofree gchar *extensions_apps_path = NULL;
  g_autofree gchar *extensions_icons_path = NULL;
  gint error_exit_status = 1;
  gint priority;
  const gchar *name;
  const gchar *root = "/";
  const gchar *update_desktop_database_argv[] = {
    "update-desktop-database",
    NULL, /* extensions_apps_path */
    NULL
  };
  const guint update_desktop_database_argv_last = 1;
  const gchar *update_icon_cache_argv[] = {
    "gtk-update-icon-cache",
    "--ignore-theme-index",
    NULL, /* extensions_icon_theme_path */
    NULL
  };
  const guint update_icon_cache_argv_last = 2;
  gint exit_status;
  GHashTableIter iter;
  gpointer k, v;

  _cby_setenv_disable_services ();

  /* Make sure we are only looking in the normal places for platform apps */
  g_setenv ("XDG_CONFIG_DIRS", "/etc/xdg", TRUE);
  g_setenv ("XDG_CONFIG_HOME", "/nonexistent", TRUE);
  g_setenv ("XDG_DATA_DIRS", "/usr/share", TRUE);
  g_setenv ("XDG_DATA_HOME", "/nonexistent", TRUE);

  /* Some appstream-glib functions have the side-effect of looking up our
   * home directory, but when invoked by Ribchester, $HOME might not be
   * set. We don't have AppArmor permissions to access /etc/passwd (we should
   * never need it) and GLib fails horribly in that situation. */
  g_setenv ("HOME", "/nonexistent", TRUE);

  context = g_option_context_new ("- update app-bundle metadata cache");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      error_exit_status = 2;
      goto error;
    }

  store = as_store_new ();
  /* Disable monitoring: we don't need it. */
  as_store_set_watch_flags (store, AS_STORE_WATCH_FLAG_NONE);

  if (opt_root != NULL)
    {
      root = opt_root;
      as_store_set_destdir (store, opt_root);
    }

  root_file = g_file_new_for_path (root);

  if (opt_platform)
    {
      prefix = g_build_filename (root, CBY_PATH_PREFIX_BUILT_IN_BUNDLE, NULL);
      dest_path = g_build_filename (root, CBY_APPSTREAM_CACHE_PLATFORM, NULL);
      priority = CBY_APPSTREAM_PRIORITY_PLATFORM;

      /* Load platform app-bundles */
      if (!as_store_load (
              store, (AS_STORE_LOAD_FLAG_APPDATA | AS_STORE_LOAD_FLAG_DESKTOP),
              NULL, &error))
        {
          g_prefix_error (&error,
                          "Unable to load metainfo and desktop entries: ");
          goto error;
        }
    }
  else
    {
      prefix = g_build_filename (root, CBY_PATH_PREFIX_STORE_BUNDLE, NULL);

      dest_path =
          g_build_filename (root, CBY_APPSTREAM_CACHE_INSTALLED_STORE, NULL);
      priority = CBY_APPSTREAM_PRIORITY_INSTALLED_STORE;
    }

  dir = g_dir_open (prefix, 0, &error);

  if (dir == NULL &&
      !_cby_ignore_enoent (&error))
    {
      g_prefix_error (&error, "Unable to open \"%s\" for reading: ", prefix);
      goto error;
    }

  for (name = dir ? g_dir_read_name (dir) : NULL;
       name != NULL;
       name = g_dir_read_name (dir))
    {
      g_autoptr (AsApp) fallback_app = NULL;
      g_autofree gchar *path = NULL;
      g_autofree gchar *main_entry_point = NULL;
      AsApp *main_app = NULL; /* unowned */

      INFO ("Inspecting \"%s/%s\"", prefix, name);

      if (!cby_is_bundle_id (name))
        continue;

      path = g_build_filename (prefix, name, "share", "metainfo", NULL);

      /* as_store_load_path already uses the @destdir, opt_root, if needed */
      if (!as_store_load_path (store, path, NULL, &error) &&
          !_cby_ignore_enoent (&error))
        {
          g_prefix_error (&error, "Unable to load from \"%s/%s\": ",
                          opt_root == NULL ? "" : opt_root, path);
          WARNING ("%s", error->message);
          g_clear_error (&error);
        }

      path = g_build_filename (prefix, name, "share", "applications", NULL);

      if (!as_store_load_path (store, path, NULL, &error) &&
          !_cby_ignore_enoent (&error))
        {
          g_prefix_error (&error, "Unable to load from \"%s/%s\": ",
                          opt_root == NULL ? "" : opt_root, path);
          WARNING ("%s", error->message);
          g_clear_error (&error);
        }

      main_entry_point = g_strdup_printf ("%s.desktop", name);

      main_app = as_store_get_app_by_id (store, name);

      if (main_app == NULL)
        main_app = as_store_get_app_by_id (store, main_entry_point);

      /* Make sure there is *something* in the cache, even if a built-in
       * app-bundle has no metadata at all. Store app-bundles don't get
       * this behaviour, because directories get created in the store
       * app-bundle location for built-in app-bundles for now, for
       * backwards compatibility with older Ribchester versions. */
      if (opt_platform && main_app == NULL)
        {
          g_autofree gchar *top = g_build_filename (prefix, name, NULL);

          fallback_app = as_app_new ();
          as_app_set_id (fallback_app, name);
          as_app_set_kind (fallback_app, AS_APP_KIND_UNKNOWN);
          as_app_set_source_file (fallback_app, top);
          as_app_set_source_kind (fallback_app, AS_APP_SOURCE_KIND_UNKNOWN);
          as_store_add_app (store, fallback_app);
          main_app = fallback_app;
        }

      if (main_app != NULL)
        {
          g_autoptr (GKeyFile) desktop_file = NULL;
          g_autofree gchar *entry_point_path = NULL;

          /* appstream-glib collects categories from the entry point whose
           * name corresponds to the component (if there is one), but it
           * specifically discards "X-" categories. Put them back, so we can
           * use them as an extension point. */
          entry_point_path = g_build_filename (path, main_entry_point, NULL);
          desktop_file = g_key_file_new ();

          if (g_key_file_load_from_file (desktop_file, entry_point_path,
                                         G_KEY_FILE_NONE, &error))
            {
              g_auto (GStrv) categories = g_key_file_get_string_list (
                  desktop_file, G_KEY_FILE_DESKTOP_GROUP,
                  G_KEY_FILE_DESKTOP_KEY_CATEGORIES, NULL, NULL);

              INFO ("Incorporating any missing categories from \"%s\"",
                    entry_point_path);

              if (categories != NULL)
                {
                  guint i;

                  for (i = 0; categories[i] != NULL; i++)
                    {
                      if (!as_app_has_category (main_app, categories[i]))
                        as_app_add_category (main_app, categories[i]);
                    }
                }
            }
          else if (!_cby_ignore_enoent (&error))
            {
              g_prefix_error (&error, "Unable to load desktop file \"%s\": ",
                              entry_point_path);
              WARNING ("%s", error->message);
              g_clear_error (&error);
            }
        }
    }

  /* We remove all apps from their original AsStore before re-adding those
   * that passed the filter, because in some cases we'll be changing their IDs,
   * which would break the original AsStore's { id => app } index. We
   * have to copy the result of as_store_get_apps() because it's an unowned
   * pointer borrowed from @store, and adding/removing apps directly modifies
   * its contents. Use a map { ID => app } so we can know whether a desired
   * ID was initially in the store. */
  apps = map_id_to_app (as_store_get_apps (store));
  as_store_remove_all (store);

  g_hash_table_iter_init (&iter, apps);

  while (g_hash_table_iter_next (&iter, &k, &v))
    {
      const gchar *id = k;
      AsApp *app = v; /* unowned */
      g_autoptr (CbyProcessInfo) pi = NULL;
      g_autoptr (CbyComponent) component = NULL;
      g_autoptr (GFile) real_source_file = NULL;
      g_autofree gchar *real_source = NULL;
      g_autofree gchar *allowed_prefix = NULL;
      g_autofree gchar *main_entry_point = NULL;
      g_autofree gchar *main_schema_basename = NULL;
      g_autofree gchar *main_schema_path = NULL;
      g_autofree gchar *real_source_without_root = NULL;
      const gchar *source;
      const gchar *bundle_id;
      const gchar *claimed_bundle_id;
      CbyProcessType type;

      source = as_app_get_source_file (app);

      if (source == NULL)
        {
          WARNING ("%s has no source", id);
          continue;
        }

      INFO ("Considering \"%s\"", id);

      if (!as_app_validate (app, (AS_APP_VALIDATE_FLAG_STRICT |
                                  AS_APP_VALIDATE_FLAG_NO_NETWORK),
                            &error))
        {
          g_prefix_error (&error, "\"%s\" is invalid: ", source);
          WARNING ("%s", error->message);
          g_clear_error (&error);
          continue;
        }

      INFO ("Processing \"%s\"...", source);

      real_source = realpath (source, NULL);

      if (real_source == NULL)
        {
          WARNING ("Unable to resolve \"%s\": %s", source, g_strerror (errno));
          continue;
        }

      real_source_file = g_file_new_for_path (real_source);

      if (opt_root != NULL)
        {
          g_autofree gchar *relative =
              g_file_get_relative_path (root_file, real_source_file);

          if (relative == NULL)
            {
              WARNING ("\"%s\" resolves to \"%s\" outside root \"%s\"", source,
                       real_source, opt_root);
              continue;
            }

          real_source_without_root = g_build_filename ("/", relative, NULL);
        }
      else
        {
          real_source_without_root = g_strdup (real_source);
        }

      pi = cby_process_info_new_for_path_and_user (
          real_source_without_root, CBY_PROCESS_INFO_NO_USER_ID);
      type = cby_process_info_get_process_type (pi);
      bundle_id = cby_process_info_get_bundle_id (pi);

      if (opt_platform)
        {
          if (type != CBY_PROCESS_TYPE_PLATFORM &&
              type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
            {
              WARNING ("Found non-platform path \"%s\" while listing platform",
                       real_source);
              continue;
            }
        }
      else
        {
          if (type != CBY_PROCESS_TYPE_STORE_BUNDLE)
            {
              WARNING ("Found non-store path \"%s\" while listing store "
                       "app-bundles",
                       real_source);
              continue;
            }
        }

      switch (type)
        {
        case CBY_PROCESS_TYPE_BUILT_IN_BUNDLE:
        case CBY_PROCESS_TYPE_STORE_BUNDLE:
          /* CbyProcessInfo guarantees this */
          g_assert (bundle_id != NULL);

          if (!cby_is_bundle_id (bundle_id))
            {
              WARNING ("Found invalid bundle ID \"%s\"", bundle_id);
              continue;
            }

          allowed_prefix = g_strdup_printf ("%s.", bundle_id);
          main_entry_point = g_strdup_printf ("%s.desktop", bundle_id);

          if (g_strcmp0 (main_entry_point, id) == 0)
            {
              INFO ("AppStream component \"%s\" matches bundle \"%s\" main "
                    "entry point",
                    id, bundle_id);
            }
          else if (g_strcmp0 (bundle_id, id) == 0)
            {
              INFO ("AppStream component \"%s\" matches bundle ID", id);
            }
          else if (g_str_has_prefix (id, allowed_prefix))
            {
              if (g_hash_table_lookup (apps, bundle_id) != NULL ||
                  g_hash_table_lookup (apps, main_entry_point) != NULL)
                {
                  /* OK, but ignored; e.g. one of Frampton's extra entry points
                   */
                  INFO ("AppStream component \"%s\" is an extra entry point, "
                        "ignoring",
                        id);
                  continue;
                }
              else
                {
                  WARNING ("No metainfo for bundle containing \"%s\"",
                           real_source);
                  continue;
                }
            }
          else
            {
              WARNING ("\"%s\" has inappropriate ID for its app-bundle \"%s\"",
                       id, bundle_id);
              continue;
            }

          as_app_add_metadata (app, CBY_APPSTREAM_X_BUNDLE_ID, bundle_id);

          /* If there is a GSettings schema whose name matches the bundle ID,
           * it is meant to be made available to the system settings UI. */
          main_schema_basename = g_strdup_printf ("%s.gschema.xml", bundle_id);
          main_schema_path = g_build_filename (prefix, bundle_id, "share",
                                               "glib-2.0", "schemas",
                                               main_schema_basename, NULL);
          DEBUG ("Checking for %s", main_schema_path);

          if (g_file_test (main_schema_path, G_FILE_TEST_IS_REGULAR))
            as_app_add_metadata (app, CBY_APPSTREAM_X_SETTINGS_SCHEMA,
                                 bundle_id);
          else
            as_app_remove_metadata (app, CBY_APPSTREAM_X_SETTINGS_SCHEMA);

          /* It's OK to change the app in-place, because it isn't in a store
           * at the moment. */
          as_app_set_id (app, bundle_id);
          as_app_set_priority (app, priority);

          /* This does some checks */
          component = cby_component_new_for_appstream_app (type, app, &error);

          if (component == NULL)
            {
              WARNING ("%s", error->message);
              g_clear_error (&error);
              continue;
            }

          INFO ("Adding app-bundle \"%s\" to cache", bundle_id);
          as_store_add_app (store, app);
          break;

        case CBY_PROCESS_TYPE_PLATFORM:
          /* CbyProcessInfo guarantees this */
          g_assert (bundle_id == NULL);

          claimed_bundle_id =
              as_app_get_metadata_item (app, CBY_APPSTREAM_X_BUNDLE_ID);

          if (claimed_bundle_id != NULL)
            {
              WARNING ("Platform component \"%s\" claims to be in bundle "
                       "\"%s\"",
                       id, claimed_bundle_id);
              continue;
            }

          as_app_set_priority (app, priority);

          /* In theory this could do some checks, although in practice it
           * doesn't do anything significant right now */
          component = cby_component_new_for_appstream_app (type, app, &error);

          if (component == NULL)
            {
              WARNING ("%s", error->message);
              g_clear_error (&error);
              continue;
            }

          INFO ("Adding platform component \"%s\" to cache", id);
          as_store_add_app (store, app);
          break;

        case CBY_PROCESS_TYPE_UNKNOWN:
        default:
          /* can't happen, we already checked that the type is one that is
           * appropriate for the value of opt_platform */
          g_assert_not_reached ();
        }
    }

  dest_file = g_file_new_for_path (dest_path);
  parent_file = g_file_get_parent (dest_file);

  if (!g_file_make_directory_with_parents (parent_file, NULL, &error) &&
      !ignore_eexist (&error))
    {
      g_prefix_error (&error, "Unable to create parents for \"%s\": ",
                      dest_path);
      goto error;
    }

  if (!as_store_to_file (store, dest_file, AS_NODE_TO_XML_FLAG_NONE,
                         NULL, &error))
    {
      g_prefix_error (&error, "Unable to save cache to \"%s\": ", dest_path);
      goto error;
    }

  g_assert (error == NULL);

  /* Update MIME cache database */
  extensions_apps_path = g_build_filename (root, CBY_PATH_SYSTEM_EXTENSIONS, "applications", NULL);
  update_desktop_database_argv[update_desktop_database_argv_last] = extensions_apps_path;

  if (!g_find_program_in_path (update_desktop_database_argv[0]))
    {
      INFO ("%s not installed, skipping update",
            update_desktop_database_argv[0]);
    }
  else if (!g_spawn_sync (NULL,
                     (gchar **) update_desktop_database_argv,
                     NULL,
                     G_SPAWN_SEARCH_PATH,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     &exit_status,
                     &error))
    {
      g_prefix_error (&error, "%s: unable to start \"%s\": \n", g_get_prgname (),
                      update_desktop_database_argv[0]);
      goto error;
    }
  else if (!g_spawn_check_exit_status (exit_status, &error))
    {
      g_prefix_error (&error, "%s: while running \"%s\": \n", g_get_prgname (),
                      update_desktop_database_argv[0]);
      goto error;
    }

  /* Update icon cache */
  extensions_icons_path = g_build_filename (root, CBY_PATH_SYSTEM_EXTENSIONS, "icons", NULL);

  g_clear_pointer (&dir, g_dir_close);

  dir = g_dir_open (extensions_icons_path, 0, &error);
  if (dir == NULL)
    {
      if (_cby_ignore_enoent (&error))
        {
          INFO ("No icon found");
        }
      else
        {
          g_prefix_error (&error, "Unable to open \"%s\" for reading: ", extensions_icons_path);
          goto error;
        }
    }
  else if (!g_find_program_in_path (update_icon_cache_argv[0]))
    {
      INFO ("%s not installed, skipping update", update_icon_cache_argv[0]);
    }
  else
    {
      /* Iterate over CBY_PATH_SYSTEM_EXTENSIONS/icons/
       * searching for themes and call gtk-update-icon-cache on
       * each icon theme found.
       */
      for (name = g_dir_read_name (dir);
           name != NULL;
           name = g_dir_read_name (dir))
        {
          g_autofree gchar *extensions_icon_theme_path = NULL;
          g_autofree gchar *path = NULL;
          const gchar *theme_name;
          GStatBuf stat_buf;

          path = g_build_filename (extensions_icons_path, name, NULL);
          if (g_lstat (path, &stat_buf) == -1 || !S_ISDIR (stat_buf.st_mode))
            continue;

          theme_name = name;

          extensions_icon_theme_path = g_build_filename (extensions_icons_path, theme_name, NULL);
          update_icon_cache_argv[update_icon_cache_argv_last] = extensions_icon_theme_path;
          if (!g_spawn_sync (NULL,
                             (gchar **) update_icon_cache_argv,
                             NULL,
                             G_SPAWN_SEARCH_PATH,
                             NULL,
                             NULL,
                             NULL,
                             NULL,
                             &exit_status,
                             &error))
            {
              g_prefix_error (&error, "%s: unable to start \"%s\": \n", g_get_prgname (),
                              update_icon_cache_argv[0]);
              goto error;
            }
          else if (!g_spawn_check_exit_status (exit_status, &error))
            {
              g_prefix_error (&error, "%s: while running \"%s\": \n", g_get_prgname (),
                              update_icon_cache_argv[0]);
              goto error;
            }
        }
    }

  return 0;

error:
  g_printerr ("%s\n", error->message);
  return error_exit_status;
}
