/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __AUDIO_MGR__
#define __AUDIO_MGR__

#include <pulse/context.h>
#include <pulse/ext-stream-restore.h>
#include <pulse/glib-mainloop.h>
#include <pulse/pulseaudio.h>

#include "activity_mgr_if.h"
#include "app_mgr_if.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "launch_mgr_if.h"

G_BEGIN_DECLS

#define DECAY_STEP .04

typedef struct _AudioMgrPriv AudioMgrPriv ;
typedef struct _AudioAppEntryInfo AudioAppEntryInfo ;
typedef struct _AudioEntry AudioEntry ;
typedef struct _ExternalAudioEntry ExternalAudioEntry;

struct _AudioAppEntryInfo
{
  gchar*  pAppName;
  gchar*  PlayingTitle;
  gchar*  PlayingArtist;
  gint    uinAudioResourceType;
  gint    uinAudioStatus;
  guint32 uinSinkIndex;
};

struct _AudioMgrPriv
{
	GList*            appEntriesDB;
	AudioAppEntryInfo RequestedAppInfo;
	pa_context*       pCtxt;
  pa_glib_mainloop* pMainLoop;
  GList*            pExtAudioEntryList;
  GList*            pAudioStack;
  GHashTable*       priority_hash_table;
  guint             uinAudioResourceType;
  guint32           uinSinkIndex;
  pa_stream*        stream;
  double            lastPeak; // The last peak_value for the Volume meter
};

struct _AudioEntry
{
  guint   uinAudioResourceType;
  gchar*  pAudioChannelName;
  gchar*  AppName;
};

struct _ExternalAudioEntry
{
  gchar*  AppName;
  guint32 App_index;
  guint32 uinSinkIndex;
  gint    mute_status;
};

void                   v_initialize_pulse_audio_subscription (void);
void                   v_send_external_app_signal( guint32 uinSinkIndex , guint audio_signal );
void v_external_app_audio_invoke_clb (const gchar *pAppName,
    const gchar *pMethodName,
    guint32 uinSinkIndex);
void                   v_audio_pitch_signal( gdouble pitch );

G_END_DECLS

#endif
