/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "apparmor.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <gio/gio.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <glib/gstdio.h>

#include <canterbury/bundle.h>
#include "canterbury/messages-internal.h"
#include "src/util.h"

/* assumed to be the same on all AppArmor installations */
#define APPARMOR_PARSER "/sbin/apparmor_parser"

/*
 * CbyAppArmorStore:
 * @path: the root directory of this AppArmor store
 * @apparmor_d: @path, opened for reading
 * @cache: the `cache` subdirectory of @path, opened for reading
 * @lock: an empty lock file `.lock` in @path, opened for writing and
 *  exclusively locked to prevent other processes from opening
 *  another #CbyAppArmorStore for the same directory
 *
 * Open a directory that stores AppArmor profiles, and lock it exclusively.
 *
 * The directory's structure mimics `/etc/apparmor.d`, but is restricted to
 * Canterbury [store application bundles]. The textual source
 * code of AppArmor profiles is stored directly inside @path, in files named
 * `Applications.com.example.Foo` for the profile
 * <code>/Applications/com.example.Foo/\*\*</code> belonging to the store
 * application bundle `com.example.Foo`.
 * Compiled binary representations of those files are stored in the `cache`
 * subdirectory of @path, with names that match the source code filenames.
 *
 * In addition, in the same way as Apertis' `/etc/apparmor.d`, there is an
 * "ubercache" in `cache/.ubercache`. The ubercache is simply the
 * concatenation of all the compiled binary representations, with one
 * exception: an entirely empty ubercache is assumed to represent an
 * invalidated cache, which will be rectified by rebuilding the ubercache
 * from individual profiles at the next opportunity. (This assumes that
 * the cost of rebuilding a cache containing no profiles is negligible.)
 *
 * The names of AppArmor profiles in this store must not collide with the
 * names of AppArmor profiles in `/etc/apparmor.d`. This is not checked by
 * this module. In Apertis, this is ensured by naming all AppArmor profiles
 * to match a filesystem path (as is conventional), and not placing any
 * AppArmor profiles starting with `/Applications/` in `/etc/apparmor.d`.
 *
 * For more details see
 * https://docs.apertis.org/latest/applications.html#apparmor-profiles
 *
 * [Store application bundles]: https://docs.apertis.org/latest/applications.html#software-categories
 */
struct _CbyAppArmorStore
{
  gchar *path;
  CbyFileDescriptor apparmor_d;
  CbyFileDescriptor cache;
  CbyFileDescriptor lock;
};

typedef enum
{
  CBY_APPARMOR_LOAD_FLAGS_NONE = 0,
  CBY_APPARMOR_LOAD_FLAGS_READ_CACHE = (1 << 0),
  CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE = (1 << 1),
} CbyAppArmorLoadFlags;

/*
 * Replace @profile_name with a profile that can be signalled (killed) by
 * privileged processes, and cannot do anything else.
 *
 * We do this instead of removing the profile, because any surviving
 * processes whose profiles are removed become unconfined, which would be
 * very bad.
 *
 * Specifically, we allow it to be killed by:
 *
 * - unconfined processes, in particular systemd --user (in the "killing
 *   spree" during session teardown, or when Canterbury asks it to stop a
 *   service) or systemd-as-pid-1 (in the "killing spree" during system
 *   shutdown if user-level processes have missed any app-bundle processes)
 *
 * - Canterbury (but only the full-functionality version that supports
 *   legacy launching code paths) during session teardown
 *
 * - Ribchester, in case we ever want to make that system process responsible
 *   for terminating app-bundles
 */
gboolean
_cby_apparmor_load_stub_profile (const gchar *profile_name,
                                 GError **error)
{
  g_autofree gchar *profile_text = NULL;

  profile_text = g_strdup_printf ("%s {\n"
                                  "  signal receive peer=unconfined,\n"
                                  "  signal receive peer=/usr/bin/canterbury,\n"
                                  "  signal receive peer=/usr/bin/ribchester,\n"
                                  "  signal receive peer=/usr/lib/ribchester/ribchester-core,\n"
                                  "}\n",
                                  profile_name);
  return _cby_apparmor_load_profile_from_string (profile_text, error);
}

G_STATIC_ASSERT (STDIN_FILENO == 0);
#define STDIN_FILENAME "/proc/self/fd/0"

/*
 * Replace the profile represented by @profile_text.
 */
gboolean
_cby_apparmor_load_profile_from_string (const gchar *profile_text,
                                        GError **error)
{
  g_autoptr (GSubprocess) child = NULL;

  g_return_val_if_fail (profile_text != NULL, FALSE);

  DEBUG ("Replacing AppArmor profile: %s", profile_text);

  child = g_subprocess_new (G_SUBPROCESS_FLAGS_STDIN_PIPE,
                            error,
                            APPARMOR_PARSER,
                            "--replace",
                            "--skip-cache",
                            "--skip-read-cache",
                            STDIN_FILENAME,
                            NULL);

  if (child == NULL)
    return FALSE;

  if (!g_subprocess_communicate_utf8 (child, profile_text, NULL, NULL, NULL,
                                      error))
    {
      g_prefix_error (error, "Unable to write AppArmor profile to pipe: ");
      return FALSE;
    }

  return g_subprocess_wait_check (child, NULL, error);
}

/*
 * Load the profile stored in text file @profile_basename into the kernel.
 *
 * If %CBY_APPARMOR_LOAD_FLAGS_READ_CACHE is in @flags, use a cached binary
 * representation of the profile (including all abstractions included
 * via `#include`) if it appears valid, rather than recompiling it from source.
 * The cached binary representation is stored in the `cache` directory,
 * and is also named @profile_basename.
 *
 * If %CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE is in @flags, try to update the
 * cached binary representation as a side-effect.
 */
static gboolean
_cby_apparmor_store_load_profile (CbyAppArmorStore *self,
                                  const gchar *profile_basename,
                                  CbyAppArmorLoadFlags flags,
                                  GError **error)
{
  g_autoptr (GSubprocessLauncher) launcher = NULL;
  g_autoptr (GSubprocess) child = NULL;
  g_autoptr (GPtrArray) argv = g_ptr_array_new_full (6, g_free);
  g_auto (CbyFileDescriptor) fd = -1;

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_NONE);

  g_ptr_array_add (argv, g_strdup (APPARMOR_PARSER));
  g_ptr_array_add (argv, g_strdup ("--replace"));

  if (!(flags & CBY_APPARMOR_LOAD_FLAGS_READ_CACHE))
    {
      g_ptr_array_add (argv, g_strdup ("--skip-read-cache"));

      if (!(flags & CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE))
        g_ptr_array_add (argv, g_strdup ("--skip-cache"));
    }

  if (flags & CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE)
    {
      g_ptr_array_add (argv, g_strdup ("--write-cache"));
    }

  /* We want the basename to be something we can use in the cache,
   * so we pass the directory (and not the file) as a fd, and work relative
   * to that directory fd, using the magic symlinks that the kernel provides
   * in /proc/self/fd. This gives us openat()-like functionality even for
   * command-line programs. */
  fd = _cby_file_descriptor_copy (self->apparmor_d, error);

  if (fd < 0)
    return FALSE;

  /* We arbitrarily choose fd 3, which is the first non-reserved fd, to be
   * apparmor.d in the subprocess. This is fd 3 in the subprocess's fd space,
   * which is independent of the parent's fd space, so we don't have to worry
   * about collisions. All fds that are not deliberately being inherited (as
   * these are) should be close-on-exec anyway, for better security and
   * robustness. */
  g_subprocess_launcher_take_fd (launcher, _cby_file_descriptor_steal (&fd), 3);
  g_ptr_array_add (argv,
                   g_strdup_printf ("/proc/self/fd/3/%s", profile_basename));

  /* apparmor_parser will write relative to here; we need to pass a directory.
   * Again, we cheat by using /proc/self/fd to get fd-relative I/O. */
  fd = _cby_file_descriptor_copy (self->cache, error);

  if (fd < 0)
    return FALSE;

  /* Similar to the use of fd 3 above, this is fd 4 in the subprocess's fd
   * space, not the parent's fd space, so we don't need to worry about
   * collisions. */
  g_subprocess_launcher_take_fd (launcher, _cby_file_descriptor_steal (&fd), 4);
  g_ptr_array_add (argv, g_strdup ("--cache-loc"));
  g_ptr_array_add (argv, g_strdup ("/proc/self/fd/4"));

  g_ptr_array_add (argv, NULL);
  child = g_subprocess_launcher_spawnv (launcher,
                                        (const gchar * const *) argv->pdata,
                                        error);

  if (child == NULL)
    return FALSE;

  return g_subprocess_wait_check (child, NULL, error);
}

/*
 * @profile_path: a directory whose contents mimic /etc/apparmor.d
 *
 * Open @profile_path as a #CbyAppArmorStore, taking an exclusive lock
 * that prevents other processes from doing the same until
 * this process calls _cby_apparmor_store_close().
 */
CbyAppArmorStore *
_cby_apparmor_store_open (const gchar *profile_path,
                          GError **error)
{
  g_autoptr (CbyAppArmorStore) self = NULL;

  g_return_val_if_fail (profile_path != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  DEBUG ("%s", profile_path);

  self = g_slice_new0 (CbyAppArmorStore);
  self->apparmor_d = -1;
  self->cache = -1;
  self->lock = -1;
  self->path = g_strdup (profile_path);

  self->apparmor_d = _cby_open_dir_at (NULL, AT_FDCWD, profile_path,
                                       CBY_PATH_FLAGS_NONE, error);

  if (self->apparmor_d < 0)
    return NULL;

  self->lock = openat (self->apparmor_d, ".lock",
                       O_WRONLY | O_CREAT | O_NOFOLLOW | O_CLOEXEC,
                       0600);

  if (self->lock < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to create lockfile \"%s/.lock\": %s",
                   profile_path, g_strerror (saved_errno));
      return NULL;
    }

  /* If another process is holding this lock, we'll block until it's
   * released. */
  while (flock (self->lock, LOCK_EX) < 0)
    {
      int saved_errno = errno;

      if (saved_errno == EINTR)
        continue;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to lock \"%s/.lock\": %s", profile_path,
                   g_strerror (saved_errno));
      return NULL;
    }

  if (mkdirat (self->apparmor_d, "cache", 0700) != 0 && errno != EEXIST)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to create \"%s/cache\": %s",
                   profile_path, g_strerror (saved_errno));
      return NULL;
    }

  self->cache = _cby_open_dir_at (profile_path, self->apparmor_d, "cache",
                                  CBY_PATH_FLAGS_NONE, error);

  if (self->cache < 0)
    return NULL;

  return g_steal_pointer (&self);
}

/*
 * Unlock and close @self.
 */
void
_cby_apparmor_store_close (CbyAppArmorStore *self)
{
  g_return_if_fail (self != NULL);

  DEBUG ("%s", self->path);

  if (self->lock >= 0)
    g_close (self->lock, NULL);

  if (self->cache >= 0)
    g_close (self->cache, NULL);

  if (self->apparmor_d >= 0)
    g_close (self->apparmor_d, NULL);

  g_free (self->path);
  g_slice_free (CbyAppArmorStore, self);
}

/*
 * Remove the AppArmor profile text file for @bundle_id, and its
 * cached binary representation, from @self.
 */
gboolean
_cby_apparmor_store_disable_profile (CbyAppArmorStore *self,
                                     const gchar *bundle_id,
                                     GError **error)
{
  g_autofree gchar *profile_basename = NULL;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (cby_is_bundle_id (bundle_id), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  profile_basename = g_strconcat ("Applications.", bundle_id, NULL);

  if (!_cby_apparmor_store_invalidate_ubercache (self, error))
    return FALSE;

  if (unlinkat (self->apparmor_d, profile_basename, 0) != 0 && errno != ENOENT)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to unlink \"%s/%s\": %s",
                   self->path, profile_basename, g_strerror (saved_errno));
      return FALSE;
    }

  if (unlinkat (self->cache, profile_basename, 0) != 0 && errno != ENOENT)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to unlink \"%s/cache/%s\": %s",
                   self->path, profile_basename, g_strerror (saved_errno));
      return FALSE;
    }

  if (fsync (self->apparmor_d) != 0 || fsync (self->cache) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to fsync \"%s\" or \"%s/cache\": %s",
                   self->path, self->path, g_strerror (saved_errno));
      return FALSE;
    }

  return TRUE;
}

/*
 * Raise an error if the textual profile source code in @profile_source
 * is syntactically invalid, or if it does not define the profile name
 * that we expect to see for @bundle_id.
 *
 * We have to forbid unexpected profile names, both to avoid collisions
 * and because if we loaded one into the kernel, we would be unable to
 * prevent the bundle's AppArmor profiles from interfering with an upgrade
 * or rollback. See
 * https://docs.apertis.org/latest/applications.html#apparmor-profiles
 *
 * This check is a developer convenience to avoid a privileged developer
 * being able to install an app-bundle with a profile name that will not
 * work; it is not a security mechanism. We make no attempt to verify that the
 * profile's contents are safe, and a profile that is in complain mode or
 * allows unconfined exec of a shell would be accepted here. The choice to
 * trust the bundle's content has already been made by the time we get here.
 */
static gboolean
_cby_apparmor_store_check_profile (CbyAppArmorStore *self,
                                   const gchar *profile_source,
                                   const gchar *bundle_id,
                                   GError **error)
{
  g_autoptr (GSubprocessLauncher) launcher = NULL;
  g_autoptr (GSubprocess) child = NULL;
  g_auto (CbyFileDescriptor) fd = -1;
  g_autofree gchar *expected_names = NULL;
  g_autofree gchar *names_buffer = NULL;

  fd = openat (AT_FDCWD, profile_source, O_RDONLY | O_CLOEXEC);

  if (fd < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open AppArmor profile text \"%s\": %s",
                   profile_source, g_strerror (saved_errno));
      return FALSE;
    }

  /* This is the expected output from listing the AppArmor profiles defined
   * in the input: a single profile name, followed by a single newline. */
  expected_names = g_strdup_printf ("/Applications/%s/**\n", bundle_id);

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE);
  g_subprocess_launcher_take_stdin_fd (launcher,
                                       _cby_file_descriptor_steal (&fd));
  child = g_subprocess_launcher_spawn (launcher, error,
                                       APPARMOR_PARSER, "--names", STDIN_FILENO,
                                       NULL);

  if (child == NULL ||
      !g_subprocess_communicate_utf8 (child, NULL, NULL, &names_buffer, NULL,
                                      error) ||
      !g_subprocess_wait_check (child, NULL, error))
    {
      g_prefix_error (error,
                      "Unable to enable AppArmor profiles from \"%s\": "
                      "failed to check using apparmor_parser: ",
                      profile_source);
      return FALSE;
    }

  if (strcmp (expected_names, names_buffer) != 0)
    {
      g_autofree gchar *escaped_expected = g_strescape (expected_names, NULL);
      g_autofree gchar *escaped_got = g_strescape (names_buffer, NULL);

      g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_DATA,
                   "Unable to enable AppArmor profiles from \"%s\": expected "
                   "AppArmor profiles list \"%s\", but got \"%s\"",
                   profile_source, escaped_expected, escaped_got);
      return FALSE;
    }

  return TRUE;
}

/*
 * @profile_source: the name of a file containing the text representation
 *  of an AppArmor profile
 * @bundle_id: the identifier of the application bundle
 *
 * Otherwise, create a symbolic link from @profile_source in @self using a
 * name derived from @bundle_id, then load the resulting profile into the
 * kernel.
 * As a side-effect, create a compiled binary representation of
 * @profile_source, including any abstractions included by `#include`,
 * in the cache directory.
 *
 * If @profile_source contains AppArmor profile names that do not match the
 * Apertis application bundle specification (in particular, if it contains
 * profile names that would interfere with upgrade or rollback),
 * do not load it and return an error.
 *
 * On failure, delete the symbolic link and its corresponding binary cache
 * file before returning error.
 */
gboolean
_cby_apparmor_store_enable_and_load_profile (CbyAppArmorStore *self,
                                             const gchar *profile_source,
                                             const gchar *bundle_id,
                                             GError **error)
{
  g_autofree gchar *profile_dest = NULL;
  g_autoptr (GSubprocess) child = NULL;
  g_autofree gchar *profile_basename = NULL;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (profile_source != NULL, FALSE);
  g_return_val_if_fail (cby_is_bundle_id (bundle_id), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!_cby_apparmor_store_invalidate_ubercache (self, error))
    return FALSE;

  if (!_cby_apparmor_store_check_profile (self, profile_source, bundle_id,
                                          error))
    return FALSE;

  profile_basename = g_strconcat ("Applications.", bundle_id, NULL);

  if (_cby_durable_symlink_at (profile_source,
                               self->path, self->apparmor_d,
                               profile_basename,
                               CBY_PATH_FLAGS_OVERWRITE,
                               error) &&
      _cby_apparmor_store_load_profile (self, profile_basename,
                                        CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE,
                                        error))
    {
      return TRUE;
    }
  else
    {
      g_autoptr (GError) unwind_error = NULL;

      /* make sure no detritus is left over - if it was, it might accidentally
       * end up in the ubercache */
      if (!_cby_apparmor_store_disable_profile (self, bundle_id, &unwind_error))
        {
          WARNING ("Unable to disable profile when recovering from failure "
                   "to enable: %s", unwind_error->message);
          g_clear_error (&unwind_error);
        }

      return FALSE;
    }
}

/*
 * Mark the ubercache (see #CbyAppArmorStore) as invalid.
 *
 * The representation we use for an invalid ubercache is an empty file.
 */
gboolean
_cby_apparmor_store_invalidate_ubercache (CbyAppArmorStore *self,
                                          GError **error)
{
  g_autofree gchar *ubercache = NULL;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  ubercache = g_strdup_printf ("/proc/self/fd/%d/.ubercache", self->cache);

  /* We overwrite the ubercache with an empty file instead of merely
   * deleting it, in an attempt to make the write durable. */
  return g_file_set_contents (ubercache, "", 0, error);
}

/*
 * Load all the AppArmor profiles from @self, using the "ubercache"
 * if possible, and perhaps building a new ubercache as a side-effect.
 *
 * If the ubercache is intact, we try to load it; if that succeeds,
 * we just return. This is the "happy path", and is hopefully what happens
 * when the canterbury-init executable is invoked during boot.
 *
 * If that fails, or if the ubercache was previously invalidated, load the
 * individual profiles, rebuilding the ubercache from them in the process.
 * Under normal circumstances, this should happen when the
 * canterbury-update-store-bundles executable is invoked by Ribchester
 * after one or more maintenance operations (install, upgrade etc.).
 * However, if the system loses power during or soon after maintenance
 * operations, the ubercache might still be marked invalid, or it might be
 * corrupt: to handle this case, canterbury-init must also be prepared to
 * do the same things as canterbury-update-store-bundles.
 *
 * If we get a write error while rebuilding the ubercache, we will still
 * try to load as many profiles as possible before we fail: this is
 * considered to be more important than building the ubercache, which
 * is merely an optimization. However, we will also report our failure
 * to the caller in this case, so that the slowdown caused by not
 * using the ubercache can be diagnosed.
 */
gboolean
_cby_apparmor_store_ensure_and_load_ubercache (CbyAppArmorStore *self,
                                               GError **error)
{
  g_autoptr (aa_kernel_interface) iface = NULL;
  g_autoptr (GDir) dir = NULL;
  g_autoptr (GOutputStream) writer = NULL;
  g_auto (CbyFileDescriptor) ubercache = -1;
  g_autofree gchar *path = NULL;
  const gchar *entry;
  struct stat stat_buf;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (aa_kernel_interface_new (&iface, NULL, NULL) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open AppArmor kernel interface: %s",
                   g_strerror (saved_errno));
      return FALSE;
    }

  /* First try to just load the ubercache, creating it if it doesn't exist.
   * If it is non-empty and can be loaded successfully, we're good. */
  ubercache = openat (self->cache, ".ubercache", O_RDWR | O_CLOEXEC | O_CREAT,
                      0600);

  if (ubercache < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open \"%s/cache/.ubercache\": %s",
                   self->path, g_strerror (saved_errno));
      return FALSE;
    }

  if (fstat (ubercache, &stat_buf) != 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to fstat \"%s/cache/.ubercache\": %s",
                   self->path, g_strerror (saved_errno));
      return FALSE;
    }

  if (stat_buf.st_size > 0)
    {
      int saved_errno;

      /* It's non-empty; try to load it into the kernel. The ubercache is in
       * binary format, so we can do this without apparmor_parser's help.
       * If we succeed, there's no more to be done. */
      if (aa_kernel_interface_replace_policy_from_fd (iface, ubercache) == 0)
        return TRUE;

      saved_errno = errno;

      WARNING ("Unable to load \"%s/cache/.ubercache\" into kernel: %s",
               self->path, g_strerror (saved_errno));
    }

  /* We failed to load the ubercache, or it is empty (perhaps because we
   * invalidated it). Fall back to loading the profiles individually;
   * we are willing to use their individual cache files, so this hopefully
   * won't take too long. */
  path = g_strdup_printf ("/proc/self/fd/%d/", self->apparmor_d);
  dir = g_dir_open (path, 0, error);

  if (dir == NULL)
    return FALSE;

  /* We're under a lock, so it should be fine to delete this. */
  if (unlinkat (self->cache, ".ubercache.new~", 0) != 0 && errno != ENOENT)
    {
      int saved_errno = errno;

      WARNING ("Unable to unlink temporary ubercache "
               "\"%s/cache/.ubercache.new~\": %s",
               self->path, g_strerror (saved_errno));
    }

  _cby_file_descriptor_clear (&ubercache);
  ubercache = openat (self->cache, ".ubercache.new~",
                      O_RDWR | O_CLOEXEC | O_CREAT | O_EXCL,
                      0600);

  if (ubercache < 0)
    {
      int saved_errno = errno;

      g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                   "Unable to open \"%s/cache/.ubercache.new~\": %s",
                   self->path, g_strerror (saved_errno));
      return FALSE;
    }

  /* take over responsibility for closing it */
  writer = g_unix_output_stream_new (_cby_file_descriptor_steal (&ubercache),
                                     TRUE);

  for (entry = g_dir_read_name (dir);
       entry != NULL;
       entry = g_dir_read_name (dir))
    {
      const gchar *bundle_id;
      g_autoptr (GError) load_error = NULL;

      /* ignore hidden files */
      if (entry[0] == '.')
        continue;

      /* ignore files that don't look like the profile of an application */
      if (!g_str_has_prefix (entry, "Applications."))
        continue;

      /* ignore temporary files */
      if (strchr (entry, '~') != NULL)
        continue;

      bundle_id = entry + strlen ("Applications.");

      if (!cby_is_bundle_id (bundle_id))
        {
          WARNING ("Not a bundle ID, ignoring: %s", bundle_id);
          continue;
        }

      /* We don't need to check the profile's contents with
       * _cby_apparmor_store_check_profile, because we already
       * did that before we symlinked it into the apparmor.d directory.
       * We assume it didn't subsequently change, because we are guarding
       * against mistakes and not malice here. */

      if (_cby_apparmor_store_load_profile (self, entry,
                                            (CBY_APPARMOR_LOAD_FLAGS_READ_CACHE |
                                             CBY_APPARMOR_LOAD_FLAGS_WRITE_CACHE),
                                            &load_error))
        {
          g_autoptr (GInputStream) reader = NULL;
          g_auto (CbyFileDescriptor) fd = -1;
          gssize spliced;

          fd = openat (self->cache, entry, O_RDONLY | O_CLOEXEC);

          if (fd < 0)
            {
              int saved_errno = errno;

              /* This particular profile has a problem, so we'll just leave
               * it out of the ubercache */
              WARNING ("Profile \"%s/%s\" was loaded successfully, but "
                       "could not open its cache file \"%s/cache/%s\": %s",
                       self->path, entry, self->path, entry,
                       g_strerror (saved_errno));
              continue;
            }

          /* If we already gave up on populating the ubercache, there's
           * nothing more to do with this particular profile. However,
           * we still continue to load the other profiles: it is functionally
           * necessary to load as many of them as we can (otherwise there
           * will be some app-bundles that don't work), and the ubercache
           * is just an optimization anyway. */
          if (writer == NULL)
            continue;

          reader = g_unix_input_stream_new (_cby_file_descriptor_steal (&fd),
                                            TRUE);

          spliced = g_output_stream_splice (writer, reader,
                                            G_OUTPUT_STREAM_SPLICE_CLOSE_SOURCE,
                                            NULL, &load_error);

          if (spliced < 0)
            {
              /* If we copied part of the file before failing, then the
               * ubercache will be corrupted. Leave it invalidated so that
               * we'll try again next boot.
               *
               * Even though we can't write the ubercache, we still want to
               * load as many profiles as we can, so keep going. */
              DEBUG ("Write error while copying \"%s/cache/%s\" into "
                     "ubercache: %s",
                     self->path, entry, load_error->message);
              DEBUG ("Continuing to load other profiles for now, but we will "
                     "return failure later.");
              g_clear_object (&writer);
              g_propagate_prefixed_error (error, g_steal_pointer (&load_error),
                                          "Unable to copy profile "
                                          "\"%s/cache/%s\" into ubercache: ",
                                          self->path, entry);
            }
        }
      else
        {
          /* This particular profile is bad, so we'll just leave it out of
           * the ubercache */
          WARNING ("Unable to load profile \"%s/%s\": %s",
                   self->path, entry, load_error->message);
          g_clear_error (&load_error);
        }
    }

  if (writer == NULL)
    {
      DEBUG ("Writing profiles to .ubercache.new~ failed: not renaming to "
             ".ubercache");
      g_assert (error == NULL || *error != NULL);
    }
  else if (g_output_stream_close (writer, NULL, error))
    {
      DEBUG ("Renaming \"%s/cache/.ubercache.new~\" to "
             "\"cache/.ubercache\"...",
             self->path);
      if (renameat (self->cache, ".ubercache.new~", self->cache,
                    ".ubercache") == 0)
        {
          DEBUG ("Rename successful");
          /* skip cleanup of .ubercache.new~ */
          return TRUE;
        }
      else
        {
          int saved_errno = errno;

          DEBUG ("Rename unsuccessful");
          g_set_error (error, G_IO_ERROR, g_io_error_from_errno (saved_errno),
                       "Unable to rename \"%s/cache/.ubercache.new~\" to "
                       "\"%s/cache/.ubercache\": %s",
                       self->path, self->path, g_strerror (saved_errno));
          /* fall through to cleanup */
        }
    }
  else
    {
      DEBUG ("Closing .ubercache.new~ failed: not renaming to "
             ".ubercache");
      g_assert (error == NULL || *error != NULL);
    }

  if (unlinkat (self->cache, ".ubercache.new~", 0) != 0)
    {
      int saved_errno = errno;

      WARNING ("Unable to unlink temporary ubercache "
               "\"%s/cache/.ubercache.new~\": %s",
               self->path, g_strerror (saved_errno));
    }

  return FALSE;
}
