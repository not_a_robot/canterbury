/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "launch-mgr"
#include "launch_mgr_if.h"
#include "launch_mgr.h"

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <glib/gstdio.h>
#include <systemd/sd-daemon.h>
#include <systemd/sd-journal.h>

#include "app_launch_mgr_if.h"
#include "app_mgr_if.h"
#include "canterbury/canterbury.h"
#include "canterbury/gdbus/enumtypes.h"
#include "canterbury/messages-internal.h"
#include "canterbury/platform/entry-point-internal.h"
#include "launcher_mgr_if.h"
#include "service-manager.h"
#include "system_res_mgr_if.h"
#include "util.h"

typedef struct _AsyncLaunchContext AsyncLaunchContext;

static void async_launch_context_launch_daemon_later (AsyncLaunchContext *);
static AsyncLaunchContext *async_launch_context_new (CbyEntryPoint *entry_point,
                                                     const gchar *launched_from,
                                                     guint special_params,
                                                     const gchar * const *uris,
                                                     GVariant *platform_data,
                                                     const gchar * const *arguments,
                                                     LaunchFlags flags);
static void async_launch_context_start_task (GTask *task);

static gboolean launch_daemon_async (AsyncLaunchContext *launch_context);
static void service_stopped_cb (CbyServiceManager *service_manager,
                                CbyEntryPoint *entry_point,
                                gpointer user_data);

typedef struct
{
  CbyEntryPointIndex *entry_point_index;
  CbyServiceManager *service_manager;

  /* (element-type CbyEntryPoint) (ownership full)
   * List of daemons that have been started with v_launch_agent_or_service()
   * or v_launch_audio_daemon(), used to avoid starting one twice.
   *
   * Every element has type CANTERBURY_EXECUTABLE_TYPE_SERVICE or
   * CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE. */
  GList*        pDaemonsList;

  /* (element-type LaunchNewAppInfo) (ownership full)
   * List of running daemons: processes or systemd services for which
   * launch_daemon_async() succeeded.
   * Every element has type CANTERBURY_EXECUTABLE_TYPE_SERVICE or
   * CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE. */
  GSList*       pRunningDaemonsList;

  /* (element-type LaunchNewAppInfo) (ownership full)
   * List of running applications: processes or systemd services for which
   * launch_mgr_launch_app_async() succeeded. */
  GSList*       pRunningAppsList;

  gboolean      bLaunchInProgress;
  GQueue        queued_launches;
  guint         uinCurrentLauncherView;
  guint         uinLaunchInProgressTimer;
  guint         uinExtWinIdCaptureAttempt;
  /* Time in seconds before starting the next daemon */
  guint daemon_delay;
} LaunchMgr;

static LaunchMgr LaunchMgrObj;

void
v_launch_mgr_get_appliation_mimes(GList** pAppName ,
                                  GList** pAppMimeType)
{
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (GPtrArray) entry_points = NULL;
  guint i;
  CbyEntryPoint *entry_point = NULL;

  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_points = cby_entry_point_index_get_entry_points (entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      const gchar *id;
      entry_point = g_ptr_array_index (entry_points, i);
      id = cby_entry_point_get_id (entry_point);
      if (cby_entry_point_get_supported_types (entry_point) != NULL )
        {
           const gchar *const *ptr = cby_entry_point_get_supported_types (entry_point);
           for (; *ptr; ptr++)
             {
               *pAppMimeType = g_list_append (*pAppMimeType , g_strdup (*ptr));
               launch_mgr_debug ("\n pAppName %s pAppMimeType %s\n", id, *ptr);
               *pAppName = g_list_append (*pAppName, g_strdup (id));
             }
        }
    }
}

void
display_launcher_app(const gchar *pCurrentAppNnTop,
    const gchar *pLaunchedClient)
{
  clear_back_stack();
  app_mgr_debug("current app on top %s  \n ", pCurrentAppNnTop );
    // ask the window manager to minimize the current app on top and swipe in Launcher
  if( g_strcmp0( pCurrentAppNnTop , LAUNCHER_APP_NAME ) != 0 )
  {
     app_mgr_debug("*** send minimize *** \n " );
     win_mgr_minimize_current_application_window( pCurrentAppNnTop , pLaunchedClient);
  }

  launch_mgr_activate_launcher (CANTERBURY_HOME_KEY);
  hard_keys_emit_home_press_response (TRUE);
}

static void
activate_launcher_cb (GObject *source_object,
                      GAsyncResult *result,
                      gpointer user_data)
{
  g_autoptr (GError) error = NULL;

  if (launch_mgr_launch_app_finish (result, &error))
    DEBUG ("Activated %s", LAUNCHER_APP_NAME);
  else
    WARNING ("Unable to activate %s: %s", LAUNCHER_APP_NAME, error->message);
}

void
launch_mgr_activate_launcher (const gchar *launched_by)
{
  LaunchNewAppInfo *appInfo = launch_mgr_find_running_app (LAUNCHER_APP_NAME);

  if (appInfo != NULL)
    {
      CbyEntryPoint *entry_point = appInfo->entry_point;

      launch_mgr_launch_app_async (entry_point, launched_by, 0xff, NULL,
                                   NULL, NULL,
                                   (LAUNCH_FLAGS_CHECK_EXISTING_GUI |
                                    LAUNCH_FLAGS_ONLY_EXISTING |
                                    LAUNCH_FLAGS_SET_ACTIVE_TITLE |
                                    LAUNCH_FLAGS_START_TIMER),
                                   NULL, activate_launcher_cb,
                                   g_object_ref (entry_point));
    }
}

static gboolean app_launch_animation_completed (gpointer data);

static void
start_launch_progress_timer (void)
{
  if(LaunchMgrObj.uinLaunchInProgressTimer>0)
  {
    g_source_remove ( LaunchMgrObj.uinLaunchInProgressTimer);
    LaunchMgrObj.uinLaunchInProgressTimer = 0;
  }
  LaunchMgrObj.uinLaunchInProgressTimer = g_timeout_add_seconds(3.5, app_launch_animation_completed , NULL);
  LaunchMgrObj.bLaunchInProgress = TRUE;
}

void
reset_launch_progress_timer (void)
{
  g_autoptr (GTask) task = NULL;

  if(LaunchMgrObj.uinLaunchInProgressTimer>0)
  {
    g_source_remove ( LaunchMgrObj.uinLaunchInProgressTimer);
    LaunchMgrObj.uinLaunchInProgressTimer = 0;
  }
  LaunchMgrObj.bLaunchInProgress = FALSE;

  task = g_queue_pop_head (&LaunchMgrObj.queued_launches);

  if (task != NULL)
    async_launch_context_start_task (task);
}

static gboolean
app_launch_animation_completed( gpointer data )
{
   LaunchMgrObj.uinLaunchInProgressTimer = 0;
   reset_launch_progress_timer ();
   return G_SOURCE_REMOVE;
}

/* FIXME: All callers of this function should probably be changed to
 * queue up their actions for after the app launch has finished, rather
 * than silently cancelling them.
 * https://phabricator.apertis.org/T2400 */
gboolean
is_app_launch_in_progress (void)
{
   if(LaunchMgrObj.bLaunchInProgress)
    return TRUE;
    else
    return FALSE;
}

LaunchNewAppInfo*
get_running_app_info_at_position(guint uinPosition)
{
  if(get_running_app_info_stack_length() > uinPosition)
    return g_slist_nth_data ( LaunchMgrObj.pRunningAppsList , uinPosition );
  else
    return NULL;
}

LaunchNewAppInfo *
launch_mgr_find_running_app (const gchar *entry_point_id)
{
  GSList *l;

  for (l = LaunchMgrObj.pRunningAppsList; l != NULL; l = g_slist_next (l))
    {
      LaunchNewAppInfo *info = l->data;

      if (g_strcmp0 (cby_entry_point_get_id (info->entry_point), entry_point_id) == 0)
        return info;
    }

  return NULL;
}

static LaunchNewAppInfo *
launch_mgr_find_running_daemon (CbyEntryPoint *entry_point)
{
  GSList *l;

  for (l = LaunchMgrObj.pRunningDaemonsList; l != NULL; l = g_slist_next (l))
    {
      LaunchNewAppInfo *info = l->data;

      if (info->entry_point == entry_point)
        return info;
    }

  return NULL;
}

guint
get_running_app_info_stack_length (void)
{
  return g_slist_length( LaunchMgrObj.pRunningAppsList );
}

LaunchNewAppInfo*
get_running_daemon_info_at_position(guint uinPosition)
{
  if(get_running_daemon_info_stack_length() > uinPosition)
    return g_slist_nth_data ( LaunchMgrObj.pRunningDaemonsList , uinPosition );
  else
    return NULL;
}


guint
get_running_daemon_info_stack_length (void)
{
  return g_slist_length( LaunchMgrObj.pRunningDaemonsList );
}

CbyEntryPointIndex *
launch_mgr_get_entry_point_index (void)
{
  return g_object_ref (LaunchMgrObj.entry_point_index);
}

void
v_launch_audio_daemon (CbyEntryPoint *entry_point)
{
  AsyncLaunchContext *context;
  const gchar *id = cby_entry_point_get_id (entry_point);

  LaunchMgrObj.pDaemonsList = g_list_append (LaunchMgrObj.pDaemonsList, g_object_ref (entry_point));
  launch_mgr_debug ("daemon name %s\n", id);

  context = async_launch_context_new (entry_point, NULL, 0, NULL, NULL, NULL,
                                      (LAUNCH_FLAGS_DAEMON |
                                       LAUNCH_FLAGS_ARGV_REPLACE_PLAY_MODE));
  async_launch_context_launch_daemon_later (g_steal_pointer (&context));
  launch_mgr_debug("\n AUDIO RESOURCE IS PUSHED INTO QUE \n");
}

/**
 * initialize_launch_mgr:
 *
 * initialize_launch_mgr initializes the launch manager service of the application manager
 * It reads the configuration of all the applications from the db and builds the database required for the launcher
**/
void
launch_mgr_init (CbyServiceManager *service_manager,
                 CbyEntryPointIndex *entry_point_index)
{
  /* Arbitrary delay in seconds before starting first daemon. Subsequent
   * daemons are started after 4, 5, ... seconds. */
  LaunchMgrObj.daemon_delay = 3;

  LaunchMgrObj.service_manager = g_object_ref (service_manager);

  g_signal_connect (LaunchMgrObj.service_manager, "service-stopped",
                    G_CALLBACK (service_stopped_cb), NULL);

  LaunchMgrObj.entry_point_index = g_object_ref (entry_point_index);
  LaunchMgrObj.pDaemonsList = NULL;
  LaunchMgrObj.pRunningAppsList = NULL;
  LaunchMgrObj.pRunningDaemonsList = NULL;
  LaunchMgrObj.bLaunchInProgress = FALSE;
  LaunchMgrObj.uinLaunchInProgressTimer = 0;
  LaunchMgrObj.uinCurrentLauncherView = 0;
}

static void
async_launch_context_launch_daemon_later (AsyncLaunchContext *daemon)
{
  /* launch_daemon_async takes ownership of the context */
  g_timeout_add_seconds_full (G_PRIORITY_DEFAULT, LaunchMgrObj.daemon_delay,
                              (GSourceFunc) launch_daemon_async,
                              g_steal_pointer (&daemon), NULL);
  /* Wait 1 more second before starting the next daemon.
   * FIXME: this has not necessarily been well thought out. In practice
   * we queue up most (all?) of the daemons in one batch, so with
   * LaunchMgrObj.daemon_delay initially set to 3, if we start at 12:00:00
   * we will start daemons at 12:00:03, 12:00:04, 12:00:05 and so on.
   * However, if we queue up another daemon for starting later, it will
   * be delayed by a few seconds for no particularly good reason. */
  LaunchMgrObj.daemon_delay += 1;
}

static void
log_exit_status (const gchar *pAppName,
                 gint status)
{
  if(  WIFEXITED(status)  )
  {
      launch_mgr_debug(" Child exited normally with exit %d \n" , WEXITSTATUS(status) );
      return;
  }
  else
  {
      if( WIFSIGNALED( status ) )
      {
          launch_mgr_debug(" Child %s exited abnormally due to receipt of a signal %d that was not caught \n" , pAppName , WTERMSIG(status) );
          /* handler the abnormal termination of a daemon, try starting it again */
      }

      if( WIFSTOPPED( status ) )
      {
          launch_mgr_debug(" Child process is stopped abnormally number of the signal that caused the child process to stop  %d \n" , WSTOPSIG(status) );
          return;
      }
  }
}

static void handle_app_exit (LaunchNewAppInfo *appInfo);

static void
v_launch_app_exit_cb (GPid pid,
    gint status,
    gpointer pUserData)
{
  guint cnt;
  guint running_apps_list_len;

  if(v_get_system_shutdown_signal())
   return;

    running_apps_list_len = g_slist_length (LaunchMgrObj.pRunningAppsList);
    for( cnt = 0 ; cnt< running_apps_list_len ; cnt++)
    {
        LaunchNewAppInfo *appInfo = g_slist_nth_data ( LaunchMgrObj.pRunningAppsList , cnt );
        const gchar *id = cby_entry_point_get_id (appInfo->entry_point);

        if( appInfo->pid != pid )
            continue;
        log_exit_status (id, status);
        handle_app_exit (appInfo);
        break;
    }
}

static void
handle_app_exit (LaunchNewAppInfo *appInfo)
{
        const gchar *id = cby_entry_point_get_id (appInfo->entry_point);

        if (v_get_system_shutdown_signal ())
          return;

        activity_mgr_remove_entry_point (appInfo->entry_point);

        LaunchMgrObj.pRunningAppsList = g_slist_remove (  LaunchMgrObj.pRunningAppsList , appInfo );

        back_stack_remove_registration (appInfo->entry_point);

        if (_cby_is_target ())
        	check_if_application_killed_from_OOM (id);


        g_clear_object (&appInfo->entry_point);
        g_strfreev (appInfo->legacy_arguments);
        g_free(appInfo);
}

static void handle_daemon_exit (LaunchNewAppInfo *appInfo,
                                gboolean service_managed);

static void
v_launch_daemon_exit_cb (GPid     pid,
                         gint     status,
                         gpointer pUserData)
{
  guint cnt;
  LaunchNewAppInfo *appInfo = NULL;
  guint running_daemons_list_len;

  if(v_get_system_shutdown_signal())
   return;

  running_daemons_list_len = g_slist_length (LaunchMgrObj.pRunningDaemonsList);

  for( cnt = 0 ; cnt< running_daemons_list_len ; cnt++)
  {
    const gchar *id;

    appInfo = g_slist_nth_data ( LaunchMgrObj.pRunningDaemonsList , cnt );
    if( appInfo->pid != pid )
        continue;

    id = cby_entry_point_get_id (appInfo->entry_point);

    log_exit_status (id, status);
    handle_daemon_exit (appInfo, FALSE);
    break;
  }
}

static void
handle_daemon_exit (LaunchNewAppInfo *appInfo,
                    gboolean service_managed)
{
    AsyncLaunchContext *context;
    const gchar *id;

    if (v_get_system_shutdown_signal ())
      return;

    LaunchMgrObj.pRunningDaemonsList = g_slist_remove (  LaunchMgrObj.pRunningDaemonsList , appInfo );

    id = cby_entry_point_get_id (appInfo->entry_point);

    if (cby_entry_point_index_get_by_id (LaunchMgrObj.entry_point_index,
                                         id) == NULL)
      {
        DEBUG ("daemon \"%s\" no longer available, not restarting", id);
        return;
      }

    DEBUG ("daemon name \"%s\" (%smanaged by systemd)",
           id, service_managed ? "" : "not ");

    /* We are not responsible for restarting systemd-managed services:
     * systemd will do that itself, and unlike canterbury, it has proper
     * rate-limiting. We only do the rest of this function for platform
     * service entry points (not in an app-bundle). */
    if (service_managed)
      return;

    context = async_launch_context_new (appInfo->entry_point, NULL, 0, NULL,
                                        NULL, NULL, LAUNCH_FLAGS_DAEMON);

    /* launch_daemon_async takes ownership of the context */
    g_timeout_add_full (G_PRIORITY_DEFAULT, 0,
                        (GSourceFunc) launch_daemon_async,
                        g_steal_pointer (&context), NULL);

    g_clear_object (&appInfo->entry_point);
    g_strfreev (appInfo->legacy_arguments);
    g_free (appInfo);
}

static void
service_stopped_cb (CbyServiceManager *service_manager,
                    CbyEntryPoint *entry_point,
                    gpointer user_data)
{
  GSList *l;

  if (v_get_system_shutdown_signal ())
    return;

  for (l = LaunchMgrObj.pRunningAppsList; l != NULL; l = l->next)
    {
      LaunchNewAppInfo *app_info = l->data;

      if (app_info->entry_point == entry_point)
        {
          handle_app_exit (app_info);
          break;
        }
    }

  for (l = LaunchMgrObj.pRunningDaemonsList; l != NULL; l = l->next)
    {
      LaunchNewAppInfo *app_info = l->data;

      if (app_info->entry_point == entry_point)
        {
          handle_daemon_exit (app_info, TRUE);
          break;
        }
    }
}

static void
stop_service_cb (GObject *source_object,
                 GAsyncResult *result,
                 gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source_object);
  gboolean success;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyEntryPoint) entry_point = user_data;

  success = _cby_service_manager_stop_service_finish (service_manager,
                                                      result,
                                                      &error);
  if (success)
    DEBUG ("Service \"%s\" for entry point \"%s\" stopped",
           _cby_service_manager_get_service_name (service_manager, entry_point),
           cby_entry_point_get_id (entry_point));
  else
    WARNING ("Unable to stop service \"%s\" for entry point \"%s\": %s",
             _cby_service_manager_get_service_name (service_manager, entry_point),
             cby_entry_point_get_id (entry_point),
             error->message);
}

gboolean
launch_mgr_stop_running_app (CbyEntryPoint *entry_point,
                             GPid pid)
{
  gboolean success;

  if (_cby_service_manager_has_service (LaunchMgrObj.service_manager,
                                        entry_point))
    {
      _cby_service_manager_stop_service_async (LaunchMgrObj.service_manager,
                                               entry_point,
                                               NULL,
                                               stop_service_cb,
                                               g_object_ref (entry_point));
      /* Lets just assume success. systemd should make sure the service is
       * stopped unless we can't contact it.
       */
      success = TRUE;
    }
  else
    {
      success = (kill (pid, SIGTERM) == 0);
    }
  return success;
}

gboolean
launch_mgr_kill_running_app (CbyEntryPoint *entry_point,
                             GPid pid)
{
  gboolean success;

  if (_cby_service_manager_has_service (LaunchMgrObj.service_manager,
                                        entry_point))
    {
      _cby_service_manager_stop_service_async (LaunchMgrObj.service_manager,
                                               entry_point,
                                               NULL,
                                               stop_service_cb,
                                               g_object_ref (entry_point));
      /* Lets just assume success. systemd should make sure the service is
       * stopped unless we can't contact it.
       */
      success = TRUE;
    }
  else
    {
      success = (kill (pid, SIGKILL) == 0);
    }
  return success;
}

static void
signal_service_cb (GObject *source_object,
                   GAsyncResult *result,
                   gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source_object);
  gboolean success;
  g_autoptr (GError) error = NULL;
  g_autoptr (CbyEntryPoint) entry_point = user_data;

  success = _cby_service_manager_signal_service_finish (service_manager,
                                                        result,
                                                        &error);
  if (success)
    DEBUG ("Service \"%s\" for entry point \"%s\" signalled",
           _cby_service_manager_get_service_name (service_manager, entry_point),
           cby_entry_point_get_id (entry_point));
  else
    WARNING ("Unable to signal service \"%s\" for entry point \"%s\": %s",
             _cby_service_manager_get_service_name (service_manager, entry_point),
             cby_entry_point_get_id (entry_point),
             error->message);
}

gboolean
launch_mgr_signal_running_app (CbyEntryPoint *entry_point,
                               GPid pid,
                               gint signum)
{
  gboolean success;

  if (_cby_service_manager_has_service (LaunchMgrObj.service_manager,
                                        entry_point))
    {
      _cby_service_manager_signal_service_async (LaunchMgrObj.service_manager,
                                                 entry_point,
                                                 signum,
                                                 NULL,
                                                 signal_service_cb,
                                                 g_object_ref (entry_point));
      /* Lets just assume success. systemd should make sure the service is
       * stopped unless we can't contact it.
       */
      success = TRUE;
    }
  else
    {
      success = (kill (pid, signum) == 0);
    }
  return success;
}

void
v_kill_running_agent( const gchar* pAppName )
{
  LaunchNewAppInfo *appInfo = NULL;
  guint running_daemons_list_len =  g_slist_length( LaunchMgrObj.pRunningDaemonsList );
  guint cnt;
  for( cnt = 0 ; cnt< running_daemons_list_len ; cnt++)
  {
    const gchar *id;

    appInfo = g_slist_nth_data ( LaunchMgrObj.pRunningDaemonsList , cnt );
    id = cby_entry_point_get_id (appInfo->entry_point);

    if (g_strcmp0 (id, pAppName) == 0)
    {
      if (!launch_mgr_kill_running_app (appInfo->entry_point, appInfo->pid))
        WARNING ("The entry point <%s> couldnt be killed", id);
      break;
    }
  }
}

void
v_launch_agent_or_service (CbyEntryPoint *entry_point)
{
  guint inAppCount = 0;
  gboolean bAppFound = FALSE;
  const gchar *id = cby_entry_point_get_id (entry_point);

  while(inAppCount < g_list_length(LaunchMgrObj.pDaemonsList))
  {
     CbyEntryPoint *daemon_entry_point;

     daemon_entry_point = g_list_nth_data (LaunchMgrObj.pDaemonsList, inAppCount);
     if (!g_strcmp0 (id, cby_entry_point_get_id (daemon_entry_point)))
     {
       bAppFound=TRUE;
       break;
     }
     inAppCount++;
  }

  if(FALSE == bAppFound)
  {
    AsyncLaunchContext *context;

    LaunchMgrObj.pDaemonsList = g_list_append (LaunchMgrObj.pDaemonsList, g_object_ref (entry_point));
    launch_mgr_debug(" daemon  name %s \n", id);

    context = async_launch_context_new (entry_point, NULL, 0, NULL, NULL, NULL,
                                        LAUNCH_FLAGS_DAEMON);
    async_launch_context_launch_daemon_later (g_steal_pointer (&context));
  }
}

static const gchar *
executable_type_to_string (CanterburyExecutableType t)
{
  GEnumClass *cls;
  GEnumValue *value;
  const gchar *ret;

  cls = g_type_class_ref (CANTERBURY_TYPE_EXECUTABLE_TYPE);
  value = g_enum_get_value (cls, t);

  if (value == NULL)
    ret = "(unknown)";
  else
    ret = value->value_nick;

  g_type_class_unref (cls);
  return ret;
}

static void
launch_mgr_start_during_startup_cb (GObject *source_object,
                                    GAsyncResult *result,
                                    gpointer user_data)
{
  g_autoptr (CbyEntryPoint) entry_point = user_data;
  g_autoptr (GError) error = NULL;

  if (launch_mgr_launch_app_finish (result, &error))
    DEBUG ("Entry point \"%s\" started successfully",
           cby_entry_point_get_id (entry_point));
  else
    WARNING ("Unable to start entry point \"%s\": %s",
             cby_entry_point_get_id (entry_point), error->message);
}

/*
 * Launch startup GUI applications.
 *
 * Also launch some legacy entry points:
 * - agents that are not managed by the CbyServiceManager (meaning they are
 *   in the platform layer, not in an app-bundle)
 * - "services", but only in the SDK image
 */
void
launch_mgr_launch_startup_applications (void)
{
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (GPtrArray) entry_points = NULL;
  guint i;
  CbyEntryPoint *entry_point = NULL;

  entry_point_index = launch_mgr_get_entry_point_index ();
  entry_points = cby_entry_point_index_get_entry_points (entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      CanterburyExecutableType executable_type;
      const gchar* id;
      entry_point = g_ptr_array_index (entry_points, i);
      id = cby_entry_point_get_id (entry_point);
      executable_type = _cby_entry_point_get_executable_type (entry_point);

      /* On target and development images, `systemd --user` will
       * launch service processes like Barkway for us, but in the "simulator"
       * environment on SDK images, we aren't running the necessary
       * systemd target for that to happen. Compensate by starting those
       * services ourselves.
       *
       * FIXME: we should make the simulator more closely resemble
       * production images. https://phabricator.apertis.org/T575 */
      if (!_cby_is_target ())
        {
          if (executable_type == CANTERBURY_EXECUTABLE_TYPE_SERVICE)
            {
              DEBUG ("Simulator environment detected, starting "
                     "service %s", id);
              v_launch_agent_or_service (entry_point);
              continue;
            }
        }

      if (executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE )
        {
          if (_cby_service_manager_has_service (LaunchMgrObj.service_manager,
                                                entry_point))
            {
              DEBUG ("Not starting agent service %s: the service manager is "
                     "responsible for it", id);
            }
          else
            {
              DEBUG ("Starting agent service %s", id);
              v_launch_agent_or_service (entry_point);
            }
        }
      else if (executable_type == CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION )
      {
        DEBUG ("Starting startup application %s", id);

        launch_mgr_launch_app_async (entry_point, CANTERBURY_SELF_CLIENT, 0xff,
                                     NULL, NULL, NULL, LAUNCH_FLAGS_NONE, NULL,
                                     launch_mgr_start_during_startup_cb,
                                     g_object_ref (entry_point));
      }
      else
      {
          DEBUG ("Not starting entry point of type %u (%s): %s",
              executable_type,
              executable_type_to_string (executable_type),
              id);
      }
    }

  window_mgr_set_active (LAUNCHER_APP_NAME, LAUNCHER_APP_NAME);
}

void
v_set_current_launcher_view_to_category (void)
{
  LaunchMgrObj.uinCurrentLauncherView = TRUE;
}

void
v_reset_current_launcher_view (void)
{
  LaunchMgrObj.uinCurrentLauncherView = FALSE;
}

gboolean
b_is_current_view_launcher (void)
{
  return LaunchMgrObj.uinCurrentLauncherView;
}

struct _AsyncLaunchContext
{
  CbyEntryPoint *entry_point;
  gchar **uris;
  GVariant *platform_data;
  gchar **legacy_arguments;
  gchar **argv;
  gchar *launched_from;
  guint special_params;
  LaunchFlags flags;
};

static AsyncLaunchContext *
async_launch_context_new (CbyEntryPoint *entry_point,
                          const gchar *launched_from,
                          guint special_params,
                          const gchar * const *uris,
                          GVariant *platform_data,
                          const gchar * const *arguments,
                          LaunchFlags flags)
{
  AsyncLaunchContext *self;

  self = g_slice_new0 (AsyncLaunchContext);
  self->entry_point = g_object_ref (entry_point);
  self->launched_from = g_strdup (launched_from);
  self->special_params = special_params;
  self->flags = flags;
  self->uris = g_strdupv ((gchar **) uris);
  self->legacy_arguments = g_strdupv ((gchar **) arguments);

  if (platform_data != NULL)
    self->platform_data = g_variant_ref_sink (platform_data);

  return self;
}

static void
async_launch_context_fill_argv (AsyncLaunchContext *self)
{
  g_autoptr (GPtrArray) argv = NULL;
  const gchar * const *iter;

  if (self->argv != NULL)
    return;

  argv = g_ptr_array_new_with_free_func (g_free);

  g_ptr_array_add (argv,
                   g_strdup (cby_entry_point_get_executable (self->entry_point)));

  for (iter = (const gchar * const *) self->legacy_arguments;
       iter != NULL && *iter != NULL;
       iter++)
    g_ptr_array_add (argv, g_strdup (*iter));

  if (!(self->flags & LAUNCH_FLAGS_ARGV_REPLACE))
    {
      for (iter = _cby_entry_point_get_arguments (self->entry_point); *iter; iter++)
        {
          g_ptr_array_add (argv, g_strdup (*iter));

          if (g_strcmp0 (*iter, "play-mode") == 0 &&
              (self->flags & LAUNCH_FLAGS_ARGV_REPLACE_PLAY_MODE))
            {
              /* Add a second argument, "play", replacing the next argument
               * if there is one. For example we might treat
               * Exec=audio-app play-mode stop url about:blank
               * as if it had been
               * Exec=audio-app play-mode play url about:blank
               * TODO: rethink this. https://phabricator.apertis.org/T992 */
              g_ptr_array_add (argv, g_strdup ("play"));

                  /* Skip the argument we replace, if any */
              if (iter[1] != NULL)
                iter++;
            }
        }
    }

  g_ptr_array_add (argv, NULL);

  self->argv = (gchar **) g_ptr_array_free (g_steal_pointer (&argv), FALSE);
}

static void async_launch_context_activate (AsyncLaunchContext *self,
                                           GPid pid);

static void
async_launch_context_launched (AsyncLaunchContext *self,
                               GPid pid,
                               gboolean service_managed)
{
  CbyEntryPoint *entry_point = self->entry_point;
  const gchar *id = cby_entry_point_get_id (entry_point);
  CanterburyExecutableType type =
    _cby_entry_point_get_executable_type (self->entry_point);
  gboolean daemon = ((self->flags & LAUNCH_FLAGS_DAEMON) != 0);
  const gchar *label = daemon ? "daemon" : "app";
  LaunchNewAppInfo *appInfo;

  DEBUG ("Launched %s \"%s\" with pid %d", label, id, pid);

  appInfo = g_new0 (LaunchNewAppInfo, 1);
  appInfo->entry_point = g_object_ref (entry_point);
  appInfo->pid = pid;

  /* We cannot add a pid watcher on an externally managed service */
  if (!service_managed)
    {
      if (daemon)
        g_child_watch_add (pid, v_launch_daemon_exit_cb, NULL);
      else
        g_child_watch_add (pid, v_launch_app_exit_cb, NULL);
    }

  /* We assume that daemons' command-line arguments are uninteresting,
   * and we know that once launched, D-Bus activatable graphical
   * programs' command-line arguments are equally uninteresting. */
  if (!daemon && !_cby_entry_point_get_dbus_activatable (entry_point))
    {
      async_launch_context_fill_argv (self);
      appInfo->legacy_arguments = g_strdupv (self->argv + 1);
    }

  if (daemon)
    LaunchMgrObj.pRunningDaemonsList =
      g_slist_append (LaunchMgrObj.pRunningDaemonsList, appInfo);
  else
    LaunchMgrObj.pRunningAppsList =
      g_slist_append (LaunchMgrObj.pRunningAppsList, appInfo);

  if (!daemon && type != CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION &&
      _cby_is_target ())
    {
      CanterburyBandwidthPriority priority =
        _cby_entry_point_get_internet_bandwidth_priority (self->entry_point);

      add_pid_to_memory_cgroup (pid);

      if (priority != CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN)
        add_bw_prio_to_bandwidth_cgroup (pid, priority);
    }

  self->flags |= LAUNCH_FLAGS_NEW;
  async_launch_context_activate (self, pid);
}

static void
async_launch_context_activate (AsyncLaunchContext *self,
                               GPid pid)
{
  CbyEntryPoint *entry_point = self->entry_point;
  const gchar *id = cby_entry_point_get_id (entry_point);
  gboolean new = ((self->flags & LAUNCH_FLAGS_NEW) != 0);
  gboolean daemon = ((self->flags & LAUNCH_FLAGS_DAEMON) != 0);
  gboolean existing = !new;
  const gchar *const *legacy_arguments = NULL;
  const gchar *const *legacy_argv = NULL;

  if (!_cby_entry_point_get_dbus_activatable (entry_point))
    {
      async_launch_context_fill_argv (self);
      legacy_arguments = (const gchar * const *) self->argv + 1;
      legacy_argv = (const gchar * const *) self->argv;
    }

  if (!daemon)
    {
      ActivateFlags flags = ACTIVATE_FLAGS_NONE;

      if (new)
        flags |= ACTIVATE_FLAGS_NEW;

      activity_mgr_schedule_activation (self->entry_point,
                                        pid,
                                        (const gchar * const *) self->uris,
                                        self->platform_data,
                                        legacy_argv,
                                        self->launched_from,
                                        self->special_params, flags);
      /* TODO, temporarily disable the back button while an app is launching */
    }

  if (existing &&
      (self->flags & LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_EXISTING))
    {
      back_stack_update_entry (self->entry_point, legacy_arguments,
                               self->launched_from);
    }

  if (existing &&
      (self->flags & LAUNCH_FLAGS_START_TIMER_IF_EXISTING))
    start_launch_progress_timer();

  /* FIXME: This was traditionally after signalling the Launcher, whereas
   * the equivalent for existing processes was traditionally before
   * signalling the Launcher - but perhaps the ordering is unimportant? */
  if (new && (self->flags & LAUNCH_FLAGS_UPDATE_BACK_STACK_IF_NEW))
    {
      back_stack_update_entry (self->entry_point, legacy_arguments,
                               self->launched_from);
    }

  /* DBusActivatable entry points don't need this because they already
   * had Activate() or Open() called on them.
   * FIXME: This seems to be redundant for non-DBusActivatable entry
   * points too, so maybe it can be removed. */
  if (existing &&
      (self->flags & LAUNCH_FLAGS_EMIT_NEW_APP_STATE_IF_EXISTING) &&
      !_cby_entry_point_get_dbus_activatable (entry_point))
    emit_new_app_state (id, CANTERBURY_APP_STATE_SHOW, legacy_arguments);
}

static void
async_launch_context_launch_failed (AsyncLaunchContext *self,
                                    gboolean service_managed,
                                    GError *error)
{
  CbyEntryPoint *entry_point = self->entry_point;
  const gchar *id = cby_entry_point_get_id (entry_point);
  gboolean daemon = ((self->flags & LAUNCH_FLAGS_DAEMON) != 0);
  const gchar *label = daemon ? "daemon" : "app";

  WARNING ("Unable to launch %s \"%s\": %s", label, id, error->message);
}

static void
async_launch_context_free (AsyncLaunchContext *self)
{
  g_clear_object (&self->entry_point);
  g_strfreev (self->uris);

  if (self->platform_data != NULL)
    g_variant_unref (self->platform_data);

  g_strfreev (self->legacy_arguments);
  g_strfreev (self->argv);
  g_free (self->launched_from);
  g_slice_free (AsyncLaunchContext, self);
}

typedef struct
{
  int stdout_fd;
  int stderr_fd;
} ChildSetupData;

static void
child_setup (void *user_data)
{
  ChildSetupData *csd = user_data;

  /* We ignore any errors here, and just let it inherit Canterbury's
   * stdout, stderr if necessary */

  if (csd->stdout_fd >= 0)
    {
      dup2 (csd->stdout_fd, STDOUT_FILENO);
      g_close (csd->stdout_fd, NULL);
    }

  if (csd->stderr_fd >= 0)
    {
      dup2 (csd->stderr_fd, STDERR_FILENO);
      g_close (csd->stderr_fd, NULL);
    }
}

static void
start_service_cb (GObject *source_object,
                  GAsyncResult *result,
                  gpointer user_data)
{
  CbyServiceManager *service_manager = CBY_SERVICE_MANAGER (source_object);
  g_autoptr (GTask) task = user_data;
  AsyncLaunchContext *launch_context = g_task_get_task_data (task);
  g_autoptr (GError) error = NULL;
  GPid pid;

  pid = _cby_service_manager_start_service_finish (service_manager,
                                                   result,
                                                   &error);
  if (error)
    {
      async_launch_context_launch_failed (launch_context, TRUE, error);
      g_task_return_error (task, g_steal_pointer (&error));
    }
  else
    {
      async_launch_context_launched (launch_context, pid, TRUE);
      g_task_return_boolean (task, TRUE);
    }
}

static void
async_launch_context_start_task (GTask *task)
{
  const gchar *id;
  AsyncLaunchContext *launch_context;

  g_return_if_fail (G_IS_TASK (task));
  launch_context = g_task_get_task_data (task);
  g_return_if_fail ((launch_context->flags & LAUNCH_FLAGS_DAEMON) ||
                    !LaunchMgrObj.bLaunchInProgress);
  g_return_if_fail (CBY_IS_ENTRY_POINT (launch_context->entry_point));

  id = cby_entry_point_get_id (launch_context->entry_point);

  if (launch_context->flags & LAUNCH_FLAGS_HIDE_GLOBAL_POPUPS)
    {
      /* FIXME: it isn't clear why, but Launcher.LaunchNewApp() historically
       * called this, whereas AppLaunchDb didn't. For the moment we do this
       * for ActivateEntryPoint() but not for OpenURIs(). */
      hide_global_popups ();
    }

  if (launch_context->flags & LAUNCH_FLAGS_SET_ACTIVE_TITLE)
    {
      window_mgr_set_active (_cby_entry_point_get_category_label (launch_context->entry_point),
                             _cby_entry_point_get_mangled_display_name (launch_context->entry_point));
    }

  if (launch_context->flags & LAUNCH_FLAGS_CHECK_EXISTING_GUI)
    {
      LaunchNewAppInfo *existing = launch_mgr_find_running_app (id);

      if (existing != NULL)
        {
          DEBUG ("\"%s\" already running as an app", id);
          async_launch_context_activate (launch_context, existing->pid);
          g_task_return_boolean (task, TRUE);
          return;
        }
    }

  if (launch_context->flags & LAUNCH_FLAGS_CHECK_EXISTING_DAEMON)
    {
      LaunchNewAppInfo *existing =
        launch_mgr_find_running_daemon (launch_context->entry_point);

      if (existing != NULL)
        {
          DEBUG ("\"%s\" already running as a daemon", id);
          launch_context->flags |= LAUNCH_FLAGS_DAEMON;
          async_launch_context_activate (launch_context, existing->pid);
          g_task_return_boolean (task, TRUE);
          return;
        }
    }

  DEBUG ("\"%s\" is not running yet", id);

  if (launch_context->flags & LAUNCH_FLAGS_ONLY_EXISTING)
    {
      g_autoptr (GError) error = NULL;

      g_set_error (&error, CBY_ERROR, CBY_ERROR_LAUNCH_FAILED,
                   "%s is not already running", id);
      async_launch_context_launch_failed (launch_context, FALSE, error);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  if (launch_context->flags & LAUNCH_FLAGS_START_TIMER_IF_NEW)
    start_launch_progress_timer ();

  if (_cby_service_manager_has_service (LaunchMgrObj.service_manager,
                                        launch_context->entry_point))
    {
      _cby_service_manager_start_service_async (LaunchMgrObj.service_manager,
                                                launch_context->entry_point,
                                                NULL,
                                                start_service_cb,
                                                g_object_ref (task));
    }
  else
    {
      const gchar *working_directory;
      g_autoptr (GError) local_error = NULL;
      ChildSetupData child_setup_data = { -1, -1 };
      g_auto (GStrv) env = NULL;
      GStrv argv;
      GPid child_pid;
      gboolean ret;
      gsize i;

      async_launch_context_fill_argv (launch_context);
      argv = launch_context->argv;

      env = g_get_environ ();

      /* don't let the child inherit our notify socket, which could confuse it */
      env = g_environ_unsetenv (env, "NOTIFY_SOCKET");

      /* We do this before fork() because sd_journal_stream_fd() doesn't seem
       * to be guaranteed to be async-signal-safe. The down side of this is
       * that we end up using Canterbury's process ID. Switching to systemd-based
       * activation will fix that in future.
       *
       * If what we are running is actually canterbury-exec, then the wrong
       * streams only last until it has reopened them on behalf of the
       * new process anyway.
       *
       * These could fail, but that isn't critical; child_setup() copes
       * with that. */
      child_setup_data.stdout_fd = sd_journal_stream_fd (id, LOG_INFO, FALSE);
      child_setup_data.stderr_fd = sd_journal_stream_fd (id, LOG_WARNING, FALSE);

      working_directory =
        _cby_entry_point_get_working_directory (launch_context->entry_point);
      DEBUG ("Working directory: %s", working_directory);
      DEBUG ("argv:");
      for (i = 0; argv[i] != NULL; i++)
        DEBUG ("  %s", argv[i]);
      DEBUG ("env:");
      for (i = 0; env[i] != NULL; i++)
        DEBUG ("  %s", env[i]);

      ret = g_spawn_async (working_directory, argv, env,
                           G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
                           child_setup, &child_setup_data,
                           &child_pid, &local_error);

      if (!ret)
        {
          g_autoptr (GError) error = NULL;

          g_set_error (&error, CBY_ERROR, CBY_ERROR_LAUNCH_FAILED,
                       "Error launching “%s”: %s %d: %s", argv[0],
                       g_quark_to_string (local_error->domain), local_error->code,
                       local_error->message);
          async_launch_context_launch_failed (launch_context, FALSE, error);
          g_task_return_error (task, g_steal_pointer (&error));
        }
      else
        {
          if (child_setup_data.stdout_fd >= 0)
            g_close (child_setup_data.stdout_fd, NULL);

          if (child_setup_data.stderr_fd >= 0)
            g_close (child_setup_data.stdout_fd, NULL);

          async_launch_context_launched (launch_context, child_pid, FALSE);
          g_task_return_boolean (task, TRUE);
        }
    }
}

/*
 * launch_mgr_launch_app_async:
 * @entry_point: The entry point to activate
 * @launched_from: the reason the entry point is being activated,
 *  typically one of the constants in canterbury_app_handler.h
 *  (%CANTERBURY_LAUNCHER_CLIENT, %CANTERBURY_HOME_SCREEN_CLIENT,
 *  %CANTERBURY_BACK_KEY, etc.)
 * @special_params: "special arguments" to be passed to the compositor, which
 *  ignores them. We do not know what this means, but mildenhall-launcher
 *  always uses 0xff in practice.
 * @uris: (nullable): Either %NULL, or URIs to open during the first
 *  activation of this entry point. If the entry point wants to reopen the
 *  same URIs by default on subsequent launches, it is responsible for
 *  saving them for that purpose.
 * @platform_data: (nullable): Either %NULL, or a %G_VARIANT_TYPE_VARDICT of
 *  extra information to be passed to the entry point on its first activation.
 *  If the entry point wants to open with similar behaviour on subsequent
 *  launches, it is responsible for remembering that.
 * @legacy_arguments: If (flags & LAUNCH_FLAGS_ARGV_REPLACE), put these
 *  arguments after argv[0] (the executable), replacing whatever is specified
 *  in the CbyEntryPoint. Otherwise, insert these arguments between argv[0]
 *  and the CbyEntryPoint's argv[1].
 * @flags: Many, many flags that affect how this entry point gets launched,
 *  preserving existing behaviour where various code paths had different
 *  contents.
 * @cancellable: optional GCancellable object, NULL to ignore.
 * @callback: a #GAsyncReadyCallback;
 * @user_data: user data passed to callback.
 *
 * Launch an entry point as if it was launched by the launcher menu or the
 * content handover service, either by activating an existing instance
 * (if suitable flags were specified) or by running a new instance.
 *
 * When the operation is finished, callback will be called. You can then call
 * launch_mgr_launch_app_finish() to get the result of the operation.
 */
void
launch_mgr_launch_app_async (CbyEntryPoint *entry_point,
                             const gchar *launched_from,
                             guint special_params,
                             const gchar * const *uris,
                             GVariant *platform_data,
                             const gchar * const *arguments,
                             LaunchFlags flags,
                             GCancellable *cancellable,
                             GAsyncReadyCallback callback,
                             gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (GVariant) platform_data_ref = NULL;
  AsyncLaunchContext *launch_context;

  if (platform_data != NULL)
    platform_data_ref = g_variant_ref_sink (platform_data);

  g_return_if_fail (CBY_IS_ENTRY_POINT (entry_point));
  g_return_if_fail (cby_entry_point_get_executable (entry_point) != NULL);
  g_return_if_fail (callback != NULL);

  /* Callers that are launching daemons should use launch_daemon_async() */
  g_return_if_fail ((flags & LAUNCH_FLAGS_DAEMON) == 0);
  /* The NEW flag is used internally, not to be used by callers */
  g_return_if_fail ((flags & LAUNCH_FLAGS_NEW) == 0);

  switch (_cby_entry_point_get_executable_type (entry_point))
    {
      case CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION:
      case CANTERBURY_EXECUTABLE_TYPE_APPLICATION:
      case CANTERBURY_EXECUTABLE_TYPE_EXT_APP:
        /* OK, it is meaningful to activate these */
        break;

      case CANTERBURY_EXECUTABLE_TYPE_UNKNOWN:
      default:
        WARNING ("Unknown executable type %d for \"%s\", assuming it is not "
                 "a valid graphical program",
                 _cby_entry_point_get_executable_type (entry_point),
                 cby_entry_point_get_id (entry_point));
        /* fall through - return an error */
      case CANTERBURY_EXECUTABLE_TYPE_SERVICE:
      case CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE:
        g_task_report_new_error (NULL, callback, user_data,
                                 launch_mgr_launch_app_async,
                                 CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT,
                                 "Service or agent \"%s\" cannot be launched "
                                 "as a graphical program",
                                 cby_entry_point_get_id (entry_point));
        return;

    }

  launch_context = async_launch_context_new (entry_point, launched_from,
                                             special_params, uris,
                                             platform_data, arguments, flags);

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_source_tag (task, launch_mgr_launch_app_async);
  g_task_set_task_data (task, launch_context,
                        (GDestroyNotify) async_launch_context_free);

  if (LaunchMgrObj.bLaunchInProgress)
    g_queue_push_tail (&LaunchMgrObj.queued_launches, g_steal_pointer (&task));
  else
    async_launch_context_start_task (task);
}

gboolean
launch_mgr_launch_app_finish (GAsyncResult *result,
                              GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, NULL), FALSE);
  g_return_val_if_fail (g_async_result_is_tagged (result,
                                                  launch_mgr_launch_app_async),
                        FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
launch_daemon_async_cb (GObject *source_object,
                        GAsyncResult *result,
                        gpointer user_data)
{
  GTask *task = G_TASK (result);
  g_autoptr (GError) error = NULL;
  AsyncLaunchContext *launch_context = g_task_get_task_data (task);

  if (!g_task_propagate_boolean (task, &error))
    WARNING ("Unable to launch daemon \"%s\": %s",
             cby_entry_point_get_id (launch_context->entry_point),
             error->message);
  else
    DEBUG ("Launched daemon \"%s\"",
           cby_entry_point_get_id (launch_context->entry_point));
}

static gboolean
launch_daemon_async (AsyncLaunchContext *launch_context)
{
  g_autoptr (GTask) task = NULL;

  g_return_val_if_fail (CBY_IS_ENTRY_POINT (launch_context->entry_point), FALSE);
  g_return_val_if_fail (launch_context->flags & LAUNCH_FLAGS_DAEMON, FALSE);

  task = g_task_new (NULL, NULL, launch_daemon_async_cb, NULL);
  g_task_set_source_tag (task, launch_daemon_async);
  g_task_set_task_data (task, launch_context,
                        (GDestroyNotify) async_launch_context_free);

  async_launch_context_start_task (task);
  return FALSE;
}

gboolean
launch_mgr_kill_the_current_running_application (CbyEntryPoint *entry_point)
{
  guint running_apps_list_len = g_slist_length (LaunchMgrObj.pRunningAppsList);
  guint cnt;

  for (cnt = 0; cnt < running_apps_list_len; cnt++)
    {
      LaunchNewAppInfo *app_info  = g_slist_nth_data (LaunchMgrObj.pRunningAppsList, cnt);

      if (entry_point == app_info->entry_point)
        {
          const gchar *id = cby_entry_point_get_id (entry_point);

          DEBUG ("Killing entry point %s (pid %d)", id, app_info->pid);

          if (!launch_mgr_kill_running_app (app_info->entry_point,
                                            app_info->pid))
            {
              WARNING ("Failed to kill entry point %s (pid %d)",
                       id, app_info->pid);
            }

          return TRUE;
        }
    }

  return FALSE;
}
