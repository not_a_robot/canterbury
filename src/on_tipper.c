/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#undef G_LOG_DOMAIN
#define G_LOG_DOMAIN "hardkeys-mgr"
#include "hard_keys.h"

#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include "canterbury/messages-internal.h"
#include "shutdown.h"
#include "util.h"

static gboolean kill_all_services(gpointer data);
static gboolean shutdown_the_system(gpointer data);

gboolean handle_register_shutdown_clb( CanterburyHardKeys *object,
                                       GDBusMethodInvocation   *invocation,
                                       gpointer                 pUserData )
{
  guint cnt;
  LaunchNewAppInfo *pAppInfo = NULL;
  guint uin_RunningAppsListLen;

  v_set_system_shutdown_signal(TRUE);

  //send wm display off signal
  v_display_off(TRUE);

  //send off signals to all applications
  uin_RunningAppsListLen = get_running_app_info_stack_length ();
  for( cnt = 0 ; cnt< uin_RunningAppsListLen ; cnt++)
  {
    pAppInfo = get_running_app_info_at_position( cnt );

    if (!launch_mgr_stop_running_app (pAppInfo->entry_point, pAppInfo->pid))
      {
        WARNING ("Failed to stop entry point \"%s\"",
                 cby_entry_point_get_id (pAppInfo->entry_point));
      }
  }

  g_timeout_add_seconds(2 , kill_all_services , NULL);

  canterbury_hard_keys_complete_shutdown (object, invocation);
  return TRUE;
}

static gboolean kill_all_services(gpointer data)
{
  //send off signals to all agents and services
  guint cnt;
  LaunchNewAppInfo *pAppInfo = NULL;
  guint uin_RunningDaemonsListLen =  get_running_daemon_info_stack_length( );
  for( cnt = 0 ; cnt< uin_RunningDaemonsListLen ; cnt++)
  {
    pAppInfo = get_running_daemon_info_at_position( cnt );

    if (!launch_mgr_stop_running_app (pAppInfo->entry_point, pAppInfo->pid))
      {
        WARNING ("Failed to stop entry point \"%s\"",
                 cby_entry_point_get_id (pAppInfo->entry_point));
      }
  }

  g_timeout_add_seconds(2 , shutdown_the_system , NULL);
  return FALSE;
}

static void
shutdown_cb (GObject      *source_object,
             GAsyncResult *result,
             gpointer      user_data)
{
  GError *error = NULL;

  canterbury_shutdown_finish (result, &error);

  if (error != NULL)
    {
      g_printerr ("SHUTDOWN MGR: shutdown unsuccessful: %s\n",
                  error->message);
      g_error_free (error);
    }
}

static gboolean shutdown_the_system(gpointer data)
{
    //save the LUM state
  if(b_get_last_user_mode_status())
  {
    last_user_mode_save_status();
  }

  g_printerr(" SHUTDOWN MGR: will this print should always come \n");
  //send a shutdown signal
  if (!_cby_is_target ())
	  exit(0);
  else
  {
      canterbury_shutdown_async (CANTERBURY_SHUTDOWN_POWER_OFF, TRUE, NULL,
                                 shutdown_cb, NULL);
  }

  return FALSE;
}
