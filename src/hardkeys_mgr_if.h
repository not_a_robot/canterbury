/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __HARDKEY_MGR_IF__
#define __HARDKEY_MGR_IF__

#include <glib.h>

#include <canterbury/canterbury-platform.h>

G_BEGIN_DECLS

typedef struct _BackStack BackStack ;

struct _BackStack
{
	gchar*  pLaunchedFrom;
	CbyEntryPoint *entry_point;
	gchar** pArgvList;
};

void init_hardkey (GDBusConnection *connection);

typedef struct
{
  /*<private>*/
  gpointer _link;
} CbyBackStackIter;

void cby_back_stack_iter_init (CbyBackStackIter *iter);
gboolean cby_back_stack_iter_next (CbyBackStackIter *iter, BackStack **entry);

void back_stack_remove_registration (CbyEntryPoint *entry_point);

void back_stack_remove_entry (CbyEntryPoint *entry_point);

void back_stack_update_entry (CbyEntryPoint *entry_point,
                              const gchar * const *argv,
                              const gchar *launched_from);

void
clear_back_stack( void );

void hard_keys_emit_home_press_response (gboolean result);

void hard_keys_emit_disable_back_press_response (gboolean disable_status);

G_END_DECLS

#endif //__HARDKEY_MGR_IF__
