/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __APP_LAUNCH_MGR_IF__
#define __APP_LAUNCH_MGR_IF__

#include <glib.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/* app launch mechanism */

void init_app_launch (GDBusConnection *connection);

void
v_launch_mgr_get_appliation_mimes( GList** ,
                                   GList**);
void
app_launch_mgr_update_app_details( gboolean arg_app_state,
                                   const gchar *arg_app_name,
                                   const gchar *arg_app_mime,
                                   const gchar *const *arg_app_argmime);

G_END_DECLS

#endif //__APP_LAUNCH_MGR_IF__
