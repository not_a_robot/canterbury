/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <canterbury/canterbury.h>
#include <errno.h>
#include <string.h>
#include <sys/apparmor.h>
#include <unistd.h>

#include <glib.h>

#include "canterbury/gdbus/canterbury.h"

#include "tests/common.h"

#define SETTINGS_BUNDLE_ID "org.apertis.CanterburyTests.LaunchPreferences"
#define SETTINGS_ENTRY_POINT SETTINGS_BUNDLE_ID

#define STORE_BUNDLE_ID "org.apertis.CanterburyTests.LaunchPreferencesStoreApp"
#define BUILT_IN_BUNDLE_ID "org.apertis.CanterburyTests.LaunchPreferencesBuiltInApp"
#define OTHER_BUNDLE_ID "org.apertis.CanterburyTests.LaunchPreferences.SomeOtherApp"

typedef struct
{
  GSettings *settings;  /* owned */
  gchar *old_preferences_entry_point;  /* owned */
  guint helper_spawn_time;  /* seconds */
  gboolean acquired_name;
} Fixture;

typedef struct
{
  const gchar *test_profile;
  const gchar *preferences_bundle_id;
  GQuark expected_error_domain;  /* 0 if no error is expected */
  gint expected_error_code;  /* 0 if no error is expected */
} Config;

typedef guint SourceId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (SourceId, g_source_remove, 0)

static void
name_acquired_cb (GDBusConnection *connection,
                  const gchar *name,
                  gpointer user_data)
{
  Fixture *f = user_data;

  f->acquired_name = TRUE;
}

static void
name_lost_cb (GDBusConnection *connection,
              const gchar *name,
              gpointer user_data)
{
  g_error ("Unable to acquire name %s", name);
}


static void
setup (Fixture       *f,
       gconstpointer  user_data)
{
  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (30);

  if (!g_test_subprocess () && getuid () != 0)
    {
      /* Update the system settings so that Canterbury will spawn our helper as the
       * preferences application, rather than mildenhall-settings.
       *
       * It will do this by launching
       * org.apertis.CanterburyTests.LaunchPreferences.
       *
       * We do the actual settings change in a subprocess and wait for the
       * change to become visible to the parent. This ensures that the D-Bus
       * signal notifying processes other than the setter about the dconf
       * change has gone out, which in turn ensures that when our test
       * subprocess makes D-Bus calls to Canterbury, those D-Bus calls will
       * arrive after the dconf change notification. Hopefully that is enough
       * to ensure that Canterbury launches our helper and not
       * mildenhall-settings. */
      const gchar * const argv[] =
      {
        "gsettings", "set", "org.apertis.Canterbury.Config",
        "preferences-entry-point", SETTINGS_ENTRY_POINT, NULL
      };
      g_autoptr (GError) error = NULL;

      f->settings = g_settings_new ("org.apertis.Canterbury.Config");
      f->old_preferences_entry_point = g_settings_get_string (f->settings,
                                                              "preferences-entry-point");

      g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH, NULL,
                    NULL, NULL, NULL, NULL, &error);
      g_assert_no_error (error);

      while (TRUE)
        {
          g_autofree gchar *new_value =
              g_settings_get_string (f->settings, "preferences-entry-point");

          if (g_strcmp0 (new_value, SETTINGS_ENTRY_POINT) == 0)
            break;

          g_main_context_iteration (NULL, TRUE);
        }
    }
}

/* Call aa_change_profile() and abort if it fails. */
static void
assert_aa_change_profile (const gchar *profile)
{
  g_test_message ("Changing AppArmor profile to %s", profile);

  if (aa_change_profile (profile) != 0)
    {
      g_error ("aa_change_profile(\"%s\") failed with %i: %s",
               profile, errno, strerror (errno));
      g_assert_not_reached ();
    }
}

typedef enum
{
  NAME_UNKNOWN,
  NAME_APPEARED,
  NAME_VANISHED,
} NameState;

typedef struct
{
  gboolean waiting_name_appeared;
  guint name_vanished_source;
  NameState name_state;
} ServiceState;

static void
name_appeared_cb (GDBusConnection *connection,
                  const gchar     *name,
                  const gchar     *name_owner,
                  gpointer         user_data)
{
  ServiceState *service_state = user_data;

  g_assert_cmpint (service_state->name_state, ==, NAME_UNKNOWN);
  service_state->name_state = NAME_APPEARED;
}

static gboolean
set_name_vanished (gpointer user_data)
{
  ServiceState *service_state = user_data;

  service_state->name_state = NAME_VANISHED;
  service_state->name_vanished_source = 0;

  return G_SOURCE_REMOVE;
}

static void
name_vanished_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  ServiceState *service_state = user_data;

  g_assert_cmpint (service_state->name_state, !=, NAME_VANISHED);
  if (service_state->name_state == NAME_APPEARED)
    {
      if (!service_state->waiting_name_appeared)
        service_state->name_state = NAME_VANISHED;
      else if (!service_state->name_vanished_source)
        {
          /* Delay setting the state to NAME_VANISHED until the second set of
           * calls to g_main_context_iteration(), so that the bulk of the test
           * is executed with the state set as NAME_APPEARED.
           * This happens if the helper program runs and exits entirely within
           * this initial set of g_main_ccontext_iteration() calls. */
          service_state->name_vanished_source = g_idle_add (set_name_vanished, user_data);
        }
    }
}

static gboolean
timeout_cb (gpointer user_data)
{
  gboolean *timeout = user_data;

  *timeout = TRUE;
  return G_SOURCE_CONTINUE;  /* removed by g_source_remove() in the caller */
}

/*
 * Check that the LaunchAppPreferences D-Bus API of Canterbury works, and
 * correctly rejects invalid input, or input which would contravene its security
 * policy.
 *
 * Do this by temporarily changing AppArmor profile to one which makes this
 * process look like a store app, a built-in app, a platform process, etc., and
 * setting Canterbury’s preferences-entry-point to launch-preferences-helper;
 * then calling LaunchAppPreferences(). launch-preferences-helper should be
 * launched when we expect, and not otherwise. This can be detected by watching
 * for its well-known name on the session bus.
 *
 * During unit testing ("make check") this is just skipped.
 */
static void
test_launch_preferences (Fixture       *f,
                         gconstpointer  user_data)
{
  const Config *c = user_data;

  if (getuid () == 0)
    {
      g_test_skip ("This test uses dconf and cannot be run as root");
      return;
    }

  if (g_test_subprocess ())
    {
      const gchar *test_profile;
      g_autoptr (CanterburyAppDbHandler) proxy = NULL;
      g_autoptr (GDBusProxy) unit_proxy = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GVariant) tuple = NULL;
      g_auto (TestsNameOwnership) signal_done_id = 0;
      g_auto (TestsNameWatch) watch_id = 0;
      g_auto (SourceId) timeout_id = 0;
      ServiceState service_state = {
        FALSE, 0, NAME_UNKNOWN
      };
      gboolean timeout = FALSE;
      g_autofree gchar *bus_address = NULL;
      g_autofree gchar *unit_path = NULL;
      g_autoptr (GDBusConnection) connection = NULL;
      guint timeout_length = 0;  /* seconds */
      const gchar *timeout_length_string;
      const gchar *unit_name;

      if (g_str_has_prefix (c->test_profile, "${G_TEST_BUILDDIR}/"))
        test_profile = g_test_get_filename (G_TEST_BUILT,
            c->test_profile + strlen ("${G_TEST_BUILDDIR}/"), NULL);
      else
        test_profile = c->test_profile;

      g_test_message ("%s: %s, %s, %s",
                      G_STRFUNC, test_profile,
                      c->preferences_bundle_id,
                      c->expected_error_code != 0 ? "error expected" : "error not expected");

      /* Change to the given AppArmor profile, so that we appear to
       * Canterbury as that program. */
      assert_aa_change_profile (test_profile);

      /* Launch the preferences for the given bundle ID. Use a new D-Bus connection
       * each time so that our credentials are definitely re-queried. Avoid using
       * cached connections. */
      bus_address = g_dbus_address_get_for_bus_sync (G_BUS_TYPE_SESSION, NULL,
                                                     &error);
      g_assert_no_error (error);

      connection = g_dbus_connection_new_for_address_sync (bus_address,
                                                           G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                           G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                           NULL, NULL,
                                                           &error);
      g_assert_no_error (error);

      unit_name = SETTINGS_ENTRY_POINT ".service";
      tuple = g_dbus_connection_call_sync (connection,
                                           SYSTEMD_BUS_NAME,
                                           SYSTEMD_PATH_MANAGER,
                                           SYSTEMD_IFACE_MANAGER,
                                           "LoadUnit",
                                           g_variant_new ("(s)", unit_name),
                                           G_VARIANT_TYPE ("(o)"),
                                           G_DBUS_CALL_FLAGS_NONE,
                                           -1, NULL, &error);
      g_assert_no_error (error);
      g_variant_get (tuple, "(o)", &unit_path);

      unit_proxy = g_dbus_proxy_new_sync (connection, G_DBUS_PROXY_FLAGS_NONE,
                                          NULL, SYSTEMD_BUS_NAME, unit_path,
                                          SYSTEMD_IFACE_UNIT, NULL, &error);
      g_assert_no_error (error);

      proxy = canterbury_app_db_handler_proxy_new_sync (connection,
                                                        G_DBUS_PROXY_FLAGS_NONE,
                                                        "org.apertis.Canterbury",
                                                        "/org/apertis/Canterbury/AppDbHandler",
                                                        NULL,
                                                        &error);
      g_assert_no_error (error);

      /* If waiting for a negative result, wait twice as long as the previous
       * positive result took, or default to 5 seconds. */
      timeout_length_string = g_getenv ("CANTERBURY_TESTS_TIMEOUT");
      if (timeout_length_string != NULL)
        timeout_length = g_ascii_strtoull (timeout_length_string, NULL, 10);
      if (timeout_length == 0)
        timeout_length = 5;  /* seconds */

      g_test_message ("timeout_length: %u", timeout_length);

      service_state.waiting_name_appeared = TRUE;

      watch_id = g_bus_watch_name_on_connection (g_dbus_proxy_get_connection (G_DBUS_PROXY (proxy)),
                                                 SETTINGS_ENTRY_POINT ".Helper",
                                                 G_BUS_NAME_WATCHER_FLAGS_NONE,
                                                 name_appeared_cb,
                                                 name_vanished_cb,
                                                 &service_state, NULL);
      timeout_id = g_timeout_add_seconds (timeout_length, timeout_cb, &timeout);

      canterbury_app_db_handler_call_launch_app_preferences_sync (proxy,
                                                                  c->preferences_bundle_id,
                                                                  NULL,
                                                                  &error);

      while (service_state.name_state == NAME_UNKNOWN && !timeout)
        g_main_context_iteration (NULL, TRUE);

      service_state.waiting_name_appeared = FALSE;

      if (c->expected_error_domain == 0 && c->expected_error_code == 0)
        {
          /* The launch should have been successful, and the helper (effectively
           * mildenhall-settings) should have been spawned. */
          g_assert_no_error (error);
          g_assert_cmpint (service_state.name_state, ==, NAME_APPEARED);
          g_assert_false (timeout);
        }
      else
        {
          /* The launch should have failed, and the helper must not have been
           * spawned. */
          g_assert_error (error, c->expected_error_domain, c->expected_error_code);
          g_assert_cmpint (service_state.name_state, ==, NAME_UNKNOWN);
          g_assert_true (timeout);
        }

      /* We need to give Canterbury time to find out the process's process ID
       * before we let it exit. As a way to signal our presence to it without
       * having to give it unrealistic AppArmor permissions, take a bus
       * name. */
      signal_done_id = g_bus_own_name_on_connection (connection,
                                                     SETTINGS_BUNDLE_ID ".Done",
                                                     G_BUS_NAME_OWNER_FLAGS_NONE,
                                                     name_acquired_cb,
                                                     name_lost_cb,
                                                     f, NULL);

      while (!f->acquired_name)
        g_main_context_iteration (NULL, TRUE);

      /* Wait for the helper to exit. */
      while (service_state.name_state == NAME_APPEARED)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpint (service_state.name_vanished_source, ==, 0);

      /* Wait for Canterbury to notice that the process has gone away.
       * To do this, we wait for the unit to signal that it is inactive.
       * This is the same signal that Canterbury responds to, and the
       * dbus-daemon imposes a total order on messages, so as long as
       * we're careful not to send any messages for the next test until after
       * we have received this one, any subsequent messages we send will be
       * processed by Canterbury after it has processed its copy of
       * this signal. Otherwise, the next test might start before Canterbury
       * knows the helper has exited. */
      while (TRUE)
        {
          g_autoptr (GVariant) state =
              g_dbus_proxy_get_cached_property (unit_proxy, "ActiveState");
          const gchar *s;

          g_assert_cmpstr (g_variant_get_type_string (state), ==, "s");
          g_variant_get (state, "&s", &s);

          if (g_strcmp0 (s, "inactive") == 0)
            break;

          g_main_context_iteration (NULL, TRUE);
        }
    }
  else
    {
      gint64 start_time, diff;
      g_autofree gchar *timeout_length_string = NULL;

      start_time = g_get_real_time ();

      /* Run the test in a subprocess. */
      g_test_trap_subprocess (NULL, 0,
                              G_TEST_SUBPROCESS_INHERIT_STDOUT |
                              G_TEST_SUBPROCESS_INHERIT_STDERR);
      g_test_trap_assert_passed ();

      /* Work out how long it took to run the test; this can be used in future
       * tests as the basis for the timeout when checking for negative results,
       * above. We have to pass this to the subprocesses as an environment
       * variable, as they are forked with no shared state. */
      diff = g_get_real_time () - start_time;
      f->helper_spawn_time = (diff + (G_USEC_PER_SEC - 1)) / G_USEC_PER_SEC;
      timeout_length_string = g_strdup_printf ("%u", f->helper_spawn_time);
      g_setenv ("CANTERBURY_TESTS_TIMEOUT", timeout_length_string, TRUE);
      g_test_message ("setting helper_spawn_time to %u", f->helper_spawn_time);
    }
}

static void
teardown (Fixture       *f,
          gconstpointer  user_data)
{
  if (!g_test_subprocess () && getuid () != 0)
    {
      /* See comments in setup() */
      const gchar * const argv[] =
      {
        "gsettings", "set", "org.apertis.Canterbury.Config",
        "preferences-entry-point", f->old_preferences_entry_point, NULL
      };
      g_autoptr (GError) error = NULL;

      g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH, NULL,
                    NULL, NULL, NULL, NULL, &error);
      g_assert_no_error (error);

      while (TRUE)
        {
          g_autofree gchar *new_value =
              g_settings_get_string (f->settings, "preferences-entry-point");

          if (g_strcmp0 (new_value, f->old_preferences_entry_point) == 0)
            break;

          g_main_context_iteration (NULL, TRUE);
        }
    }

  /* cancel watchdog */
  alarm (0);
}

int
main (int argc,
    char **argv)
{
  const Config vectors[] = {
    /* test_profile, preferences_bundle_id, expected_error_domain, *_code */

    /* The settings application itself is allowed to open any schema. Normally,
     * this would be org.apertis.Mildenhall.Settings; but we change the settings
     * application in setup(), so it’s actually org.apertis.CanterburyTests.
     *
     * List some positive tests first, so that f->helper_spawn_time can be
     * initialised: */
    { "/usr/Applications/" SETTINGS_BUNDLE_ID "/bin/mildenhall-settings",
      STORE_BUNDLE_ID, 0, 0 },
    { "/usr/Applications/" SETTINGS_BUNDLE_ID "/bin/mildenhall-settings",
      BUILT_IN_BUNDLE_ID, 0, 0 },
    { "/usr/Applications/" SETTINGS_BUNDLE_ID "/bin/mildenhall-settings",
      SETTINGS_BUNDLE_ID, 0, 0 },
    /* unconfined and platform programs are not allowed to open any schemas: */
    { "unconfined", STORE_BUNDLE_ID, CBY_ERROR, CBY_ERROR_ACCESS_DENIED },
    { "${G_TEST_BUILDDIR}/platform-service", STORE_BUNDLE_ID,
      CBY_ERROR, CBY_ERROR_ACCESS_DENIED },
    /* Built-in apps are only allowed to open their own schemas: */
    { "/usr/Applications/" BUILT_IN_BUNDLE_ID "/bin/built-in-app",
      BUILT_IN_BUNDLE_ID, 0, 0 },
    { "/usr/Applications/" BUILT_IN_BUNDLE_ID "/bin/built-in-app",
      "org.apertis.CanterburyTests.LaunchPreferences.SomeOtherApp",
      CBY_ERROR, CBY_ERROR_ACCESS_DENIED },
    /* Store apps are only allowed to open their own schemas: */
    { "/Applications/" STORE_BUNDLE_ID "/bin/store-app", STORE_BUNDLE_ID,
      0, 0 },
    { "/Applications/" STORE_BUNDLE_ID "/bin/store-app", OTHER_BUNDLE_ID,
      CBY_ERROR, CBY_ERROR_ACCESS_DENIED },
    /* Schema names are validated: */
    { "/usr/Applications/" SETTINGS_BUNDLE_ID "/bin/mildenhall-settings",
      "not a valid bundle ID", CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT },
    { "/usr/Applications/" SETTINGS_BUNDLE_ID "/bin/mildenhall-settings",
      "", CBY_ERROR, CBY_ERROR_INVALID_ARGUMENT },
  };
  gsize i;

  g_test_init (&argc, &argv, NULL);

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autofree gchar *test_path = NULL;

      test_path = g_strdup_printf ("/launch-preferences/%" G_GSIZE_FORMAT, i);
      g_test_add (test_path, Fixture, &vectors[i],
                  setup, test_launch_preferences, teardown);
    }

  return g_test_run ();
}
