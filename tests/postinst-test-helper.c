/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

/*
 * postinst-test-helper DIRECTORY VERSION
 *
 * Helper program for tests/postinst-prerm, to verify that AppArmor profiles
 * are set up during installation.
 *
 * Attempt to write DIRECTORY/both-may-write, DIRECTORY/neither-may-write,
 * DIRECTORY/v1-may-write and DIRECTORY/v2-may-write. Assert that the
 * results are consistent with VERSION.
 */

int
main (int argc,
      char **argv)
{
  const char *dir = argv[1];
  const char *version = argv[2];
  g_autofree gchar *both_may_write =
      g_build_filename (dir, "both-may-write", NULL);
  g_autofree gchar *neither_may_write =
      g_build_filename (dir, "neither-may-write", NULL);
  g_autofree gchar *v1_may_write =
      g_build_filename (dir, "v1-may-write", NULL);
  g_autofree gchar *v2_may_write =
      g_build_filename (dir, "v2-may-write", NULL);
  g_autoptr (GError) error = NULL;
  g_autoptr (GPtrArray) expect_success = g_ptr_array_sized_new (2);
  g_autoptr (GPtrArray) expect_failure = g_ptr_array_sized_new (2);
  gboolean failed = FALSE;
  guint i;

  g_printerr ("# %s imitating app-bundle version %s\n", g_get_prgname (),
              version);

  g_ptr_array_add (expect_success, both_may_write);
  g_ptr_array_add (expect_failure, neither_may_write);

  if (version[0] == '1')
    g_ptr_array_add (expect_success, v1_may_write);
  else
    g_ptr_array_add (expect_failure, v1_may_write);

  if (version[0] == '2')
    g_ptr_array_add (expect_success, v2_may_write);
  else
    g_ptr_array_add (expect_failure, v2_may_write);

  for (i = 0; i < expect_success->len; i++)
    {
      const gchar *f = g_ptr_array_index (expect_success, i);

      if (g_file_set_contents (f, version, -1, &error))
        {
          g_printerr ("# write %s: OK, as expected\n", f);
        }
      else
        {
          g_printerr ("# write %s: unexpected failure: %s\n", f,
                      error->message);
          failed = TRUE;
          g_clear_error (&error);
        }
    }

  for (i = 0; i < expect_failure->len; i++)
    {
      const gchar *f = g_ptr_array_index (expect_failure, i);

      if (g_file_set_contents (f, version, -1, &error))
        {
          g_printerr ("# write %s: unexpected success\n", f);
          failed = TRUE;
        }
      else if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_ACCES))
        {
          g_printerr ("# write %s: failed as expected: %s\n", f,
                      error->message);
          g_clear_error (&error);
        }
      else
        {
          g_printerr ("# write %s: failed, but not like I expected: %s\n", f,
                      error->message);
          failed = TRUE;
          g_clear_error (&error);
        }
    }

  return failed ? 1 : 0;
}
