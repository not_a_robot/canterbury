/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <sys/time.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/platform/environment.h"

#include "src/service-manager.h"
#include "tests/common.h"

#define TEST_BUNDLE_ID "org.apertis.CanterburyTests.ServiceManager"

typedef struct
{
  TestsAppBundle bundle;
  GDBusProxy *app_store_proxy;
  GDBusProxy *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
} Fixture;

static const gchar * const version_numbers[] = { "1.0", "2.0" };

typedef struct
{
  gchar *tmpdir;
  gchar *xdg_runtime_dir;
} TestsTempDir;

static gchar *
tests_temp_dir_init (TestsTempDir *self)
{
  g_autoptr (GError) error = NULL;

  g_assert_nonnull (self);

  self->tmpdir = g_dir_make_tmp ("canterbury-test.XXXXXX", &error);
  g_assert_no_error (error);
  g_assert_nonnull (self->tmpdir);

  self->xdg_runtime_dir =
      g_build_filename (self->tmpdir, "run", NULL);

  return NULL;
}

static void
tests_fix_permissions (const gchar *dir)
{
  g_autoptr (GError) error = NULL;
  gint exit_status;

  /* We need to chmod it in case it's a copy of the read-only data
   * during distcheck. */
  const gchar *argv[] = { "chmod", "-R", "u+w", "<tmpdir>", NULL };

  argv[3] = dir;

  g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
tests_remove_dir (const gchar *dir)
{
  g_autoptr (GError) error = NULL;
  const gchar *argv[] = { "rm", "-fr", "<dir>", NULL };
  gint exit_status;

  g_assert_nonnull (dir);

  tests_fix_permissions (dir);

  argv[2] = dir;
  g_spawn_sync (NULL, (gchar **) argv, NULL,     /* envp */
                G_SPAWN_SEARCH_PATH, NULL, NULL, /* child setup */
                NULL,                            /* stdout */
                NULL,                            /* stderr */
                &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
tests_temp_dir_cleanup (TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_runtime_dir);

  tests_remove_dir (self->tmpdir);
  g_free (self->tmpdir);
  g_free (self->xdg_runtime_dir);
}

static void
tests_temp_dir_setup (const TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_runtime_dir);

  g_mkdir_with_parents (self->xdg_runtime_dir, 0700);
}

static void
tests_temp_dir_teardown (const TestsTempDir *self)
{
  g_assert_nonnull (self);
  g_assert_nonnull (self->tmpdir);
  g_assert_nonnull (self->xdg_runtime_dir);

  tests_remove_dir (self->xdg_runtime_dir);
}

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, f->app_store_proxy,
                                f->bundle_manager1_proxy, TEST_BUNDLE_ID,
                                f->ordinary_uid, version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;

  tests_temp_dir_setup (temp_dir);
}

static gchar *
get_entry_point_service_path (const gchar *entry_point_id,
                              CanterburyExecutableType executable_type)
{
  g_autofree gchar *service_file = NULL;

  g_assert_true (executable_type == CANTERBURY_EXECUTABLE_TYPE_APPLICATION ||
                 executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE);

  service_file = g_strdup_printf ("%s.service", entry_point_id);

  /* Entry points should have a service file located at $XDG_RUNTIME_DIR/systemd/transient/ */
  return g_build_filename (g_get_user_runtime_dir (), "systemd", "transient",
                           service_file, NULL);
}

static gchar *
get_expected_service_contents (Fixture *f,
                               const gchar *entry_point_id,
                               const gchar *version,
                               CanterburyExecutableType executable_type)
{
  g_autoptr (GString) buffer = g_string_new ("[Service]\n");

  g_string_append_printf (buffer,
                          "ExecStart=%s/canterbury-exec -- %s/%s/bin/%s version %s\n",
                          BINDIR, CBY_PATH_PREFIX_STORE_BUNDLE, f->bundle.id,
                          entry_point_id, version);
  g_string_append_printf (buffer, "WorkingDirectory=%s/%s\n",
                          CBY_PATH_PREFIX_STORE_BUNDLE, f->bundle.id);
  g_string_append_printf (buffer, "Slice=%s.slice\n", f->bundle.id);
  g_string_append_printf (buffer, "TimeoutStopSec=%ds\n",
                          CBY_SERVICE_STOP_TIMEOUT_SEC);
  g_string_append (buffer, "Type=simple\n");

  if (executable_type == CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE)
    {
      g_string_append (buffer, "Restart=on-failure\n"
                               "RestartSec=5s\n"
                               "[Unit]\n"
                               "StartLimitBurst=5\n"
                               "StartLimitIntervalSec=20min\n");
    }

  return g_string_free (g_steal_pointer (&buffer), FALSE);
}

static void
check_file_contents (const gchar *filename,
                     const gchar *expected_contents)
{
  g_autofree gchar *actual_contents = NULL;
  g_autoptr (GError) error = NULL;

  g_file_get_contents (filename, &actual_contents, NULL, &error);
  g_assert_no_error (error);

  g_assert_cmpstr (actual_contents, ==, expected_contents);
}

static void
inc_counter (CbyServiceManager *service_manager,
             CbyEntryPoint *entry_point,
             guint *counter)
{
  g_assert (CBY_IS_SERVICE_MANAGER (service_manager));
  g_assert (CBY_IS_ENTRY_POINT (entry_point));
  g_assert_nonnull (counter);

  *counter += 1;
}

static void
test_services_availability (Fixture *f,
                            gconstpointer unused G_GNUC_UNUSED)
{
  g_autoptr (GDBusConnection) session_bus = NULL;
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (CbyEntryPointIndex) entry_point_index = NULL;
  g_autoptr (CbyServiceManager) service_manager = NULL;
  g_autofree gchar *unpack_to = NULL;
  g_autofree gchar *main_entry_point_service_path = NULL;
  g_autofree gchar *main_entry_point_service_contents = NULL;
  g_autofree gchar *main_entry_point_service_contents_v2 = NULL;
  g_autofree gchar *agent_entry_point_id = NULL;
  g_autofree gchar *agent_entry_point_service_path = NULL;
  g_autofree gchar *agent_entry_point_service_contents = NULL;
  g_autofree gchar *ribchester_full = g_find_program_in_path ("ribchester");
  g_autoptr (GError) error = NULL;
  GStatBuf stat_buf;
  struct timeval then;
  const TestsVersion *old_version = &f->bundle.versions[0];
  const TestsVersion *new_version = &f->bundle.versions[1];
  gulong service_created_signal_id;
  gulong service_updated_signal_id;
  gulong service_removed_signal_id;
  guint service_created = 0;
  guint service_updated = 0;
  guint service_removed = 0;
  guint expect_services;
  gboolean use_low_level = (ribchester_full != NULL);

  tests_assert_syscall_0 (gettimeofday (&then, NULL));

  if (!tests_require_running_as_root ())
    return;

  if (g_getenv ("USING_SYSTEMD_MOCK") == NULL)
    {
      g_test_skip ("This test requires running with a mocked systemd (tests/systemd-mock-run.py)");
      return;
    }

  if (g_test_failed ())
    return;

  session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (session_bus);

  /* Setup entry points helper variables */
  agent_entry_point_id = g_strdup_printf ("%s.Agent", f->bundle.id);

  main_entry_point_service_path = get_entry_point_service_path (f->bundle.id,
                                                                CANTERBURY_EXECUTABLE_TYPE_APPLICATION);
  agent_entry_point_service_path = get_entry_point_service_path (agent_entry_point_id,
                                                                 CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE);

  component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &error);
  g_assert_no_error (error);
  g_assert (CBY_IS_COMPONENT_INDEX (component_index));

  entry_point_index = cby_entry_point_index_new (component_index);
  g_assert (CBY_IS_ENTRY_POINT_INDEX (entry_point_index));

  service_manager = _cby_service_manager_new (session_bus, entry_point_index, &error);
  g_assert_no_error (error);
  service_created_signal_id =
      g_signal_connect (service_manager, "service-created", G_CALLBACK (inc_counter), &service_created);
  service_updated_signal_id =
      g_signal_connect (service_manager, "service-updated", G_CALLBACK (inc_counter), &service_updated);
  service_removed_signal_id =
      g_signal_connect (service_manager, "service-removed", G_CALLBACK (inc_counter), &service_removed);

  /* Install */

  /* Remove service files to make sure they get updated */
  /* Lets not assert here as the service files may not exist yet */
  tests_assert_syscall_0_or_enoent (unlink (main_entry_point_service_path));
  tests_assert_syscall_0_or_enoent (unlink (agent_entry_point_service_path));

  main_entry_point_service_contents =
      get_expected_service_contents (f, f->bundle.id, old_version->number,
                                     CANTERBURY_EXECUTABLE_TYPE_APPLICATION);
  agent_entry_point_service_contents =
      get_expected_service_contents (f, agent_entry_point_id,
                                     old_version->number,
                                     CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE);

  /* We always use the high-level API here since there's nothing we want
   * to do that can only be done in the low-level API */
  tests_app_bundle_install_bundle (&f->bundle, NULL, old_version);

  while (service_created < 2)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (service_created, ==, 2);
  g_assert_cmpuint (service_updated, ==, 0);
  g_assert_cmpuint (service_removed, ==, 0);

  /* Check if service files (main entry point + agent) were updated */
  tests_assert_syscall_0 (g_stat (main_entry_point_service_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
  check_file_contents (main_entry_point_service_path,
                       main_entry_point_service_contents);

  tests_assert_syscall_0 (g_stat (agent_entry_point_service_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
  check_file_contents (agent_entry_point_service_path,
                       agent_entry_point_service_contents);

  /* Upgrade */

  /* Reset vars */
  service_updated = 0;
  service_created = 0;
  service_removed = 0;

  /* Remove service files to make sure they get updated */
  tests_assert_syscall_0 (unlink (main_entry_point_service_path));
  tests_assert_syscall_0 (unlink (agent_entry_point_service_path));

  main_entry_point_service_contents_v2 =
      get_expected_service_contents (f, f->bundle.id, new_version->number,
                                     CANTERBURY_EXECUTABLE_TYPE_APPLICATION);

  tests_app_bundle_install_bundle (&f->bundle, old_version, new_version);

  while (service_updated < 1 || service_removed < 1)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (service_created, ==, 0);
  g_assert_cmpuint (service_updated, ==, 1);
  g_assert_cmpuint (service_removed, ==, 1);

  /* Check if service file for the main entry point was updated. */
  tests_assert_syscall_0 (g_stat (main_entry_point_service_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
  check_file_contents (main_entry_point_service_path,
                       main_entry_point_service_contents_v2);

  /* Check if the service file for the agent was removed */
  tests_assert_no_file (agent_entry_point_service_path);

  /* Roll back to old version */

  /* Reset vars */
  service_updated = 0;
  service_created = 0;
  service_removed = 0;

  /* Remove service files to make sure they get updated */
  tests_assert_syscall_0 (unlink (main_entry_point_service_path));
  /* agent service file wasn't created in last test, nothing to remove */

  if (use_low_level)
    {
      tests_app_bundle_roll_back (&f->bundle, TESTS_ROLLBACK_MODE_ROLL_BACK,
                                  new_version, old_version);

      while (service_created < 1 || service_updated < 1)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpuint (service_created, ==, 1);
      g_assert_cmpuint (service_updated, ==, 1);
      g_assert_cmpuint (service_removed, ==, 0);

      /* Check if service files (main entry point + agent) were updated */
      tests_assert_syscall_0 (g_stat (main_entry_point_service_path,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      check_file_contents (main_entry_point_service_path,
                           main_entry_point_service_contents);

      tests_assert_syscall_0 (g_stat (agent_entry_point_service_path,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      check_file_contents (agent_entry_point_service_path,
                           agent_entry_point_service_contents);

      /* Uninstall (temporary removal) */

      /* Reset vars */
      service_updated = 0;
      service_created = 0;
      service_removed = 0;

      /* Lets not remove service files to check if they get removed on
       * uninstall */

      tests_app_bundle_uninstall (&f->bundle, old_version);

      while (service_removed < 2)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpuint (service_created, ==, 0);
      g_assert_cmpuint (service_updated, ==, 0);
      g_assert_cmpuint (service_removed, ==, 2);

      /* Check if service files (main entry point + agent) were removed */
      tests_assert_no_file (main_entry_point_service_path);
      tests_assert_no_file (agent_entry_point_service_path);

      /* Reinstall (reverse uninstall) */

      /* service files weren't created in last test, nothing to remove */

      /* Reset vars */
      service_updated = 0;
      service_created = 0;
      service_removed = 0;

      tests_app_bundle_reinstall (&f->bundle, old_version);

      while (service_created < 2)
        g_main_context_iteration (NULL, TRUE);

      g_assert_cmpuint (service_created, ==, 2);
      g_assert_cmpuint (service_updated, ==, 0);
      g_assert_cmpuint (service_removed, ==, 0);

      /* Check if service files (main entry point + agent) were updated */
      tests_assert_syscall_0 (g_stat (main_entry_point_service_path,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      check_file_contents (main_entry_point_service_path,
                           main_entry_point_service_contents);

      tests_assert_syscall_0 (g_stat (agent_entry_point_service_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      check_file_contents (agent_entry_point_service_path,
                           agent_entry_point_service_contents);

      /* We have rolled back to version 1, which had 2 entry points */
      expect_services = 2;
    }
  else
    {
      /* We are still on version 2, which has 1 entry point */
      expect_services = 1;
    }

  /* Uninstall (removal) */

  /* Reset vars */
  service_updated = 0;
  service_created = 0;
  service_removed = 0;

  /* Lets not remove service files to check if they get removed on uninstall */

  tests_app_bundle_remove (&f->bundle);

  while (service_removed < expect_services)
    g_main_context_iteration (NULL, TRUE);

  g_assert_cmpuint (service_created, ==, 0);
  g_assert_cmpuint (service_updated, ==, 0);
  g_assert_cmpuint (service_removed, ==, expect_services);

  /* Check if service files (main entry point + agent) were removed */
  tests_assert_no_file (main_entry_point_service_path);
  tests_assert_no_file (agent_entry_point_service_path);

  g_signal_handler_disconnect (service_manager, service_created_signal_id);
  g_signal_handler_disconnect (service_manager, service_updated_signal_id);
  g_signal_handler_disconnect (service_manager, service_removed_signal_id);
}

static void
teardown (Fixture *f,
          gconstpointer user_data)
{
  const TestsTempDir *temp_dir = user_data;

  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->app_store_proxy);
  g_clear_object (&f->bundle_manager1_proxy);

  tests_temp_dir_teardown (temp_dir);
}

int
main (int argc,
    char **argv)
{
  TestsTempDir temp_dir;
  int exit_status;

  tests_temp_dir_init (&temp_dir);
  g_setenv ("XDG_RUNTIME_DIR", temp_dir.xdg_runtime_dir, TRUE);

  cby_init_environment ();

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/service-manager/services-availability", Fixture, &temp_dir, setup,
              test_services_availability, teardown);

  exit_status = g_test_run ();

  tests_temp_dir_cleanup (&temp_dir);

  return exit_status;
}
