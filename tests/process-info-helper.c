/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "canterbury/canterbury.h"

#include "tests/process-info-generated.h"

/*
 * process-info-helper — helper for process-info test case
 *
 * Usage:
 * process-info-helper-whatever <D-Bus unique name of process-info>
 *
 * This helper is linked multiple times (with exactly the same code),
 * resulting in different filenames which are linked to different profiles by
 * process-info's AppArmor profile. It calls a D-Bus method on the
 * process-info test, which allows that test to exercise extraction of
 * remote process information from a GDBusMethodInvocation.
 *
 * The helper also passes its own idea of its process info as arguments
 * to the D-Bus method, so that the process-info test can compare them.
 */

static const char *
not_null (const char *s)
{
  return (s == NULL ? "(null)" : s);
}

int
main (int argc,
    char **argv)
{
  GError *error = NULL;
  TestsProcessInfo *proxy;
  CbyProcessInfo *me;

  /* Call back to the test that ran us. */
  proxy = tests_process_info_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
      G_DBUS_PROXY_FLAGS_NONE,
      argv[1],
      "/org/apertis/CanterburyTests/ProcessInfo",
      NULL,
      &error);
  g_assert_no_error (error);

  me = cby_process_info_get_self ();
  tests_process_info_call_hello_sync (proxy,
      not_null (cby_process_info_get_apparmor_label (me)),
      not_null (cby_process_info_get_bundle_id (me)),
      cby_process_info_get_process_type (me),
      cby_process_info_get_user_id (me), NULL, &error);
  g_assert_no_error (error);

  g_object_unref (proxy);
  return 0;
}
