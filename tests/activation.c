/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016-2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <unistd.h>

#include <glib.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include <canterbury/gdbus/canterbury.h>

#include "src/util.h"
#include "tests/common.h"

#define BUNDLE_ID "org.apertis.CanterburyTests.Activatable"
#define MAIN_ENTRY_POINT_ID BUNDLE_ID
#define MAIN_OBJECT_PATH "/org/apertis/CanterburyTests/Activatable"
#define OTHER_ENTRY_POINT_ID BUNDLE_ID ".Other"
#define DIRECT_ENTRY_POINT_ID BUNDLE_ID ".TheHardWay"
#define DIRECT_OBJECT_PATH MAIN_OBJECT_PATH "/TheHardWay"
#define LOGGING_IFACE BUNDLE_ID ".Log"

typedef struct
{
  enum { EXECUTED, ACTIVATED, OPENED, NEW_APP_STATE } event;
  /* only for NEW_APP_STATE */
  CanterburyAppState app_state;
  gchar *entry_point_id;
  /* argv or files to open or whatever */
  gchar **parameters;
} Message;

static void
message_free (gpointer p)
{
  Message *m = p;

  g_free (m->entry_point_id);
  g_strfreev (m->parameters);
  g_free (m);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Message, message_free)

typedef struct
{
  GDBusConnection *session_bus;
  /* (element-type Message) */
  GQueue queue;
} Fixture;

typedef struct
{
  /* Not used in this test yet */
  int dummy;
} Config;

static void
setup (Fixture       *f,
       gconstpointer  user_data)
{
  /* be killed by SIGALRM if it takes unreasonably long */
  alarm (60);
}

static const gchar *
stringify_enum (GType type,
                gint numeric)
{
  GEnumClass *cls = g_type_class_ref (type);
  GEnumValue *value;
  const gchar *ret = "???";

  value = g_enum_get_value (cls, numeric);

  if (value != NULL)
    ret = value->value_nick;

  g_type_class_unref (cls);
  return ret;
}

static void
logging_cb (GDBusConnection *connection,
            const gchar *sender,
            const gchar *object_path,
            const gchar *iface,
            const gchar *signal,
            GVariant *parameters,
            gpointer user_data)
{
  g_autoptr (Message) m = g_new0 (Message, 1);
  Fixture *f = user_data;

  if (g_strcmp0 (signal, "ReceivedActivate") == 0)
    {
      m->event = ACTIVATED;
      /* Deliberately no type-checking here: this is just test code */
      g_variant_get (parameters, "(s)", &m->entry_point_id);
      g_test_message ("%s: %s received Activate({...})",
                      sender, m->entry_point_id);
    }
  else if (g_strcmp0 (signal, "ReceivedOpen") == 0)
    {
      m->event = OPENED;
      g_variant_get (parameters, "(s^as)", &m->entry_point_id, &m->parameters);
      g_test_message ("%s: %s received Open([%u URIs], {...})",
                      sender, m->entry_point_id, g_strv_length (m->parameters));
    }
  else if (g_strcmp0 (signal, "ReceivedCommandLine") == 0)
    {
      m->event = EXECUTED;
      g_variant_get (parameters, "(s^as)", &m->entry_point_id, &m->parameters);
      g_test_message ("%s: %s executed with %u argv entries",
                      sender, m->entry_point_id, g_strv_length (m->parameters));
    }
  else if (g_strcmp0 (signal, "ReceivedNewAppState") == 0)
    {
      m->event = NEW_APP_STATE;
      g_variant_get (parameters, "(su^as)", &m->entry_point_id, &m->app_state,
                     &m->parameters);
      g_test_message ("%s: %s received NewAppState(%u=%s, [%u arguments]",
                      sender, m->entry_point_id, m->app_state,
                      stringify_enum (CANTERBURY_TYPE_APP_STATE, m->app_state),
                      g_strv_length (m->parameters));
    }
  else
    {
      return;
    }

  g_queue_push_tail (&f->queue, g_steal_pointer (&m));
}

static void
restart_canterbury (void)
{
  g_autoptr (GError) error = NULL;
  g_autofree gchar *state = NULL;
  gint exit_status;
  /* We use systemctl because that's the easiest way for test code to
   * block on it. */
  const gchar *argv[] =
    {
      "systemctl",
      "--user",
      "daemon-reload",
      NULL,
      NULL
    };

  /* We can't restart Canterbury on target devices, because it would
   * reboot the system. Assume developer testing is mostly going to be
   * in the SDK environment. */
  if (_cby_is_target ())
    return;

  g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH,
                NULL, NULL, NULL, NULL, &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  argv[2] = "stop";
  argv[3] = "canterbury.service";
  g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH,
                NULL, NULL, NULL, NULL, &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);

  /* Stop last-user mode from interfering */
  state = g_build_filename (g_get_user_data_dir (), "canterbury", "state.ini",
                            NULL);
  tests_assert_syscall_0_or_enoent (g_unlink (state));

  argv[2] = "start";
  argv[3] = "canterbury.service";
  g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH,
                NULL, NULL, NULL, NULL, &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
kill_slice (void)
{
  g_autoptr (GError) error = NULL;
  gint exit_status;
  /* We use systemctl because that's the easiest way for test code to
   * block on it. */
  const gchar *argv[] =
    {
      "systemctl",
      "--user",
      "stop",
      BUNDLE_ID ".slice",
      NULL
    };

  g_spawn_sync (NULL, (gchar **) argv, NULL, G_SPAWN_SEARCH_PATH,
                NULL, NULL, NULL, NULL, &exit_status, &error);
  g_assert_no_error (error);
  g_spawn_check_exit_status (exit_status, &error);
  g_assert_no_error (error);
}

static void
test_activation (Fixture *f,
                 gconstpointer user_data)
{
  g_autoptr (CbyComponentIndex) components = NULL;
  g_autoptr (CbyEntryPointIndex) entry_points = NULL;
  g_autoptr (CbyEntryPoint) main_entry_point = NULL;
  g_autoptr (CbyEntryPoint) other_entry_point = NULL;
  g_autoptr (CbyEntryPoint) direct_entry_point = NULL;
  g_autoptr (GVariant) tuple = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (Message) m = NULL;
  guint logging_subscription;
  guint direct_logging_subscription;

  if (getuid () == 0)
    {
      g_test_skip ("This test uses the user's Canterbury process and cannot "
                   "be run as root");
      return;
    }

  restart_canterbury ();

  components = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_ONCE, &error);
  g_assert_no_error (error);
  g_assert_nonnull (components);

  entry_points = cby_entry_point_index_new (components);
  g_assert_no_error (error);
  g_assert_nonnull (entry_points);

  main_entry_point = cby_entry_point_index_get_by_id (entry_points,
                                                      MAIN_ENTRY_POINT_ID);
  g_assert_nonnull (main_entry_point);

  other_entry_point = cby_entry_point_index_get_by_id (entry_points,
                                                       OTHER_ENTRY_POINT_ID);
  g_assert_nonnull (other_entry_point);

  direct_entry_point = cby_entry_point_index_get_by_id (entry_points,
                                                        DIRECT_ENTRY_POINT_ID);
  g_assert_nonnull (direct_entry_point);

  f->session_bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &error);
  g_assert_no_error (error);
  g_assert_nonnull (f->session_bus);

  /* Preparation: make sure the process is not running. */
  kill_slice ();

  g_test_message ("Waiting for app-bundle to die");
  tests_wait_for_no_name_owner (f->session_bus, MAIN_ENTRY_POINT_ID, 10);

  /* Record what happens as we go */
  logging_subscription =
    g_dbus_connection_signal_subscribe (f->session_bus,
                                        MAIN_ENTRY_POINT_ID,
                                        LOGGING_IFACE,
                                        NULL,   /* member */
                                        MAIN_OBJECT_PATH,
                                        NULL,
                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                        logging_cb, f, NULL);
  direct_logging_subscription =
    g_dbus_connection_signal_subscribe (f->session_bus,
                                        DIRECT_ENTRY_POINT_ID,
                                        LOGGING_IFACE,
                                        NULL,   /* member */
                                        DIRECT_OBJECT_PATH,
                                        NULL,
                                        G_DBUS_SIGNAL_FLAGS_NONE,
                                        logging_cb, f, NULL);

  cby_entry_point_activate_async (main_entry_point, NULL, NULL,
                                  tests_store_result_cb, &result);

  g_test_message ("Waiting for first activation");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_activate_finish (main_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of first activation");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, ACTIVATED);
  g_assert_cmpstr (m->entry_point_id, ==, MAIN_ENTRY_POINT_ID);
  g_clear_pointer (&m, message_free);

  cby_entry_point_open_uri_async (main_entry_point, "http://example.com",
                                  NULL, NULL, tests_store_result_cb, &result);

  g_test_message ("Waiting for OpenURIs");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_open_uri_finish (main_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of OpenURIs");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  /* Deliberately not sorting the queue here - we know what order these
   * will appear in. */

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, OPENED);
  g_assert_cmpstr (m->entry_point_id, ==, MAIN_ENTRY_POINT_ID);
  g_assert_cmpstr (m->parameters[0], ==, "http://example.com");
  g_assert_cmpstr (m->parameters[1], ==, NULL);
  g_clear_pointer (&m, message_free);

  /* Activate the other "head". */
  cby_entry_point_activate_async (other_entry_point, NULL, NULL,
                                  tests_store_result_cb, &result);

  g_test_message ("Waiting for activation of other entry point");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_activate_finish (other_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of activation of other entry point");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, ACTIVATED);
  g_assert_cmpstr (m->entry_point_id, ==, OTHER_ENTRY_POINT_ID);
  g_clear_pointer (&m, message_free);

  /* Launch a separate implementation. This one implements
   * org.freedesktop.Application from first principles, without using
   * GApplication. */
  cby_entry_point_open_uri_async (direct_entry_point, "about:blank", NULL,
                                  NULL, tests_store_result_cb, &result);

  g_test_message ("Waiting for activation of different process");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_open_uri_finish (direct_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of activation of different process");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, OPENED);
  g_assert_cmpstr (m->entry_point_id, ==, DIRECT_ENTRY_POINT_ID);
  g_assert_cmpstr (m->parameters[0], ==, "about:blank");
  g_assert_cmpstr (m->parameters[1], ==, NULL);
  g_clear_pointer (&m, message_free);

  /* Bring the first one back to the foreground. */
  cby_entry_point_activate_async (main_entry_point, NULL, NULL,
                                  tests_store_result_cb, &result);

  g_test_message ("Waiting for re-activation of first process");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_activate_finish (main_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of re-activation of first process");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, ACTIVATED);
  g_assert_cmpstr (m->entry_point_id, ==, MAIN_ENTRY_POINT_ID);
  g_clear_pointer (&m, message_free);

  /* Activate the other implementation. This means we have exercised
   * both open-before-activate and activate-before-open. */
  cby_entry_point_activate_async (direct_entry_point, NULL, NULL,
                                  tests_store_result_cb, &result);

  g_test_message ("Waiting for re-activation of different process");

  while (result == NULL)
    g_main_context_iteration (NULL, TRUE);

  cby_entry_point_activate_finish (direct_entry_point, result, &error);
  g_assert_no_error (error);
  g_clear_object (&result);

  g_test_message ("Waiting for effects of re-activation of different process");

  while (f->queue.length < 1)
    g_main_context_iteration (NULL, TRUE);

  m = g_queue_pop_head (&f->queue);
  g_assert_cmpint (m->event, ==, ACTIVATED);
  g_assert_cmpstr (m->entry_point_id, ==, DIRECT_ENTRY_POINT_ID);
  g_clear_pointer (&m, message_free);

  /* Kill the whole bundle (abruptly, for now) */
  kill_slice ();
  g_test_message ("Waiting for app-bundle to die");
  tests_wait_for_no_name_owner (f->session_bus, MAIN_ENTRY_POINT_ID, 10);
  tests_wait_for_no_name_owner (f->session_bus, DIRECT_ENTRY_POINT_ID, 10);

  g_dbus_connection_signal_unsubscribe (f->session_bus, logging_subscription);
  g_dbus_connection_signal_unsubscribe (f->session_bus,
                                        direct_logging_subscription);
}

static void
teardown (Fixture       *f,
          gconstpointer  user_data)
{
  g_clear_object (&f->session_bus);

  /* cancel watchdog */
  alarm (0);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/activation", Fixture, NULL, setup, test_activation, teardown);

  return g_test_run ();
}
