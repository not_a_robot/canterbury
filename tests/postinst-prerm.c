/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e-s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <utime.h>

#include <glib.h>
#include <glib/gstdio.h>

#ifdef TEST_FULL_VERSION
# include <gtk/gtk.h>
#endif

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>
#include "canterbury/platform/component-index-internal.h"
#include "canterbury/platform/environment.h"

#include "src/apparmor.h"
#include "tests/common.h"

/* App-bundle to use for testing */
#define TEST_BUNDLE_ID "org.apertis.CanterburyTests.PostinstPrerm"

/* Minimum size in bytes where we believe the ubercache might be valid */
#define MINIMUM_REASONABLE_UBERCACHE_SIZE 100

typedef struct
{
  TestsAppBundle bundle;
  GDBusProxy *app_store_proxy;
  GDBusProxy *bundle_manager1_proxy;
  guint ordinary_uid;
  guint ordinary_gid;
  guint unrelated_uid;
  guint unrelated_gid;
} Fixture;

static const gchar * const version_numbers[] = { "1.0", "2.0" };
/*
 * Check https://appdev.apertis.org/documentation/bundle-spec.html#icon-for-the-bundle
 * for more info on icons for app-bundles.
 */
static guint supported_icon_sizes[] = { 8, 16, 22, 24, 32, 36, 42, 48, 64, 72, 96, 128, 192, 256, 512 };

#define DEFAULT_ICON_THEME_NAME "hicolor"
#define ALTERNATIVE_ICON_THEME_NAME "com.example.Metallic"

/*
 * Common setup for all tests.
 */
static void
setup (Fixture *f,
       gconstpointer user_data)
{
  g_autofree gchar *ribchester_full = g_find_program_in_path ("ribchester");
  const gchar *api = user_data;
  gboolean use_low_level = (g_strcmp0 (api, "low-level") == 0);

  if (use_low_level && ribchester_full == NULL)
    {
      g_test_skip ("This test is only applicable to ribchester-full");
      return;
    }

  if (!tests_require_ordinary_uid (&f->ordinary_uid, &f->ordinary_gid,
                                   &f->unrelated_uid, &f->unrelated_gid))
    return;

  if (!tests_require_ribchester (NULL, &f->app_store_proxy,
                                 &f->bundle_manager1_proxy))
    return;

  if (!tests_app_bundle_set_up (&f->bundle, f->app_store_proxy,
                                f->bundle_manager1_proxy, TEST_BUNDLE_ID,
                                f->ordinary_uid, version_numbers,
                                G_N_ELEMENTS (version_numbers)))
    return;
}

static gchar *
get_entry_point_path (const gchar *bundle_id,
                      const gchar *entry_point_id)
{
  return g_strdup_printf ("%s/%s/share/applications/%s.desktop",
                          CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id, entry_point_id);
}

static gchar *
get_entry_point_link_path (const gchar *entry_point_id)
{
  return g_strdup_printf ("%s/applications/%s.desktop",
                          CBY_PATH_SYSTEM_EXTENSIONS, entry_point_id);
}

static gchar *
get_entry_point_mime_type (const gchar *entry_point_id)
{
  return g_strdup_printf ("application/vnd.example.%s", entry_point_id);
}

static gchar *
get_icon_path (const gchar *bundle_id,
               const gchar *entry_point_id,
               const gchar *icon_theme_name,
               guint size)
{
  return g_strdup_printf ("%s/%s/share/icons/%s/%dx%d/apps/%s.png",
                          CBY_PATH_PREFIX_STORE_BUNDLE, bundle_id, icon_theme_name,
                          size, size, entry_point_id);
}

static gchar *
get_icon_link_path (const gchar *entry_point_id,
                    const gchar *icon_theme_name,
                    guint size)
{
  return g_strdup_printf ("%s/icons/%s/%dx%d/apps/%s.png",
                          CBY_PATH_SYSTEM_EXTENSIONS, icon_theme_name,
                          size, size, entry_point_id);
}

static void
install_entry_point (Fixture *f,
                     const gchar *prefix,
                     const gchar *entry_point_id,
                     const gchar *mimetype)
{
  g_autoptr (GError) error = NULL;

  g_mkdir_with_parents (tests_printf ("%s/share/applications", prefix), 0755);
  g_file_set_contents (tests_printf ("%s/share/applications/%s.desktop",
                                     prefix, entry_point_id),
                       tests_printf ("[Desktop Entry]\n"
                                     "Name=Test bundle\n"
                                     "Exec=canterbury-exec %s/%s/bin/%s\n"
                                     "MimeType=%s;\n"
                                     "Type=Application\n",
                                     CBY_PATH_PREFIX_STORE_BUNDLE,
                                     f->bundle.id,
                                     entry_point_id,
                                     mimetype),
                       -1, &error);
  g_assert_no_error (error);
}

static void
install_icons (Fixture *f,
               const gchar *prefix,
               const gchar *entry_point_id,
               const gchar *icon_theme_name)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GFile) src_file = NULL;
  const gchar *src;

  src = g_test_get_filename (G_TEST_DIST, "tests", "usr", "share", "icons",
                             "example.png", NULL);
  src_file = g_file_new_for_path (src);

  for (guint i = 0; i < G_N_ELEMENTS (supported_icon_sizes); ++i)
    {
      guint size = supported_icon_sizes[i];
      g_autofree gchar *destdir;
      g_autofree gchar *dest;
      g_autoptr (GFile) dest_file = NULL;

      destdir = g_strdup_printf ("%s/share/icons/%s/%dx%d/apps",
                                 prefix, icon_theme_name, size, size);
      g_mkdir_with_parents (destdir, 0755);

      dest = g_strdup_printf ("%s/%s.png", destdir, entry_point_id);
      dest_file = g_file_new_for_path (dest);

      g_test_message ("%s: dest: %s; copying icon from: %s/%s",
                      G_STRFUNC, dest, g_test_get_dir (G_TEST_DIST), src);

      g_file_copy (src_file, dest_file, G_FILE_COPY_OVERWRITE,
                   NULL, NULL, NULL, &error);
      g_assert_no_error (error);
    }
}

static void
install_bundle_data (Fixture *f,
                     const gchar *prefix,
                     const gchar *entry_point_id,
                     const gchar *mimetype)
{
  install_entry_point (f, prefix, entry_point_id, mimetype);
  install_icons (f, prefix, entry_point_id, DEFAULT_ICON_THEME_NAME);
  install_icons (f, prefix, entry_point_id, ALTERNATIVE_ICON_THEME_NAME);
}

#ifdef TEST_FULL_VERSION
static void
test_mime_registered (const gchar *app_id,
                      const gchar *expected_mime_type,
                      gboolean should_be_available)
{
  GList *registered_apps = NULL;
  const GList *iter;
  g_autofree gchar *expected_id = NULL;
  gboolean app_found = FALSE;

  expected_id = g_strdup_printf ("%s.desktop", app_id);

  g_test_message ("Checking if \"%s\" is registered: expected %d", app_id, should_be_available);
  registered_apps = g_app_info_get_all ();
  app_found = FALSE;
  for (iter = registered_apps; iter != NULL; iter = iter->next)
    {
      if (g_strcmp0 (expected_id, g_app_info_get_id (iter->data)) == 0)
        {
          app_found = TRUE;
          break;
        }
    }
  g_assert_cmpint (app_found, ==, should_be_available);
  g_list_free_full (registered_apps, g_object_unref);

  g_test_message ("Checking if mimetype \"%s\" can be handled by \"%s\": expected %d",
                  expected_mime_type, app_id, should_be_available);
  registered_apps = g_app_info_get_all_for_type (expected_mime_type);
  app_found = FALSE;
  for (iter = registered_apps; iter != NULL; iter = iter->next)
    {
      if (g_strcmp0 (expected_id, g_app_info_get_id (iter->data)) == 0)
        {
          app_found = TRUE;
          break;
        }
    }
  g_assert_cmpint (app_found, ==, should_be_available);
  g_list_free_full (registered_apps, g_object_unref);
}

typedef enum
{
  TEST_ICON_DEFAULT    = 1 << 0,
  TEST_ICON_CHECK_SIZE = 1 << 1
} TestIconFlags;

static void
test_icon_registered (const gchar *app_id,
                      const gchar *icon_theme_name,
                      TestIconFlags flags,
                      gboolean should_be_available)
{
  g_autoptr (GtkIconTheme) icon_theme = NULL;

  icon_theme = gtk_icon_theme_new ();
  gtk_icon_theme_set_custom_theme (icon_theme, icon_theme_name);

  if (flags & TEST_ICON_DEFAULT)
    {
      g_test_message ("Checking if icon \"%s\" is available on \"%s\" icon theme: expected %d",
                      app_id, icon_theme_name, should_be_available);
      g_assert_cmpint (gtk_icon_theme_has_icon (icon_theme, app_id), ==,
                       should_be_available);
    }

  if (flags & TEST_ICON_CHECK_SIZE)
    {
      for (guint i = 0; i < G_N_ELEMENTS (supported_icon_sizes); ++i)
        {
          guint size = supported_icon_sizes[i];
          g_autoptr (GtkIconInfo) icon_info;

          g_test_message ("Checking if icon \"%s\" for size %d is available on \"%s\" icon theme: expected %d",
                          app_id, size, icon_theme_name, should_be_available);
          icon_info = gtk_icon_theme_lookup_icon (icon_theme, app_id, size, 0);
          if (should_be_available)
            g_assert_nonnull (icon_info);
          else
            g_assert_null (icon_info);
        }
    }
}

static void
test_app_registered (const gchar *app_id,
                     const gchar *expected_mime_type,
                     gboolean should_be_available)
{
  test_mime_registered (app_id, expected_mime_type, should_be_available);
  test_icon_registered (app_id, DEFAULT_ICON_THEME_NAME,
                        TEST_ICON_DEFAULT | TEST_ICON_CHECK_SIZE,
                        should_be_available);
  test_icon_registered (app_id, ALTERNATIVE_ICON_THEME_NAME,
                        TEST_ICON_DEFAULT | TEST_ICON_CHECK_SIZE,
                        should_be_available);
}

static void
test_system_app_registered (void)
{
  if (!g_file_test ("/usr/share/applications/evince.desktop", G_FILE_TEST_EXISTS))
    g_test_message ("Skipping test for system apps on desktop MIME database cache - requires Evince to be installed");
  else
    {
      test_mime_registered ("evince", "application/pdf", TRUE);
      test_icon_registered ("evince", DEFAULT_ICON_THEME_NAME, TEST_ICON_DEFAULT, TRUE);
    }
}
#endif

static void
test_install (Fixture *f,
              gconstpointer user_data)
{
  g_autofree gchar *main_entry_point_path = NULL;
  g_autofree gchar *main_entry_point_link_path = NULL;
  g_autofree gchar *main_entry_point_mime_type = NULL;
  g_autofree gchar *main_default_icon_path = NULL;
  g_autofree gchar *main_default_icon_link_path = NULL;
  g_autofree gchar *main_alternative_icon_path = NULL;
  g_autofree gchar *main_alternative_icon_link_path = NULL;
  g_autofree gchar *entry_point_v1_id = NULL;
  g_autofree gchar *entry_point_v1_path = NULL;
  g_autofree gchar *entry_point_v1_link_path = NULL;
  g_autofree gchar *entry_point_v1_mime_type = NULL;
  g_autofree gchar *icon_default_v1_path = NULL;
  g_autofree gchar *icon_default_v1_link_path = NULL;
  g_autofree gchar *icon_alternative_v1_path = NULL;
  g_autofree gchar *icon_alternative_v1_link_path = NULL;
  g_autofree gchar *entry_point_v2_id = NULL;
  g_autofree gchar *entry_point_v2_path = NULL;
  g_autofree gchar *entry_point_v2_link_path = NULL;
  g_autofree gchar *entry_point_v2_mime_type = NULL;
  g_autofree gchar *icon_default_v2_path = NULL;
  g_autofree gchar *icon_default_v2_link_path = NULL;
  g_autofree gchar *icon_alternative_v2_path = NULL;
  g_autofree gchar *icon_alternative_v2_link_path = NULL;
  g_autofree gchar *stamp = NULL;
  g_autofree gchar *stamp_contents = NULL;
  g_autoptr (GError) error = NULL;
  GStatBuf stat_buf;
  struct timeval then;
  struct timeval times[2] = { { 1, 2 }, { 1, 2 } };
  const TestsVersion *old_version = &f->bundle.versions[0];
  const TestsVersion *new_version = &f->bundle.versions[1];
  const gchar *mime_cache_path;
  const gchar *default_icon_cache_path;
  const gchar *alternative_icon_cache_path;
  const gchar *api = user_data;
  gboolean use_low_level = (g_strcmp0 (api, "low-level") == 0);

  tests_assert_syscall_0 (gettimeofday (&then, NULL));

  if (!tests_require_running_as_root ())
    return;

  if (g_test_failed ())
    return;

  /* Install */

  /* Setup entry points helper variables */
  entry_point_v1_id = g_strdup_printf ("%s.v1", f->bundle.id);
  entry_point_v2_id = g_strdup_printf ("%s.v2", f->bundle.id);

  main_entry_point_path = get_entry_point_path (f->bundle.id, f->bundle.id);
  entry_point_v1_path = get_entry_point_path (f->bundle.id, entry_point_v1_id);
  entry_point_v2_path = get_entry_point_path (f->bundle.id, entry_point_v2_id);

  main_entry_point_link_path = get_entry_point_link_path (f->bundle.id);
  entry_point_v1_link_path = get_entry_point_link_path (entry_point_v1_id);
  entry_point_v2_link_path = get_entry_point_link_path (entry_point_v2_id);

  main_entry_point_mime_type = get_entry_point_mime_type (f->bundle.id);
  entry_point_v1_mime_type = get_entry_point_mime_type (entry_point_v1_id);
  entry_point_v2_mime_type = get_entry_point_mime_type (entry_point_v2_id);

  /* We will only be checking the icons/symlinks for the 64x64 size for the
   * default theme and 8x8 for the alternative theme, to avoid having to
   * loop through all supported sizes all the time.
   * This should be a good compromise.
   */
  main_default_icon_path = get_icon_path (f->bundle.id, f->bundle.id, DEFAULT_ICON_THEME_NAME, 64);
  icon_default_v1_path = get_icon_path (f->bundle.id, entry_point_v1_id, DEFAULT_ICON_THEME_NAME, 64);
  icon_default_v2_path = get_icon_path (f->bundle.id, entry_point_v2_id, DEFAULT_ICON_THEME_NAME, 64);

  main_default_icon_link_path = get_icon_link_path (f->bundle.id, DEFAULT_ICON_THEME_NAME, 64);
  icon_default_v1_link_path = get_icon_link_path (entry_point_v1_id, DEFAULT_ICON_THEME_NAME, 64);
  icon_default_v2_link_path = get_icon_link_path (entry_point_v2_id, DEFAULT_ICON_THEME_NAME, 64);

  main_alternative_icon_path = get_icon_path (f->bundle.id, f->bundle.id, ALTERNATIVE_ICON_THEME_NAME, 8);
  icon_alternative_v1_path = get_icon_path (f->bundle.id, entry_point_v1_id, ALTERNATIVE_ICON_THEME_NAME, 8);
  icon_alternative_v2_path = get_icon_path (f->bundle.id, entry_point_v2_id, ALTERNATIVE_ICON_THEME_NAME, 8);

  main_alternative_icon_link_path = get_icon_link_path (f->bundle.id, ALTERNATIVE_ICON_THEME_NAME, 8);
  icon_alternative_v1_link_path = get_icon_link_path (entry_point_v1_id, ALTERNATIVE_ICON_THEME_NAME, 8);
  icon_alternative_v2_link_path = get_icon_link_path (entry_point_v2_id, ALTERNATIVE_ICON_THEME_NAME, 8);

  mime_cache_path = CBY_PATH_SYSTEM_EXTENSIONS "/applications/mimeinfo.cache";
  default_icon_cache_path = CBY_PATH_SYSTEM_EXTENSIONS "/icons/" DEFAULT_ICON_THEME_NAME "/icon-theme.cache";
  alternative_icon_cache_path = CBY_PATH_SYSTEM_EXTENSIONS "/icons/" ALTERNATIVE_ICON_THEME_NAME "/icon-theme.cache";

  /* Make the file appear to be very old, so it's obvious when it has
   * changed */
  tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE, times));
  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

  stamp = g_strdup_printf ("%s/%s.stamp", PKGCACHEDIR, f->bundle.id);
  tests_assert_no_file (stamp);

  /* Remove current MIME and icon cache databases to make sure they get updated */
  /* Lets not assert here as the caches may not exist yet */
  tests_assert_syscall_0_or_enoent (unlink (mime_cache_path));
  tests_assert_syscall_0_or_enoent (unlink (default_icon_cache_path));
  tests_assert_syscall_0_or_enoent (unlink (alternative_icon_cache_path));

  /* We always use the high-level API here since there's nothing we want
   * to do that can only be done in the low-level API */
  tests_app_bundle_install_bundle (&f->bundle, NULL, old_version);

  /* We can't assert that the time is strictly newer than then.tv_sec
   * (unless we sleep for at least 1 second, which we don't really
   * want to do, or make assumptions about the resolution of timestamps
   * on the filesystem that holds CBY_APPSTREAM_CACHE_INSTALLED_STORE, which
   * we don't really want to do either) but we can assert that it is at least
   * then.tv_sec. */
  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_file_get_contents (stamp, &stamp_contents, NULL, &error);
  g_assert_no_error (error);
  g_assert_cmpstr (stamp_contents, ==, old_version->number);

#ifdef TEST_FULL_VERSION
  /* Check if MIME cache database was updated. We don't care whether it's a
   * symlink to a regular file, or a regular file. The non-GUI build omits
   * this check because it doesn't guarantee to install desktop-file-utils. */
  tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

  /* Check if icon cache database was updated. The non-GUI build omits
   * this check because it doesn't guarantee to install
   * gtk-update-icon-theme. */
  tests_assert_syscall_0 (g_stat (default_icon_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
  tests_assert_syscall_0 (g_stat (alternative_icon_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

  test_app_registered (f->bundle.id, main_entry_point_mime_type, TRUE);
  test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, TRUE);
  test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, FALSE);
  test_system_app_registered ();
#endif

  /* Check if entry points/icons symlinks (main+v1) are properly created.
   * At the moment we still symlink icons into place in the non-GUI build,
   * because that's "cheap": we just don't update the icon cache. */
  tests_assert_type_nofollow (main_entry_point_path, S_IFREG);
  tests_assert_symlink_to (main_entry_point_link_path, main_entry_point_path);
  tests_assert_type_nofollow (entry_point_v1_path, S_IFREG);
  tests_assert_symlink_to (entry_point_v1_link_path, entry_point_v1_path);
  tests_assert_no_file (entry_point_v2_path);
  tests_assert_no_file (entry_point_v2_link_path);

  tests_assert_type_nofollow (main_default_icon_path, S_IFREG);
  tests_assert_symlink_to (main_default_icon_link_path, main_default_icon_path);
  tests_assert_type_nofollow (icon_default_v1_path, S_IFREG);
  tests_assert_symlink_to (icon_default_v1_link_path, icon_default_v1_path);
  tests_assert_no_file (icon_default_v2_path);
  tests_assert_no_file (icon_default_v2_link_path);

  tests_assert_type_nofollow (main_alternative_icon_path, S_IFREG);
  tests_assert_symlink_to (main_alternative_icon_link_path, main_alternative_icon_path);
  tests_assert_type_nofollow (icon_alternative_v1_path, S_IFREG);
  tests_assert_symlink_to (icon_alternative_v1_link_path, icon_alternative_v1_path);
  tests_assert_no_file (icon_alternative_v2_path);
  tests_assert_no_file (icon_alternative_v2_link_path);

  /* Upgrade */

  tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE, times));
  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
  /* Remove current MIME cache database to make sure it gets updated */
  tests_assert_syscall_0 (unlink (mime_cache_path));
  tests_assert_syscall_0 (unlink (default_icon_cache_path));
  tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

  if (g_strcmp0 (api, "low-level") == 0)
    {
      g_autofree gchar *unpack_to =
          tests_app_bundle_begin_install (&f->bundle, old_version, new_version);

      /* Install entry points (main+v2) */
      install_bundle_data (f, unpack_to, f->bundle.id,
                           main_entry_point_mime_type);
      install_bundle_data (f, unpack_to, entry_point_v2_id,
                           entry_point_v2_mime_type);

      /* We can't check that this file doesn't exist when using the
       * higher-level API, which is why this code path is still here */
      tests_assert_no_file (stamp);

      tests_app_bundle_commit_install (&f->bundle, old_version, new_version);
    }
  else
    {
      tests_app_bundle_install_bundle (&f->bundle, old_version, new_version);
    }

  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_file_get_contents (stamp, &stamp_contents, NULL, &error);
  g_assert_no_error (error);
  g_assert_cmpstr (stamp_contents, ==, new_version->number);

#ifdef TEST_FULL_VERSION
  /* Check if MIME cache database was updated */
  tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

  /* Check if icon cache database was updated */
  tests_assert_syscall_0 (g_stat (default_icon_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
  tests_assert_syscall_0 (g_stat (alternative_icon_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

  test_app_registered (f->bundle.id, main_entry_point_mime_type, TRUE);
  test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, FALSE);
  test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, TRUE);
  test_system_app_registered ();
#endif

  /* Check if entry points/icons symlinks (main+v2) are properly created and
   * entry point/icons symlinks for v1 are removed */
  tests_assert_type_nofollow (main_entry_point_path, S_IFREG);
  tests_assert_symlink_to (main_entry_point_link_path, main_entry_point_path);
  tests_assert_no_file (entry_point_v1_path);
  tests_assert_no_file (entry_point_v1_link_path);
  tests_assert_type_nofollow (entry_point_v2_path, S_IFREG);
  tests_assert_symlink_to (entry_point_v2_link_path, entry_point_v2_path);

  tests_assert_type_nofollow (main_default_icon_path, S_IFREG);
  tests_assert_symlink_to (main_default_icon_link_path, main_default_icon_path);
  tests_assert_no_file (icon_default_v1_path);
  tests_assert_no_file (icon_default_v1_link_path);
  tests_assert_type_nofollow (icon_default_v2_path, S_IFREG);
  tests_assert_symlink_to (icon_default_v2_link_path, icon_default_v2_path);

  tests_assert_type_nofollow (main_alternative_icon_path, S_IFREG);
  tests_assert_symlink_to (main_alternative_icon_link_path, main_alternative_icon_path);
  tests_assert_no_file (icon_alternative_v1_path);
  tests_assert_no_file (icon_alternative_v1_link_path);
  tests_assert_type_nofollow (icon_alternative_v2_path, S_IFREG);
  tests_assert_symlink_to (icon_alternative_v2_link_path, icon_alternative_v2_path);

  if (use_low_level)
    {
      g_autofree gchar *unpack_to;

      /* Roll back to old version */

      tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      times));
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
      /* Remove current MIME cache database to make sure it gets updated */
      tests_assert_syscall_0 (unlink (mime_cache_path));
      tests_assert_syscall_0 (unlink (default_icon_cache_path));
      tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

      tests_app_bundle_roll_back (&f->bundle, TESTS_ROLLBACK_MODE_ROLL_BACK,
                                  new_version, old_version);

      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_file_get_contents (stamp, &stamp_contents, NULL, &error);
      g_assert_no_error (error);
      g_assert_cmpstr (stamp_contents, ==, old_version->number);

#ifdef TEST_FULL_VERSION
      /* Check if MIME cache database was updated */
      tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      /* Check if icon cache database was updated */
      tests_assert_syscall_0 (g_stat (default_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      tests_assert_syscall_0 (g_stat (alternative_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      test_app_registered (f->bundle.id, main_entry_point_mime_type, TRUE);
      test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, TRUE);
      test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, FALSE);
      test_system_app_registered ();
#endif

      /* Check if entry points/icons symlinks (main+v1) are properly created
       * and entry point/icons symlinks for v2 are removed */
      tests_assert_type_nofollow (main_entry_point_path, S_IFREG);
      tests_assert_symlink_to (main_entry_point_link_path,
                               main_entry_point_path);
      tests_assert_type_nofollow (entry_point_v1_path, S_IFREG);
      tests_assert_symlink_to (entry_point_v1_link_path, entry_point_v1_path);
      tests_assert_no_file (entry_point_v2_path);
      tests_assert_no_file (entry_point_v2_link_path);

      tests_assert_type_nofollow (main_default_icon_path, S_IFREG);
      tests_assert_symlink_to (main_default_icon_link_path,
                               main_default_icon_path);
      tests_assert_type_nofollow (icon_default_v1_path, S_IFREG);
      tests_assert_symlink_to (icon_default_v1_link_path, icon_default_v1_path);
      tests_assert_no_file (icon_default_v2_path);
      tests_assert_no_file (icon_default_v2_link_path);

      tests_assert_type_nofollow (main_alternative_icon_path, S_IFREG);
      tests_assert_symlink_to (main_alternative_icon_link_path,
                               main_alternative_icon_path);
      tests_assert_type_nofollow (icon_alternative_v1_path, S_IFREG);
      tests_assert_symlink_to (icon_alternative_v1_link_path,
                               icon_alternative_v1_path);
      tests_assert_no_file (icon_alternative_v2_path);
      tests_assert_no_file (icon_alternative_v2_link_path);

      /* Uninstall (temporary removal) */

      tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      times));
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
      /* Remove current MIME cache database to make sure it gets updated */
      tests_assert_syscall_0 (unlink (mime_cache_path));
      tests_assert_syscall_0 (unlink (default_icon_cache_path));
      tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

      tests_app_bundle_uninstall (&f->bundle, old_version);

      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      tests_assert_no_file (stamp);

#ifdef TEST_FULL_VERSION
      /* Check if MIME cache database was updated */
      tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
#endif

      /* gtk-update-icon-cache does not create icon-theme.cache if there
       * no icon available on the search path, so as we removed the app,
       * there should be no icon or icon cache, unless some other app
       * with icons was installed using the same icon theme */
      if (g_stat (default_icon_cache_path, &stat_buf) == 0)
        {
          /* We probably have some other app icon on the search path */

          /* Check if icon cache database was updated */
          g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
          g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
        }
      else if (errno == ENOENT)
        {
          /* Do nothing - we probably don't have any icon on the search path */
        }
      else
        {
          g_error ("g_stat (\"%s\"): %s", default_icon_cache_path,
                   g_strerror (errno));
        }

      if (g_stat (alternative_icon_cache_path, &stat_buf) == 0)
        {
          /* We probably have some other app icon on the search path */

          /* Check if icon cache database was updated */
          g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
          g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
        }
      else if (errno == ENOENT)
        {
          /* Do nothing - we probably don't have any icon on the search path */
        }
      else
        {
          g_error ("g_stat (\"%s\"): %s", alternative_icon_cache_path, g_strerror (errno));
        }

#ifdef TEST_FULL_VERSION
      test_app_registered (f->bundle.id, main_entry_point_mime_type, FALSE);
      test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, FALSE);
      test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, FALSE);
      test_system_app_registered ();
#endif

      /* Check if entry points/icons (main+v1+v2) and the corresponding
       * symlinks are properly removed */
      tests_assert_no_file (main_entry_point_path);
      tests_assert_no_file (main_entry_point_link_path);
      tests_assert_no_file (entry_point_v1_path);
      tests_assert_no_file (entry_point_v1_link_path);
      tests_assert_no_file (entry_point_v2_path);
      tests_assert_no_file (entry_point_v2_link_path);

      tests_assert_no_file (main_default_icon_path);
      tests_assert_no_file (main_default_icon_link_path);
      tests_assert_no_file (icon_default_v1_path);
      tests_assert_no_file (icon_default_v1_link_path);
      tests_assert_no_file (icon_default_v2_path);
      tests_assert_no_file (icon_default_v2_link_path);

      tests_assert_no_file (main_alternative_icon_path);
      tests_assert_no_file (main_alternative_icon_link_path);
      tests_assert_no_file (icon_alternative_v1_path);
      tests_assert_no_file (icon_alternative_v1_link_path);
      tests_assert_no_file (icon_alternative_v2_path);
      tests_assert_no_file (icon_alternative_v2_link_path);

      /* Reinstall (reverse uninstall) */

      tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      times));
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
      /* Remove current MIME cache database to make sure it gets updated */
      tests_assert_syscall_0 (unlink (mime_cache_path));
      if (g_file_test (default_icon_cache_path, G_FILE_TEST_EXISTS))
        tests_assert_syscall_0 (unlink (default_icon_cache_path));
      if (g_file_test (alternative_icon_cache_path, G_FILE_TEST_EXISTS))
        tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

      tests_app_bundle_reinstall (&f->bundle, old_version);

      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_file_get_contents (stamp, &stamp_contents, NULL, &error);
      g_assert_no_error (error);
      g_assert_cmpstr (stamp_contents, ==, old_version->number);

#ifdef TEST_FULL_VERSION
      /* Check if MIME cache database was updated */
      tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      /* Check if icon cache database was updated */
      tests_assert_syscall_0 (g_stat (default_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      tests_assert_syscall_0 (g_stat (alternative_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      test_app_registered (f->bundle.id, main_entry_point_mime_type, TRUE);
      test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, TRUE);
      test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, FALSE);
      test_system_app_registered ();
#endif

      /* Check if entry points/icons symlinks (main+v1) are properly created */
      tests_assert_type_nofollow (main_entry_point_path, S_IFREG);
      tests_assert_symlink_to (main_entry_point_link_path,
                               main_entry_point_path);
      tests_assert_type_nofollow (entry_point_v1_path, S_IFREG);
      tests_assert_symlink_to (entry_point_v1_link_path, entry_point_v1_path);
      tests_assert_no_file (entry_point_v2_path);
      tests_assert_no_file (entry_point_v2_link_path);

      tests_assert_type_nofollow (main_default_icon_path, S_IFREG);
      tests_assert_symlink_to (main_default_icon_link_path,
                               main_default_icon_path);
      tests_assert_type_nofollow (icon_default_v1_path, S_IFREG);
      tests_assert_symlink_to (icon_default_v1_link_path, icon_default_v1_path);
      tests_assert_no_file (icon_default_v2_path);
      tests_assert_no_file (icon_default_v2_link_path);

      tests_assert_type_nofollow (main_alternative_icon_path, S_IFREG);
      tests_assert_symlink_to (main_alternative_icon_link_path,
                               main_alternative_icon_path);
      tests_assert_type_nofollow (icon_alternative_v1_path, S_IFREG);
      tests_assert_symlink_to (icon_alternative_v1_link_path,
                               icon_alternative_v1_path);
      tests_assert_no_file (icon_alternative_v2_path);
      tests_assert_no_file (icon_alternative_v2_link_path);

      /* Upgrade again */

      tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      times));
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
      /* Remove current MIME cache database to make sure it gets updated */
      tests_assert_syscall_0 (unlink (mime_cache_path));
      tests_assert_syscall_0 (unlink (default_icon_cache_path));
      tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

      unpack_to = tests_app_bundle_begin_install (&f->bundle, old_version,
                                                  new_version);

      /* Install entry points (main+v2) */
      install_bundle_data (f, unpack_to, f->bundle.id,
                           main_entry_point_mime_type);
      install_bundle_data (f, unpack_to, entry_point_v2_id,
                           entry_point_v2_mime_type);

      tests_assert_no_file (stamp);

      tests_app_bundle_commit_install (&f->bundle, old_version, new_version);

      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_file_get_contents (stamp, &stamp_contents, NULL, &error);
      g_assert_no_error (error);
      g_assert_cmpstr (stamp_contents, ==, new_version->number);

#ifdef TEST_FULL_VERSION
      /* Check if MIME cache database was updated */
      tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      /* Check if icon cache database was updated */
      tests_assert_syscall_0 (g_stat (default_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
      tests_assert_syscall_0 (g_stat (alternative_icon_cache_path, &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);

      test_app_registered (f->bundle.id, main_entry_point_mime_type, TRUE);
      test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, FALSE);
      test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, TRUE);
      test_system_app_registered ();
#endif

      /* Check if entry points/icons symlinks (main+v2) are properly created
       * and entry point/icons symlinks for v1 are removed */
      tests_assert_type_nofollow (main_entry_point_path, S_IFREG);
      tests_assert_symlink_to (main_entry_point_link_path,
                               main_entry_point_path);
      tests_assert_no_file (entry_point_v1_path);
      tests_assert_no_file (entry_point_v1_link_path);
      tests_assert_type_nofollow (entry_point_v2_path, S_IFREG);
      tests_assert_symlink_to (entry_point_v2_link_path, entry_point_v2_path);

      tests_assert_type_nofollow (main_default_icon_path, S_IFREG);
      tests_assert_symlink_to (main_default_icon_link_path,
                               main_default_icon_path);
      tests_assert_no_file (icon_default_v1_path);
      tests_assert_no_file (icon_default_v1_link_path);
      tests_assert_type_nofollow (icon_default_v2_path, S_IFREG);
      tests_assert_symlink_to (icon_default_v2_link_path,
                               icon_default_v2_path);

      tests_assert_type_nofollow (main_alternative_icon_path, S_IFREG);
      tests_assert_symlink_to (main_alternative_icon_link_path,
                               main_alternative_icon_path);
      tests_assert_no_file (icon_alternative_v1_path);
      tests_assert_no_file (icon_alternative_v1_link_path);
      tests_assert_type_nofollow (icon_alternative_v2_path, S_IFREG);
      tests_assert_symlink_to (icon_alternative_v2_link_path,
                               icon_alternative_v2_path);

      /* Remove rollback snapshot */

      tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      times));
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

      tests_app_bundle_delete_rollback_snapshot (&f->bundle, old_version);

      /* it had no effect */
      tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                      &stat_buf));
      g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);
      g_file_get_contents (stamp, &stamp_contents, NULL, &error);
      g_assert_no_error (error);
      g_assert_cmpstr (stamp_contents, ==, new_version->number);
    }
  /* ... else skip testing RollBackApp, DeleteRollbackSnapshot, UninstallApp
   * and ReInstallApp, none of which can be done without using the
   * low-level APIs */

  /* Remove */

  tests_assert_syscall_0 (utimes (CBY_APPSTREAM_CACHE_INSTALLED_STORE, times));
  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, <, then.tv_sec);

#ifdef TEST_FULL_VERSION
  /* Remove current MIME cache database to make sure it gets updated */
  tests_assert_syscall_0 (unlink (mime_cache_path));
  tests_assert_syscall_0 (unlink (default_icon_cache_path));
  tests_assert_syscall_0 (unlink (alternative_icon_cache_path));
#endif

  tests_app_bundle_remove (&f->bundle);

  tests_assert_syscall_0 (g_stat (CBY_APPSTREAM_CACHE_INSTALLED_STORE,
                                  &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  tests_assert_no_file (stamp);

#ifdef TEST_FULL_VERSION
  /* Check if MIME cache database was updated */
  tests_assert_syscall_0 (g_stat (mime_cache_path, &stat_buf));
  g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
  g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
#endif

  if (g_stat (default_icon_cache_path, &stat_buf) == 0)
    {
      /* We probably have some other app icon on the search path */

      /* Check if icon cache database was updated */
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
    }
  else if (errno == ENOENT)
    {
      /* Do nothing - we probably don't have any icon on the search path */
    }
  else
    {
      g_error ("g_stat (\"%s\"): %s", default_icon_cache_path, g_strerror (errno));
    }

  if (g_stat (alternative_icon_cache_path, &stat_buf) == 0)
    {
      /* We probably have some other app icon on the search path */

      /* Check if icon cache database was updated */
      g_assert_cmpint (stat_buf.st_mtime, >=, then.tv_sec);
      g_assert_cmpint (stat_buf.st_mode & S_IFMT, ==, S_IFREG);
    }
  else if (errno == ENOENT)
    {
      /* Do nothing - we probably don't have any icon on the search path */
    }
  else
    {
      g_error ("g_stat (\"%s\"): %s", alternative_icon_cache_path, g_strerror (errno));
    }

#ifdef TEST_FULL_VERSION
  test_app_registered (f->bundle.id, main_entry_point_mime_type, FALSE);
  test_app_registered (entry_point_v1_id, entry_point_v1_mime_type, FALSE);
  test_app_registered (entry_point_v2_id, entry_point_v2_mime_type, FALSE);
  test_system_app_registered ();
#endif

  /* Check if entry points/icons (main+v1+v2) and the corresponding symlinks are
   * properly removed */
  tests_assert_no_file (main_entry_point_path);
  tests_assert_no_file (main_entry_point_link_path);
  tests_assert_no_file (entry_point_v1_path);
  tests_assert_no_file (entry_point_v1_link_path);
  tests_assert_no_file (entry_point_v2_path);
  tests_assert_no_file (entry_point_v2_link_path);

  tests_assert_no_file (main_default_icon_path);
  tests_assert_no_file (main_default_icon_link_path);
  tests_assert_no_file (icon_default_v1_path);
  tests_assert_no_file (icon_default_v1_link_path);
  tests_assert_no_file (icon_default_v2_path);
  tests_assert_no_file (icon_default_v2_link_path);

  tests_assert_no_file (main_alternative_icon_path);
  tests_assert_no_file (main_alternative_icon_link_path);
  tests_assert_no_file (icon_alternative_v1_path);
  tests_assert_no_file (icon_alternative_v1_link_path);
  tests_assert_no_file (icon_alternative_v2_path);
  tests_assert_no_file (icon_alternative_v2_link_path);
}

static void
test_install_apparmor (Fixture *f,
                       gconstpointer user_data)
{
  g_autofree gchar *installed_profile = NULL;
  g_autofree gchar *installed_symlink = NULL;
  g_autofree gchar *profile_basename = NULL;
  g_autofree gchar *contents = NULL;
  g_autofree gchar *real_path = NULL;
  g_autofree gchar *helper_rel = NULL;
  g_autofree gchar *helper = NULL;
  g_autofree gchar *profile = NULL;
  g_autofree gchar *working_directory = NULL;
  g_autofree gchar *cache = NULL;
  g_autofree gchar *ubercache = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) child = NULL;
  GStatBuf stat_buf;
  const TestsVersion *old_version = &f->bundle.versions[0];
  const TestsVersion *new_version = &f->bundle.versions[1];
  const TestsVersion *current_version;
  gchar template[] = "/tmp/cby-postinst-prerm.XXXXXX";
  const gchar *tmpdir;
  const gchar *api = user_data;
  gboolean use_low_level = (g_strcmp0 (api, "low-level") == 0);

  if (!tests_require_running_as_root ())
    return;

  if (g_test_failed ())
    return;

  if (!tests_require_apparmor_active ())
    return;

  working_directory = g_get_current_dir ();
  tmpdir = g_mkdtemp (template);
  g_assert_true (tmpdir == template);

  helper_rel = g_test_build_filename (G_TEST_BUILT, "postinst-test-helper",
                                      NULL);

  if (g_path_is_absolute (helper_rel))
    helper = g_strdup (helper_rel);
  else
    helper = g_build_filename (working_directory, helper_rel, NULL);

  profile = g_strdup_printf ("%s/%s/**",
                             CBY_PATH_PREFIX_STORE_BUNDLE,
                             f->bundle.id);

  profile_basename = g_strdup_printf ("Applications.%s",
                                      f->bundle.id);
  installed_symlink = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS,
                                        "apparmor.d", profile_basename, NULL);
  installed_profile = g_build_filename (CBY_PATH_PREFIX_STORE_BUNDLE,
                                        f->bundle.id, "etc",
                                        "apparmor.d", profile_basename, NULL);
  /* Put some stupid contents in the ubercache first to be able to verify
   * that it gets re-populated */
  ubercache = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS,
                                "apparmor.d", "cache", ".ubercache", NULL);
  g_file_set_contents (ubercache, "hello", -1, &error);
  g_assert_no_error (error);

  /* Install */
  if (use_low_level)
    {
      g_autofree gchar *unpack_to =
          tests_app_bundle_begin_install (&f->bundle, NULL, old_version);

      /* The ubercache has been invalidated. We can't test this with
       * the high-level API because we don't control precisely when
       * it gets invalidated, which is why this code path is still here. */
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, ==, 0);

      /* Keep this code to populate the app-bundle in sync with
       * tests/make-bundle.py */
      g_mkdir_with_parents (tests_printf ("%s/etc/apparmor.d",
                                          unpack_to), 0755);
      g_file_set_contents (tests_printf ("%s/etc/apparmor.d/%s",
                                         unpack_to, profile_basename),
                           tests_printf ("# Version 1\n"
                                         "#include <tunables/global>\n"
                                         "/Applications/%s/** {\n"
                                         "  #include <abstractions/base>\n"
                                         "  %s rmix,\n"
                                         "  %s/both-may-write{,.*} rw,\n"
                                         "  %s/v1-may-write{,.*} rw,\n"
                                         "}\n",
                                         f->bundle.id,
                                         helper,
                                         tmpdir,
                                         tmpdir),
                           -1, &error);
      g_assert_no_error (error);
      g_clear_pointer (&unpack_to, g_free);

      tests_app_bundle_commit_install (&f->bundle, NULL, old_version);
    }
  else
    {
      tests_app_bundle_install_bundle (&f->bundle, NULL, old_version);
    }

  /* ubercache has been populated */
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, >=, MINIMUM_REASONABLE_UBERCACHE_SIZE);

  /* Check that the bundle's AppArmor profile has been symlinked
   * into place */
  real_path = realpath (installed_symlink, NULL);
  g_assert_cmpstr (real_path, ==, installed_profile);
  g_clear_pointer (&real_path, free);
  g_file_get_contents (installed_symlink, &contents, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (g_str_has_prefix (contents, "# Version 1\n"));
  g_clear_pointer (&contents, g_free);

  /* Check that the bundle's AppArmor profile is actually loaded */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile,
                            "--",
                            helper,
                            tmpdir,
                            old_version->number,
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  /* Upgrade */

  if (use_low_level)
    {
      g_autofree gchar *unpack_to =
          tests_app_bundle_begin_install (&f->bundle, old_version, new_version);

      /* ubercache has been invalidated again */
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, ==, 0);

      g_mkdir_with_parents (tests_printf ("%s/etc/apparmor.d",
                                          unpack_to), 0755);
      g_file_set_contents (tests_printf ("%s/etc/apparmor.d/Applications.%s",
                                         unpack_to, f->bundle.id),
                           tests_printf ("# Version 2\n"
                                         "#include <tunables/global>\n"
                                         "/Applications/%s/** {\n"
                                         "  #include <abstractions/base>\n"
                                         "  %s rmix,\n"
                                         "  %s/both-may-write{,.*} rw,\n"
                                         "  %s/v2-may-write{,.*} rw,\n"
                                         "}\n",
                                         f->bundle.id,
                                         helper,
                                         tmpdir,
                                         tmpdir),
                           -1, &error);
      g_assert_no_error (error);
      g_clear_pointer (&unpack_to, g_free);

      tests_app_bundle_commit_install (&f->bundle, old_version, new_version);
    }
  else
    {
      tests_app_bundle_install_bundle (&f->bundle, old_version, new_version);
    }

  /* ubercache has been populated */
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, >=, MINIMUM_REASONABLE_UBERCACHE_SIZE);

  /* Check that the bundle's new AppArmor profile has been symlinked
   * into place */
  real_path = realpath (installed_symlink, NULL);
  g_assert_cmpstr (real_path, ==, installed_profile);
  g_clear_pointer (&real_path, free);
  g_file_get_contents (installed_symlink, &contents, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (g_str_has_prefix (contents, "# Version 2\n"));
  g_clear_pointer (&contents, g_free);

  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile,
                            "--",
                            helper,
                            tmpdir,
                            new_version->number,
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  if (use_low_level)
    {
      /* Roll back to old version. We can only do this with the older AppStore
       * API at the moment. */

      /* Put some stupid contents in the ubercache first to be able to verify
       * that it gets re-populated */
      g_file_set_contents (ubercache, "hello", -1, &error);
      g_assert_no_error (error);

      tests_app_bundle_roll_back (&f->bundle, TESTS_ROLLBACK_MODE_ROLL_BACK,
                                  new_version, old_version);

      /* ubercache has been re-populated */
      tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
      g_assert_cmpint (stat_buf.st_size, >=, MINIMUM_REASONABLE_UBERCACHE_SIZE);

      /* We are back to the old AppArmor profile */
      real_path = realpath (installed_symlink, NULL);
      g_assert_cmpstr (real_path, ==, installed_profile);
      g_clear_pointer (&real_path, free);
      g_file_get_contents (installed_symlink, &contents, NULL, &error);
      g_assert_no_error (error);
      g_assert_true (g_str_has_prefix (contents, "# Version 1\n"));
      g_clear_pointer (&contents, g_free);

      child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                                &error,
                                "aa-exec",
                                "-p", profile,
                                "--",
                                helper,
                                tmpdir,
                                old_version->number,
                                NULL);
      g_assert_no_error (error);
      g_subprocess_wait_check (child, NULL, &error);
      g_assert_no_error (error);
      g_clear_object (&child);

      current_version = old_version;
    }
  else
    {
      /* We are still on the new version. */
      current_version = new_version;
    }

  /* Simulate what might have happened if we lost power part way through
   * installation */
  g_file_set_contents (ubercache, "this is corrupted", -1, &error);
  g_assert_no_error (error);

  /* The profile wouldn't have been loaded at the beginning of boot */
  tests_remove_apparmor_profile (profile, &error);
  g_assert_no_error (error);

  /* Run canterbury-init (in real life this would happen during boot) */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            PKGLIBEXECDIR "/canterbury-init",
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  /* ubercache has been re-populated */
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, >=, MINIMUM_REASONABLE_UBERCACHE_SIZE);

  /* Profile is loaded */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile,
                            "--",
                            helper,
                            tmpdir,
                            current_version->number,
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  /* Simulate what might have happened if we lost power part way through
   * installation - a different version, where ubercache has been deleted,
   * and the cache is also corrupt */
  tests_assert_syscall_0 (g_unlink (ubercache));
  cache = g_build_filename (CBY_PATH_SYSTEM_EXTENSIONS, "apparmor.d", "cache",
                            profile_basename, NULL);
  g_file_set_contents (cache, "this is corrupted", -1, &error);
  g_assert_no_error (error);

  /* Again, the profile wouldn't have been loaded at the beginning of boot */
  tests_remove_apparmor_profile (profile, &error);
  g_assert_no_error (error);

  /* Again, run canterbury-init (in real life this would happen during boot) */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            PKGLIBEXECDIR "/canterbury-init",
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  /* ubercache has been re-populated */
  tests_assert_syscall_0 (g_stat (ubercache, &stat_buf));
  g_assert_cmpint (stat_buf.st_size, >=, MINIMUM_REASONABLE_UBERCACHE_SIZE);

  /* Profile is loaded */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_NONE,
                            &error,
                            "aa-exec",
                            "-p", profile,
                            "--",
                            helper,
                            tmpdir,
                            current_version->number,
                            NULL);
  g_assert_no_error (error);
  g_subprocess_wait_check (child, NULL, &error);
  g_assert_no_error (error);
  g_clear_object (&child);

  /* Remove */

  tests_app_bundle_remove (&f->bundle);

  g_unlink (tests_printf ("%s/%s", tmpdir, "both-may-write"));
  g_unlink (tests_printf ("%s/%s", tmpdir, "v1-may-write"));
  g_unlink (tests_printf ("%s/%s", tmpdir, "v2-may-write"));
  tests_assert_syscall_0 (g_rmdir (tmpdir));
}

static void
test_remove_apparmor (Fixture *f,
                      gconstpointer unused G_GNUC_UNUSED)
{
  g_autofree gchar *unlink_on_usr1 = NULL;
  g_autofree gchar *unlink_on_usr2 = NULL;
  g_autofree gchar *helper_rel = NULL;
  g_autofree gchar *helper = NULL;
  g_autofree gchar *profile = NULL;
  g_autofree gchar *profile_text = NULL;
  g_autofree gchar *working_directory = NULL;
  gchar template[] = "/tmp/cby-postinst-prerm.XXXXXX";
  gchar buffer[1024];
  gsize bytes_read;
  const gchar *tmpdir;
  g_autoptr (GError) error = NULL;
  g_autoptr (GSubprocess) child = NULL;
  g_autoptr (GAsyncResult) result = NULL;
  g_autoptr (GVariant) tuple = NULL;
  GInputStream *child_stdout = NULL;
  const TestsVersion *version = &f->bundle.versions[0];
  gint64 pid;
  guint i;
  gboolean success;

  if (!tests_require_running_as_root ())
    return;

  if (!tests_require_apparmor_active ())
    return;

  if (g_test_failed ())
    return;

  working_directory = g_get_current_dir ();
  tmpdir = g_mkdtemp (template);
  g_assert_true (tmpdir == template);
  /* The test helper, prerm-apparmor-helper, will unlink these files when
   * signalled. */
  unlink_on_usr1 = g_build_filename (tmpdir, "unlink-on-usr1", NULL);
  unlink_on_usr2 = g_build_filename (tmpdir, "unlink-on-usr2", NULL);
  g_file_set_contents (unlink_on_usr1, "hello", -1, &error);
  g_assert_no_error (error);
  g_file_set_contents (unlink_on_usr2, "hello", -1, &error);
  g_assert_no_error (error);

  helper_rel = g_test_build_filename (G_TEST_BUILT, "prerm-apparmor-helper",
                                      NULL);

  if (g_path_is_absolute (helper_rel))
    helper = g_strdup (helper_rel);
  else
    helper = g_build_filename (working_directory, helper_rel, NULL);

  profile = g_strdup_printf ("%s/%s/**",
                             CBY_PATH_PREFIX_STORE_BUNDLE,
                             f->bundle.id);

  tests_app_bundle_install_bundle (&f->bundle, NULL, version);

  /* Load an AppArmor profile that pretends to be the one for that
   * bundle. This will overwrite the one that was really in the bundle. */
  profile_text = g_strdup_printf ("#include <tunables/global>\n"
                                  "%s (complain) {\n"
                                  "  #include <abstractions/base>\n"
                                  "  %s rmix,\n"
                                  "  %s/{,**} rw,\n"
                                  "}\n",
                                  profile,
                                  helper,
                                  tmpdir);
  _cby_apparmor_load_profile_from_string (profile_text, &error);
  g_assert_no_error (error);
  g_clear_pointer (&profile_text, g_free);

  /* Install an AppArmor profile that is in the way of removal. 
   * It's empty, because we never actually use it. */
  profile_text = g_strdup_printf ("%s/%s/InTheWay (complain) {\n"
                                  "}\n",
                                  CBY_PATH_PREFIX_STORE_BUNDLE,
                                  f->bundle.id);
  _cby_apparmor_load_profile_from_string (profile_text, &error);
  g_assert_no_error (error);
  g_clear_pointer (&profile_text, g_free);

  /* Install an AppArmor profile that is not in the way of removal:
   * it's an unrelated app-bundle that happens to have a similar name. */
  profile_text = g_strdup_printf ("%s/%s.ExceptNot (complain) {\n"
                                  "}\n",
                                  CBY_PATH_PREFIX_STORE_BUNDLE,
                                  f->bundle.id);
  _cby_apparmor_load_profile_from_string (profile_text, &error);
  g_assert_no_error (error);
  g_clear_pointer (&profile_text, g_free);

  /* Run a subprocess with that AppArmor profile. Wait for it to signal ready,
   * which it does by closing its stdout. */
  child = g_subprocess_new (G_SUBPROCESS_FLAGS_STDOUT_PIPE,
                            &error,
                            "aa-exec",
                            "-p", profile,
                            "--",
                            helper,
                            tmpdir,
                            NULL);
  g_assert_no_error (error);

  /* The child signals that it is ready by closing its stdout */
  child_stdout = g_subprocess_get_stdout_pipe (child);
  g_input_stream_read_all (child_stdout, buffer, sizeof (buffer), &bytes_read,
                           NULL, &error);
  g_assert_no_error (error);
  g_assert (bytes_read == 0);

  pid = g_ascii_strtoll (g_subprocess_get_identifier (child), NULL, 10);
  g_assert_cmpint (pid, >, 0);
  g_assert_cmpint (pid, <, G_MAXINT64);

  tests_assert_syscall_0 (kill (pid, SIGUSR1));

  /* Wait up to 5s for it to happen */
  for (i = 0; i < 50; i++)
    {
      if (!g_file_test (unlink_on_usr1, G_FILE_TEST_EXISTS))
        break;

      g_usleep (G_USEC_PER_SEC / 10);
    }

  tests_assert_no_file (unlink_on_usr1);
  tests_assert_type_nofollow (unlink_on_usr2, S_IFREG);

  /* Removing the app-bundle should fail, because the InTheWay profile
   * is in the way. */
  success = tests_app_bundle_try_remove (&f->bundle, &error);
  g_assert_error (error, G_SPAWN_EXIT_ERROR, 1);
  g_assert_cmpint (success, ==, FALSE);
  g_clear_error (&error);

  /* Get rid of the profile that is in the way. */
  tests_remove_apparmor_profile (tests_printf ("%s/%s/InTheWay",
                                               CBY_PATH_PREFIX_STORE_BUNDLE,
                                               f->bundle.id), &error);
  g_assert_no_error (error);

  /* Now we *can* remove the app-bundle. The ...ExceptNot profile is not
   * a problem. */
  tests_app_bundle_remove (&f->bundle);

  /* It can receive our SIGUSR2, but it can't do anything in response.
   * It exits with status equal to SIGUSR2. */
  tests_assert_syscall_0 (kill (pid, SIGUSR2));
  g_subprocess_wait (child, NULL, &error);
  g_assert_no_error (error);
  g_assert_true (g_subprocess_get_if_exited (child));
  g_assert_cmpint (g_subprocess_get_exit_status (child), ==, SIGUSR2);

  /* It was unable to unlink the file, so the file is still there. */
  tests_assert_type_nofollow (unlink_on_usr2, S_IFREG);

  tests_remove_apparmor_profile (tests_printf ("%s/%s.ExceptNot",
                                               CBY_PATH_PREFIX_STORE_BUNDLE,
                                               f->bundle.id), &error);
  g_assert_no_error (error);
  tests_assert_syscall_0 (g_unlink (unlink_on_usr2));
  tests_assert_syscall_0 (g_rmdir (tmpdir));
}

static void
teardown (Fixture *f,
          gconstpointer unused G_GNUC_UNUSED)
{
  tests_app_bundle_tear_down (&f->bundle);
  g_clear_object (&f->app_store_proxy);
  g_clear_object (&f->bundle_manager1_proxy);
}

int
main (int argc,
    char **argv)
{
  cby_init_environment ();

  g_test_init (&argc, &argv, NULL);

  g_test_add ("/postinst-prerm/install/bundle-manager1",
              Fixture, "bundle-manager1", setup, test_install, teardown);
  g_test_add ("/postinst-prerm/install/low-level",
              Fixture, "low-level", setup, test_install, teardown);

  g_test_add ("/postinst-prerm/install-apparmor/bundle-manager1",
              Fixture, "bundle-manager1", setup, test_install_apparmor,
              teardown);
  g_test_add ("/postinst-prerm/install-apparmor/low-level",
              Fixture, "low-level", setup, test_install_apparmor, teardown);

  g_test_add ("/postinst-prerm/remove-apparmor", Fixture, NULL, setup,
              test_remove_apparmor, teardown);

  return g_test_run ();
}
