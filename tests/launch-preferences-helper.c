/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <gio/gio.h>

/*
 * launch-preferences-helper — helper for launch-preferences test case
 *
 * Usage:
 * launch-preferences-helper
 *
 * This helper is set as the system preferences application for Canterbury to
 * launch when LaunchAppPreferences() is called. It grabs a well-known name on
 * the session bus, which launch-preferences is watching, and then exits. This
 * is a simple signal to launch-preferences that the system preferences
 * application was successfully launched.
 */

typedef guint BusNameId;
G_DEFINE_AUTO_CLEANUP_FREE_FUNC (BusNameId, g_bus_unown_name, 0)

static guint name_watch_id = 0;

static void
test_done_cb (GDBusConnection *connection,
              const gchar *name,
              const gchar *owner,
              gpointer user_data)
{
  gboolean *done = user_data;

  g_debug ("Name ‘%s’ now owned by '%s'.", name, owner);
  *done = TRUE;

}

static void
name_acquired_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  gboolean *done = user_data;

  g_debug ("Name ‘%s’ acquired.", name);

  /* We need to give Canterbury time to find out this process's process ID
   * from systemd before we let it exit, because Canterbury tracks
   * process IDs to be able to pass them to Shapwick. If this process
   * exits too soon then Canterbury will report its activation as having
   * failed. Wait for the launch-preferences unit test to own a name:
   * this is a simple way it can signal its presence that doesn't require
   * extra (unrealistic) AppArmor permissions. */
  name_watch_id = g_bus_watch_name_on_connection (connection,
      "org.apertis.CanterburyTests.LaunchPreferences.Done",
      G_BUS_NAME_WATCHER_FLAGS_NONE, test_done_cb, NULL, done, NULL);
}

static void
name_lost_cb (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{
  g_error ("Name ‘%s’ lost.", name);
}

int
main (int argc,
    char **argv)
{
  g_auto (BusNameId) name_id = 0;
  gboolean done = FALSE;

  /* Call back to the test that ran us, by grabbing a name on the bus
   * temporarily. */
  name_id = g_bus_own_name (G_BUS_TYPE_SESSION,
                            "org.apertis.CanterburyTests.LaunchPreferences.Helper",
                            G_BUS_NAME_OWNER_FLAGS_REPLACE |
                            G_BUS_NAME_OWNER_FLAGS_ALLOW_REPLACEMENT,
                            NULL, name_acquired_cb, name_lost_cb, &done, NULL);

  while (!done)
    g_main_context_iteration (NULL, TRUE);

  g_bus_unwatch_name (name_watch_id);
  return 0;
}
