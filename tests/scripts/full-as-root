#!/bin/sh

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e

exec 2>&1

skip_all () {
    echo "SKIP: $*"
    # We exit 0 because autopkgtest has no concept of a skipped test, only
    # pass or fail, and we don't want tests that are not applicable to
    # root to be a hard failure when run as non-root.
    exit 0
}

case "$(id -n -u)" in
    (root)
        as_root=
        ;;

    (user)
        if ! sudo --non-interactive --validate; then
            skip_all "cannot use sudo without password here"
        fi
        as_root="sudo -H --non-interactive"
        ;;

    (*)
        skip_all "neither root nor user; is this really Apertis?"
        ;;
esac

# It's inappropriate to use gvfs in privileged processes
export GIO_USE_VFS=local

# The SDK "simulator" sometimes shuts down Ribchester; make sure it's back up
$as_root systemctl start ribchester.service

NULL=
EXIT_STATUS=0

$as_root \
    dbus-run-session -- \
    gnome-desktop-testing-runner \
    canterbury-0/postinst-prerm-core.t \
    canterbury-0/postinst-prerm.t \
    canterbury-0/process-info.t \
    ${NULL} || EXIT_STATUS=$?

$as_root \
    dbus-run-session -- \
    python3 /usr/lib/installed-tests/canterbury-0/systemd-mock-run.py --  \
    gnome-desktop-testing-runner \
    canterbury-0/service-manager.t \
    ${NULL} || EXIT_STATUS=$?

exit $EXIT_STATUS
