/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * agent-helper — an instrumented agent (session service) for the Canterbury
 *                test suite
 *
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <errno.h>
#include <signal.h>
#include <sys/prctl.h>

#include <glib.h>
#include <gio/gio.h>

#define BUNDLE_ID "org.apertis.CanterburyTests.Agent"
#define ENTRY_POINT_ID BUNDLE_ID
#define AGENT_IFACE BUNDLE_ID

/* We "crash" if we notice this name on the session bus. */
#define TRIGGER BUNDLE_ID ".PleaseCrash"

#define TESTS_TYPE_AGENT (tests_agent_get_type ())
G_DECLARE_FINAL_TYPE (TestsAgent, tests_agent, TESTS, AGENT, GApplication)

struct _TestsAgent
{
  GApplication parent;
};

G_DEFINE_TYPE (TestsAgent, tests_agent, G_TYPE_APPLICATION)

static void
trigger_appeared_cb (GDBusConnection *connection G_GNUC_UNUSED,
                     const gchar *name G_GNUC_UNUSED,
                     const gchar *name_owner G_GNUC_UNUSED,
                     gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;

  /* Before crashing, tell the test what is going on. */
  g_dbus_connection_emit_signal (connection, NULL, "/", AGENT_IFACE,
                                 "CrashTriggered", NULL, &error);
  g_assert_no_error (error);
  /* We need to flush the connection, because otherwise our untimely death
   * will probably kill the message-sending thread before the signal is
   * sent. */
  g_dbus_connection_flush_sync (connection, NULL, &error);
  g_assert_no_error (error);

  /* We have been asked to crash. Raise a fatal signal. */
  raise (SIGBUS);
  g_assert_not_reached ();
}

static gboolean
tests_agent_dbus_register (GApplication     *app,
                                    GDBusConnection  *connection,
                                    const gchar      *object_path,
                                    GError          **error)
{
  g_autoptr (GError) local_error = NULL;

  g_application_hold (app);

  g_dbus_connection_emit_signal (connection, NULL, "/", AGENT_IFACE,
                                 "Started", NULL, &local_error);
  g_assert_no_error (local_error);

  /* To test https://phabricator.apertis.org/T3414 we need a way to be told
   * to (pretend to) crash. */
  g_bus_watch_name_on_connection (connection, TRIGGER,
                                  G_BUS_NAME_WATCHER_FLAGS_NONE,
                                  trigger_appeared_cb,
                                  NULL, NULL, NULL);

  return G_APPLICATION_CLASS (tests_agent_parent_class)->
      dbus_register (app, connection, object_path, error);
}

static void
tests_agent_class_init (TestsAgentClass *cls)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (cls);

  app_class->dbus_register = tests_agent_dbus_register;
}

static void
tests_agent_init (TestsAgent *self)
{
}

int
main (int argc, char **argv)
{
  g_autoptr (GApplication) app = NULL;

  /* Don't leave debris behind. We don't want systemd-coredumpd to collect
   * this, since it isn't really a failure that it "crashes" when asked
   * to do so. */
  if (prctl (PR_SET_DUMPABLE, 0, 0, 0, 0) != 0)
    g_error ("Unable to disable core dump: %s", g_strerror (errno));

  app = G_APPLICATION (g_object_new (TESTS_TYPE_AGENT,
                                     "application-id", ENTRY_POINT_ID,
                                     "flags", G_APPLICATION_IS_SERVICE,
                                     NULL));

  return g_application_run (app, argc, argv);
}
