/*
 * Copyright © 2015 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_BUNDLE_H__
#define __CANTERBURY_BUNDLE_H__

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury.h> can be included directly"
#endif

#include <glib.h>

G_BEGIN_DECLS

gboolean cby_is_bundle_id (const gchar *maybe_id);
const gchar *cby_get_bundle_id (void);

gboolean cby_is_entry_point_id (const gchar *maybe_id);

G_END_DECLS

#endif
