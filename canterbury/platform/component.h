/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CANTERBURY_PLATFORM_COMPONENT_H
#define CANTERBURY_PLATFORM_COMPONENT_H

#if !defined(__CANTERBURY_IN_META_HEADER__) && !defined(CANTERBURY_COMPILATION)
#error "Only <canterbury/canterbury-platform.h> can be included directly"
#endif

#include <appstream-glib.h>
#include <glib.h>

#include <canterbury/process-info.h>

G_BEGIN_DECLS

#define CBY_TYPE_COMPONENT (cby_component_get_type ())
G_DECLARE_FINAL_TYPE (CbyComponent, cby_component, CBY, COMPONENT, GObject)

CbyComponent *cby_component_new_for_appstream_app (CbyProcessType type,
                                                   AsApp *app,
                                                   GError **error);
AsApp *cby_component_get_appstream_app (CbyComponent *self);
CbyProcessType cby_component_get_component_type (CbyComponent *self);
const gchar *cby_component_get_id (CbyComponent *self);
const gchar *cby_component_get_bundle_id (CbyComponent *self);

const gchar *const *cby_component_get_categories (CbyComponent *self);

const gchar *cby_component_get_settings_schema_id (CbyComponent *self);

G_END_DECLS

#endif
