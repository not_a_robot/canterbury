/*<private_header>*/
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_SRC_ENTRY_POINT_INTERNAL_H__
#define __CANTERBURY_SRC_ENTRY_POINT_INTERNAL_H__

#include "entry-point.h"
#include "canterbury/gdbus/canterbury_app_handler.h"
#include "canterbury/process-info.h"

typedef enum _CbyServiceType CbyServiceType;

enum _CbyServiceType
{
  APPSTORE = 1,
  MEDIA_USB,
  MEDIA_UPNP,
  INVALID_TYPE
};

struct _CbyEntryPoint
{
  GObject parent;
};

#define CBY_ENTRY_POINT_KEY_X_APERTIS_SERVICE_EXEC \
  "X-Apertis-ServiceExec"
#define CBY_ENTRY_POINT_KEY_X_APERTIS_PARENT_ENTRY \
  "X-Apertis-ParentEntry"

#define CBY_OBJECT_PATH_PRIVATE_ENTRY_POINTS1 \
    "/org/apertis/Canterbury/Private/EntryPoints1"
#define CBY_INTERFACE_PRIVATE_ENTRY_POINTS1 \
    "org.apertis.Canterbury.Private.EntryPoints1"

CbyEntryPoint *_cby_entry_point_new_from_app_info (CbyServiceType service_type,
                                                   const gchar *app_name,
                                                   GAppInfo *app_info,
                                                   GError **error);
gboolean _cby_entry_point_update_from_app_info (CbyEntryPoint *self,
                                                GAppInfo *app_info,
                                                gboolean *changed,
                                                GError **error);

CanterburyAppBkgState _cby_entry_point_get_background_state (CbyEntryPoint *self);

const gchar *_cby_entry_point_get_working_directory (CbyEntryPoint *self);

CanterburyExecutableType _cby_entry_point_get_executable_type (CbyEntryPoint *self);

const gchar *const *_cby_entry_point_get_arguments (CbyEntryPoint *self);
const gchar *const *_cby_entry_point_get_argv (CbyEntryPoint *self);

CanterburyAudioType _cby_entry_point_get_audio_resource_type (CbyEntryPoint *self);

const gchar *_cby_entry_point_get_audio_channel (CbyEntryPoint *self);

const gchar * _cby_entry_point_get_audio_resource_owner_id (CbyEntryPoint *self);

const gchar *_cby_entry_point_get_category_label (CbyEntryPoint *self);

const gchar *_cby_entry_point_get_mangled_display_name (CbyEntryPoint *self);

CanterburyBandwidthPriority _cby_entry_point_get_internet_bandwidth_priority (CbyEntryPoint *self);

const gchar *_cby_entry_point_get_splash_screen (CbyEntryPoint *self);

CbyServiceType _cby_entry_point_get_service_type (CbyEntryPoint *self);

GAppInfo *_cby_entry_point_get_app_info (CbyEntryPoint *self);

CbyProcessInfo *_cby_entry_point_get_process_info (CbyEntryPoint *self);

gboolean _cby_entry_point_get_dbus_activatable (CbyEntryPoint *self);

#endif
