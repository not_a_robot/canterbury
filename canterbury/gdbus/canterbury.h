/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <canterbury/gdbus/org.apertis.Canterbury.AppDbHandler.h>
#include <canterbury/gdbus/org.apertis.Canterbury.AppManager.h>
#include <canterbury/gdbus/org.apertis.Canterbury.AudioMgr.h>
#include <canterbury/gdbus/org.apertis.Canterbury.HardKeys.h>
#include <canterbury/gdbus/org.apertis.Canterbury.Launcher.h>
#include <canterbury/gdbus/org.apertis.Canterbury.Mutter.Plugin.h>
#include <canterbury/gdbus/canterbury_app_handler.h>
#include <canterbury/gdbus/enumtypes.h>
