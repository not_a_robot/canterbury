<node>
<!--
  org.apertis.Canterbury.HardKeys:
  @short_description: The Hard Keys service of application manager provides SDK API to take keyboard inputs and provide necessary actions. It also provides back handling API's for applications.

-->

<interface name="org.apertis.Canterbury.HardKeys">

  <!--
    BackPress:
    @since: 1.0

    Tell the foreground graphical program to switch to a different view,
    or switch to a previous foreground graphical program.

    This method is intended to be called by a platform component
    (FIXME: which one?) when the Back key is pressed.
  -->
  <method name="BackPress">
  </method>

  <!--
    BackPressResponse:
    @result: TRUE if something happened. FALSE if the Launcher was already
      on top and remains on top.
    @since: 1.0

    Emitted when the Back key has been pressed and the action to be taken
    has been identified.

    FIXME: Do programs other than the caller of BackPress listen to this
    signal? Why would a program listen to this signal?
  -->
  <signal name="BackPressResponse">
    <arg name="result" type="b"/>
  </signal>

  <!--
    RegisterBackPress:
    @entry_point_id: ID of the entry point registering for back press events
    @since: 1.0

    Register to be sent BackPressRegisterResponse signals.
  -->
  <method name="RegisterBackPress">
  <arg name="entry_point_id" direction="in" type="s"/>
  </method>

  <!--
    BackPressRegisterResponse:
    @entry_point_id: Entry point ID of the foreground graphical program
    @since: 1.0

    Emitted when BackPress() is called while the foreground graphical program
    is one that had previously called RegisterBackPress().

    That program is expected to call BackPressConsumed() soon. If it calls
    that method with a %TRUE argument, it is assumed to have gone back to
    a previous state, and the application manager does not react to that
    Back keypress any further.

    If it calls that method with a %FALSE argument (or does not call it
    at all within a reasonable time), then it is assumed to
    have declined to handle the Back keypress (for example if it does not
    have a previous state to go back to), and the application manager will
    switch to a previous foreground graphical program instead.

    Other programs must ignore this signal. (FIXME: This violates the
    app-confidentiality security property; see
    <https://phabricator.apertis.org/T3389>)
  -->
  <signal name="BackPressRegisterResponse">
    <arg name="entry_point_id" type="s"/>
  </signal>

  <!--
    BackPressConsumed:
    @entry_point_id: Entry point ID of the application
    @consumed_status: Information if the application handled the back key or not
    @since: 1.0

    When the app manager sends the BackPressResponse messsage to the app on top, apps view manager has to inform app manager if it consumed it or not by using this method. This should be send for every BackPressResponse message sent. If this method is not called with 3 seconds from BackPressResponse message the app manager automatically switches to previous app.
  -->
  <method name="BackPressConsumed">
   <arg name="entry_point_id"  direction="in" type="s"/>
   <arg name="consumed_status" direction="in" type="b"/>
  </method>

  <!--
    DisableBackPress:
    @entry_point_id: Entry point ID of the application disabling back press events
    @disable_status: TRUE means disable, FALSE means enable
    @since: 1.0

    Applications can temporarily disable back key
  -->
  <method name="DisableBackPress">
   <arg name="entry_point_id" direction="in" type="s"/>
   <arg name="disable_status" direction="in" type="b"/>
  </method>

  <!--
    DisableBackPressResponse:
    @disable_status: TRUE means disabled, FALSE means enabled
    @since: 1.0

    Signal requesting client to disable back button
  -->
  <signal name="DisableBackPressResponse">
    <arg name="disable_status" type="b"/>
  </signal>

  <!--
    HomePress:
    @since: 1.0

    This message is issued on pressing the home key
  -->
  <method name="HomePress">
  </method>

  <!--
    HomePressResponse:
    @result: Back key Handled response TRUE or FALSE
    @since: 1.0

    Response message for home key press
  -->
  <signal name="HomePressResponse">
    <arg name="result" type="b"/>
  </signal>

  <!--
    NowPlayingPress:
    @since: 1.0

    This message is issued on pressing the Now Playing key
  -->
  <method name="NowPlayingPress">
  </method>

  <!--
    CategoryPress:
    @since: 1.1

    This message is issued on pressing the category key
  -->
  <method name="CategoryPress">
  </method>

  <!--
    Shutdown:
    @since: 1.0

    This message is issued on pressing the On tipper to shutdown the system
  -->
  <method name="Shutdown">
  </method>

  <!--
    NowPlayingPressResponse:
    @entry_point_id: Entry point ID for the application
    @display_name: Human-readable display name (quick menu entry) for the current playing application
    @since: 1.0

    Signal to client to position the current playing application entry to focus
  -->
  <signal name="NowPlayingPressResponse">
    <arg name="entry_point_id" type="s"/>
    <arg name="display_name" type="s"/>
  </signal>

</interface>
</node>
