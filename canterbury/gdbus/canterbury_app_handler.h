/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_APP_HANDLER__
#define __CANTERBURY_APP_HANDLER__

#include <glib.h>

G_BEGIN_DECLS

/**
 * SECTION:gdbus/canterbury_app_handler.h
 * @title: Application handler enumerated types
 * @short-description: Enumerations related to the application handler
 */

/**
 * CanterburyAppState:
 * @CANTERBURY_APP_STATE_START: An application when registered to app manager would immediately get an App start state.
 * @CANTERBURY_APP_STATE_BACKGROUND: An application which is no longer visible but is still active in the background would receive this state.
 *                                   Application can make use of this state to hide clutter actors to save on CPU processing.
 * @CANTERBURY_APP_STATE_PAUSE: An application which is no longer visible to the user and is soon to be stopped by the application manager would receive this state
 *                              Applications should store their persistent data immediately and free any system resources like bandwidth etc.
 *                              There is no guarantee of an application gone to Pause state to come back to Start state as it purely depends on user activity.
 *                              Applications would be given a time of around 5 sec before stopping them. If apps want more time in this state,
 *                              then they still have to explicitly ask the app manager.
 * @CANTERBURY_APP_STATE_RESTART: An application already available in the activity stack, when selected again would result in this state if previously it was set to pause state.
 *                                This would mean that it has come back to foreground and has to disply its clutter actors.
 * @CANTERBURY_APP_STATE_SHOW: An application already available in the activity stack, when selected again would result in this state if previously it was set to background state.
 *                             This would mean that it has come back to foreground and has to disply its clutter actors.
 * @CANTERBURY_APP_STATE_OFF: Similar to pause state, only difference being, after this state the application is immediately killed and not maintained in the activity stack.
 * @CANTERBURY_APP_STATE_INVALID: Application has entered an invalid state. This should never happen.
 *
 * Application state.
 */
typedef enum {
  CANTERBURY_APP_STATE_START= 1,
  CANTERBURY_APP_STATE_BACKGROUND,
  CANTERBURY_APP_STATE_RESTART,
  CANTERBURY_APP_STATE_SHOW,
  CANTERBURY_APP_STATE_OFF,
  CANTERBURY_APP_STATE_PAUSE,
  CANTERBURY_APP_STATE_INVALID = 0xFF
} CanterburyAppState;

enum _CanterburyAudioType
{
  CANTERBURY_AUDIO_RESOURCE_NONE,
  CANTERBURY_AUDIO_RESOURCE_MUSIC,
  CANTERBURY_AUDIO_RESOURCE_INTERRUPT,
  CANTERBURY_AUDIO_RESOURCE_PHONE,
  CANTERBURY_AUDIO_RESOURCE_VIDEO,
  CANTERBURY_AUDIO_RESOURCE_RECORD,
  CANTERBURY_AUDIO_RESOURCE_EXTERNAL
};
typedef enum _CanterburyAudioType CanterburyAudioType;


typedef enum {
 CANTERBURY_APPSTORE_APP_STATE_INITIAL = 1 ,
 CANTERBURY_APPSTORE_APP_STATE_PURCHASED ,
 CANTERBURY_APPSTORE_APP_STATE_DOWNLOAD_READY,
 CANTERBURY_APPSTORE_APP_STATE_DOWNLOADING     ,
 CANTERBURY_APPSTORE_APP_STATE_INSTALLING    ,
 CANTERBURY_APPSTORE_APP_STATE_INSTALLED      ,
 CANTERBURY_APPSTORE_APP_STATE_UPDATE          ,
 CANTERBURY_APPSTORE_APP_STATE_UNINSTALLED     ,
} CanterburyAppstoreAppState;

/* Different types of data that is available for share */
#define CANTERBURY_ATTACHMENT_DATA "Attachment"
#define CANTERBURY_LINK_DATA "Link"
#define CANTERBURY_CONTACTS_DATA "ContactsInfo"
#define CANTERBURY_LOCATION_DATA "LocationInfo"

/* HMI states */

#define CANTERBURY_APPSTORE_LOCAL_APP_STATE_NOT_INSTALLED      9
#define CANTERBURY_APPSTORE_LOCAL_APP_STATE_FAILED             10
#define CANTERBURY_APPSTORE_LOCAL_APPSTORE_APP_UPDATING        11
#define CANTERBURY_APPSTORE_LOCAL_APP_ROLLBACK                 12

#define CANTERBURY_APPSTORE_APP_WAITING_STATE "W A I T I N G"
#define CANTERBURY_APPSTORE_APP_DOWNLOADING_STATE "L O A D I N G"
#define CANTERBURY_APPSTORE_APP_INSTALLING_STATE "I N S T A L L I N G"
#define CANTERBURY_APPSTORE_APP_UPDATING_STATE "U P D A T I N G"
#define CANTERBURY_APPSTORE_APP_REMOVAL_STATE  "R E M O V I N G"



/* Different modes of audio recording */
typedef enum
{
	CANTERBURY_AUDIO_RECORD_TYPE_MIXED,
	CANTERBURY_AUDIO_RECORD_TYPE_EXCLUSIVE,
	CANTERBURY_AUDIO_RECORD_TYPE_INVALID
} CanterburyAudioRecordType;

typedef enum
{
  CANTERBURY_EXECUTABLE_TYPE_APPLICATION,
  CANTERBURY_EXECUTABLE_TYPE_SERVICE,
  CANTERBURY_EXECUTABLE_TYPE_EXT_APP,
  CANTERBURY_EXECUTABLE_TYPE_AGENT_SERVICE,
  CANTERBURY_EXECUTABLE_TYPE_STARTUP_APPLICATION,
  CANTERBURY_EXECUTABLE_TYPE_UNKNOWN
} CanterburyExecutableType;

typedef enum
{
  CANTERBURY_BKG_STATE_RUNNING =0,
  CANTERBURY_BKG_STATE_STOPPED,
  CANTERBURY_BKG_STATE_KILLED,
  CANTERBURY_BKG_STATE_UNKNOWN
} CanterburyAppBkgState;

typedef enum
{
	CANTERBURY_BANDWIDTH_PRIORITY_HIGHEST,
	CANTERBURY_BANDWIDTH_PRIORITY_HIGH,
	CANTERBURY_BANDWIDTH_PRIORITY_NORMAL,
	CANTERBURY_BANDWIDTH_PRIORITY_MID = CANTERBURY_BANDWIDTH_PRIORITY_NORMAL,
	CANTERBURY_BANDWIDTH_PRIORITY_LOW,
	CANTERBURY_BANDWIDTH_PRIORITY_LOWEST,
	CANTERBURY_BANDWIDTH_PRIORITY_UNKNOWN
} CanterburyBandwidthPriority;

typedef enum
{
  /* FIXME: what's the difference between this and UNKNOWN? */
  CANTERBURY_AUDIO_RESOURCE_TYPE_NONE,
  /* PA_PROP_MEDIA_ROLE: music */
  CANTERBURY_AUDIO_RESOURCE_TYPE_MUSIC,
  /* FIXME: what does this mean? */
  CANTERBURY_AUDIO_RESOURCE_TYPE_INTERRUPT,
  /* PA_PROP_MEDIA_ROLE: phone */
  CANTERBURY_AUDIO_RESOURCE_TYPE_PHONE,
  /* PA_PROP_MEDIA_ROLE: video */
  CANTERBURY_AUDIO_RESOURCE_TYPE_VIDEO,
  /* PA_PROP_MEDIA_ROLE: production? maybe */
  CANTERBURY_AUDIO_RESOURCE_TYPE_RECORD,
  /* FIXME: what does this mean? */
  CANTERBURY_AUDIO_RESOURCE_TYPE_EXTERNAL,
  CANTERBURY_AUDIO_RESOURCE_TYPE_UNKNOWN
} CanterburyAudioResourceType;


typedef enum {
	CANTERBURY_STATUSBAR_MAXIMIZED = 1,
	CANTERBURY_STATUSBAR_UNDER_ANIMATION = 2,
	CANTERBURY_STATUSBAR_MINIMIZED = 3
} CanterburyStatusBarState;

/* Dbus string to represent NULL string */
#define CANTERBURY_DBUS_UNKNOWN "### UNKNOWN ###"

/* Application Launch from various clients or hard keys */
#define CANTERBURY_LAUNCHER_CLIENT     "Launcher"
#define CANTERBURY_HOME_SCREEN_CLIENT  "home-screen"
#define CANTERBURY_STATUS_BAR_CLIENT   "status-bar"
#define CANTERBURY_POPUP_CLIENT        "popup-layer"
#define CANTERBURY_DATA_EXG_MGR_CLIENT "data-exchnge-mgr"
#define CANTERBURY_LUM_CLIENT          "last-user-mode"
#define CANTERBURY_SELF_CLIENT         "app-manager"
#define CANTERBURY_BACK_KEY            "back-key"
#define CANTERBURY_HOME_KEY            "home-key"
#define CANTERBURY_CATEGORY_KEY        "category-key"
#define CANTERBURY_NOW_PLAYING_KEY     "now-playing-key"
#define CANTERBURY_WINDOW_MANAGER      "win-mgr"
#define CANTERBURY_UNKNOWN_CLIENT      "Unknown"


/* Application Launch Trigger Definition */
//#define APP_START_LAUNCHER "AppStartFromLauncher"
//#define APP_START_SHARE    "AppStartFromShare"
//#define APP_START_RELATED  "AppStartFromRelated"


/* application audio request type */
#define CANTERBURY_APP_AUDIO_REQUEST  "AudioRequest"
#define CANTERBURY_APP_AUDIO_RELEASE  "AudioRelease"

/* application audio state */
#define CANTERBURY_AUDIO_PLAY  1
#define CANTERBURY_AUDIO_PAUSE 2
#define CANTERBURY_AUDIO_STOP  3

G_END_DECLS

#endif
