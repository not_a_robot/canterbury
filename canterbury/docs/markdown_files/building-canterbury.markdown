## Building Canterbury

[Mail the maintainers](mailto:canterbury@apertis.org)

### Platform-specific instructions

#### Linux

Debian images can be obtained [here](https://repositories.apertis.org/)

To build Canterbury from sources, get the latest source archives from
<https://git.apertis.org/cgit/canterbury.git/>. Once you have extracted
the sources from the archive execute the following commands in the
top-level directory:

```
$ ./autogen.sh
$ make
$ make install
```

You can configure the build with number of additional arguments passed
to the configure script, the full list of which can be obtained by
running `./configure --help`.
