/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "canterbury/errors.h"

#include <gio/gio.h>

/**
 * SECTION:errors.h
 * @title: General error types
 * @short_description: Errors reported by the Canterbury API
 */

static const GDBusErrorEntry error_entries[] =
{
    { CBY_ERROR_FAILED, "org.apertis.Canterbury.Error.Failed" },
    { CBY_ERROR_INVALID_ARGUMENT,
      "org.apertis.Canterbury.Error.InvalidArgument" },
    { CBY_ERROR_ACCESS_DENIED, "org.apertis.Canterbury.Error.AccessDenied" },
    { CBY_ERROR_BUSY, "org.apertis.Canterbury.Error.Busy" },
    { CBY_ERROR_LAUNCH_FAILED, "org.apertis.Canterbury.Error.LaunchFailed" },
    { CBY_ERROR_INVALID_ENTRY_POINT, "org.apertis.Canterbury.Error.InvalidEntryPoint" },
    { CBY_ERROR_NOT_OUR_ENTRY_POINT, "org.apertis.Canterbury.Error.NotOurEntryPoint" },
};

G_STATIC_ASSERT (G_N_ELEMENTS (error_entries) == CBY_N_ERRORS);

GQuark
cby_error_quark (void)
{
  static volatile gsize id = 0;

  g_dbus_error_register_error_domain ("cby-error-quark", &id, error_entries,
                                      G_N_ELEMENTS (error_entries));

  return (GQuark) id;
}
