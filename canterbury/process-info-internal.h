/*<private_header>*/
/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CANTERBURY_PROCESS_INFO_INTERNAL_H__
#define __CANTERBURY_PROCESS_INFO_INTERNAL_H__

#include "canterbury/process-info.h"

/* Mainly exposed here for CbyEntryPoint internal usage - to be used when the
 * entry point executable doesnt point to an absolute path (which is required
 * by cby_process_info_new_for_path_and_user()).
 */
static inline CbyProcessInfo *
_cby_process_info_new (void)
{
  return g_initable_new (CBY_TYPE_PROCESS_INFO, NULL, NULL, NULL);
}

#endif
