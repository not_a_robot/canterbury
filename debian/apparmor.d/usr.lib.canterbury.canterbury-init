# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>
#include <tunables/sys>

/usr/lib/canterbury/canterbury-init {
  # This tool is run with administrative permissions, so we don't use
  # the broader chaiwala-base abstraction.
  #include <abstractions/base>
  #include <abstractions/apparmor_api/find_mountpoint>

  /usr/lib/canterbury/canterbury-init mr,
  /{usr/,}sbin/apparmor_parser mrix,

  capability mac_admin,
  /etc/apparmor/** r,
  /etc/apparmor.d/** r,
  owner @{PROC}/@{pid}/fd/ r,
  owner @{PROC}/sys/kernel/osrelease r,
  @{sys}/kernel/security/apparmor/.replace w,
  @{sys}/kernel/security/apparmor/{,**} r,
  /var/lib/apertis_extensions/apparmor.d/{,**} r,
  /var/lib/apertis_extensions/apparmor.d/.lock rwk,
  /var/lib/apertis_extensions/apparmor.d/cache/{,**} rw,
  /var/lib/apertis_extensions/apparmor.d/cache/.ubercache rw,
  /var/lib/apertis_extensions/apparmor.d/cache/.ubercache.new~ rw,
  /Applications/*.*/etc/apparmor.d/* r,

  # Don't use pluggable GIO modules at all.
  deny /usr/lib/@{multiarch}/gio/modules/{,**} mr,
}
