# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>

/usr/lib/canterbury/canterbury-postinst-entry-points {
  # This tool is run with administrative permissions, so we don't use
  # the broader chaiwala-base abstraction.
  #include <abstractions/base>

  /usr/lib/canterbury/canterbury-postinst-entry-points mr,

  /var/lib/apertis_extensions/applications/{,**} rw,
  /var/lib/apertis_extensions/icons/{,**} rw,

  # We don't use <abstractions/canterbury-platform-read-entry-points>
  # because we need write access (above), and because we only need to
  # consume icons and entry points in /Applications, not in other prefixes
  /Applications/*.*/share/applications/{,**} r,
  /Applications/*.*/share/icons/{,**} r,

  # Don't use pluggable GIO modules at all.
  deny /usr/lib/@{multiarch}/gio/modules/{,**} mr,
}
